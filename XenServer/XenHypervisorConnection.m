//
//  XenHypervidoeConnection.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// comment this in for more debug logging.
// #define displayLogging

#import "XenHypervisorConnection.h"
#import "XMLRPCRequest.h"
#import "XMLRPCResponse.h"
#import "HypervisorConnectionFactory.h"
#import "XenVM.h"
#import "XenHost.h"
#import "XenStorage.h"
#import "XenNIC.h"
#import "XenHost_metrics.h"
#import "XenVDI.h"
#import "XenVBD.h"
#import "XenNetwork.h"
#import "XenVM_metrics.h"

// declare the private methods
@interface XenHypervisorConnection (){
    NSDate* ignoreHostMetricRefreshUntil;
}

- (void)setLastErrorFromXMLRPCResponse: (XMLRPCResponse *)response;
- (void)sendRequestWithMethod:(NSString *)method;
- (void)sendRequestWithMethod:(NSString *)method andParamter:(NSString *)parameter useTimeout:(bool)useTimeout;
- (void)sendRequestWithMethod:(NSString *)method andParamters:(NSArray *)parameter useTimeout:(bool)useTimeout;
- (void)sendRequestWithMethod:(NSString *)method andParamter:(NSString *)parameter withTimeout:(NSNumber*)timeoutSeconds;
- (void)ProcessRequestQueue;
- (void) processLoginResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventRegisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventNextResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventUnregisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVMResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVMObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processHostResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleHostObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processHostMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleHostMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processHostCPUResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleHostCPUObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processNICResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleNICObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processNetworkResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleNetworkObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processStorageResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleStorageObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processPBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSinglePBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVMGuestMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVMGuestMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVMMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVMMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processVDIResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processSingleVDIObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processType:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processObjectRef:(NSString *) requestedOpaqueRef Type:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (void) processEventDataUsingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess;
- (XenBase *) buildObjectType:(int)objectType Reference:reference FromDictionary:objectContent;
- (void) setConnectionAddress:(NSURL*) theConnectionAddress;
- (void) setSessionID:(NSString *)theSessionID;

- (void) refreshAllHostPerformanceData;

@end

// store the list of class names that the XenServer API uses to register events for (along with the
// identifier used by this connection class, so that all objects that are stored in the cache are 
// also registered with the eventing mechanism.
static NSDictionary *eventClasses = nil;

@implementation XenHypervisorConnection

@synthesize closing;

- (NSURL*) connectionAddress{
    return connectionAddress;
}

- (NSString*) sessionID{
    return sessionID;
}

- (void) setSessionID:(NSString *)theSessionID{
    sessionID = [theSessionID copy];
}

- (void) setConnectionAddress:(NSURL*) theConnectionAddress{
    connectionAddress  = theConnectionAddress; 
}

- (void)reloadCoreData;{
    int requestObjects = 0;
    if ([self isUpdateAvailableForType:HYPOBJ_VM]){
        requestObjects = HYPOBJ_VM;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_STORAGE]){
        requestObjects = requestObjects | HYPOBJ_STORAGE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_HOST]){
        requestObjects = requestObjects | HYPOBJ_HOST;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_NETWORK]){
        requestObjects = requestObjects | HYPOBJ_NETWORK;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_STORAGE]){
        requestObjects = requestObjects | HYPOBJ_STORAGE;
    }
    
    if (requestObjects != 0){
        [self RequestHypObjectsForType:requestObjects];  //VM implies TEMPLATE TOO
    }
}

// constructor for the class
-(id) init{
    self = [super init];
    if (self) {
        // build the event dictionary
        if (!eventClasses){
           eventClasses = [NSDictionary dictionaryWithObjectsAndKeys:
                       [NSNumber numberWithInt:HYPOBJ_POOL], @"pool",   // dont need this otherwise XenServer events never timeout
                       [NSNumber numberWithInt:HYPOBJ_VM], @"vm",
                       [NSNumber numberWithInt:HYPOBJ_HOST], @"host",
                       [NSNumber numberWithInt:HYPOBJ_STORAGE], @"sr",
                       [NSNumber numberWithInt:HYPOBJ_NETWORK], @"network",
                       [NSNumber numberWithInt:HYPOBJ_NIC], @"pif",
                       [NSNumber numberWithInt:HYPOBJ_PBD], @"pbd",
                       [NSNumber numberWithInt:HYPOBJ_VM_GUEST_METRICS], @"vm_guest_metrics",
                       [NSNumber numberWithInt:HYPOBJ_VM_METRICS], @"vm_metrics",
                       [NSNumber numberWithInt:HYPOBJ_HOST_METRICS], @"host_metrics",
                       [NSNumber numberWithInt:HYPOBJ_VBD], @"vbd",
                       [NSNumber numberWithInt:HYPOBJ_VDI], @"vdi",
                       [NSNumber numberWithInt:HYPOBJ_CONSOLES], @"console",
                       nil];
        }
        hypervisorConnectionType = HYPERVISOR_XEN;
        [self setClosing:NO]; 
    }
    return self;
}
// implement any abstract methods that are required

// close the connection
- (void)closeConnection{
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_XEN];
    NSMutableDictionary *xenConnections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    if (xenConnections != nil){
        NSString *key = nil;
        for (NSString* connectionKey in [xenConnections allKeys]){
            XenHypervisorConnection *connection = [xenConnections objectForKey:connectionKey];
            if (connection == self){
                key = connectionKey;
            }
        }
        if (key){
            [self setClosing:YES];
            NSLog(@"Closing connection %@", key);
            // send an unregister and logout
            NSArray *x = [[NSArray alloc] init];
            [self sendRequestWithMethod:CMD_EVENT_UNREGISTER andParamters:[NSArray arrayWithObjects:sessionID,x,nil] useTimeout:NO];
            x = nil;
            [self sendRequestWithMethod:CMD_SESSION_LOGOUT andParamter:sessionID useTimeout:NO];
            // if suspending dont remove the connection
            if (!suspending){
               [xenConnections removeObjectForKey:key];
            }
        }
    }
    
    hypervisorId = nil;
    if (suspending){
        suspending = NO;
        suspended = YES;
    }
    else
    {
        connectionUsername = nil;
        connectionPassword = nil;
    }
}


// Connect to the hypervisor using the supplied details
// return Yes if connected or No if failed
- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString*)password
{
    [super ConnectToAddress:address withUsername:username andPassword:password];
    NSLog(@"ConnectToAddress %@ username is %@",address,username);
    if ([self GetConnectionState] != CONNECTION_CONNECTED && [self GetConnectionState] != CONNECTION_CONNECTING){
        closing = NO;
        NSString *finalAddress;
        if (![address hasPrefix:@"http://"] && ![address hasPrefix:@"https://"]){
            finalAddress = [@"http://" stringByAppendingString:address];
        }
        else {
            finalAddress = address;
        }
        [self setConnectionAddress: [NSURL URLWithString:finalAddress]];

        // use the async version so that can apply short timeout from settings
        [self sendRequestWithMethod:CMD_SESSION_LOGIN andParamters:[NSArray arrayWithObjects:username,password,nil] useTimeout:YES];
        connectionUsername = nil;
        connectionUsername = [username copy];
        connectionPassword = nil;
        connectionPassword = [password copy];
    }
    else {
        // dont connect again just imply the connect
        NSLog(@"Already connected");  
    }
}

// close the connection mark all objects as outofdate
- (void)suspendConnection{
    if (autoUpdateEnabled){
        suspending = YES;
        [self closeConnection];
        [self flushHypObjectCache];
    }
}

// clear the cache, reopen the connection mark all objects as outofdate
- (void)resumeConnection{
    if (autoUpdateEnabled){
        resuming = YES;
        [self ConnectToAddress:[connectionAddress description] withUsername:connectionUsername andPassword:connectionPassword];
    }
}


// return the state of the current connection
- (ConnectionState) GetConnectionState{
    //Todo --> implement this properly if have sessionID then its connected I think right now
    // might not be correct after dropped to backgrond for long periods of time.  Might need to
    // be resolved as part of reconnection logic TBD.
    if (suspending){
        return CONNECTION_SUSPENDING;
    }

    if (resuming){
        return CONNECTON_RESUMING;
    }

    if (suspended){
        return CONNECTION_SUSPENDED;
    }

    if ([self sessionID] == nil){
        return CONNECTION_UNCONNECTED;
    }
    else
    {
        return CONNECTION_CONNECTED;
    }
}

// clean up
// think that this should be on the main thead
- (void)dealloc{

    NSLog(@"dealloc XenHypervisor Connection");
    if (outstandingSingleObjectRequestsByType){
        outstandingSingleObjectRequestsByType = nil;
    }
    if (eventClasses){
        eventClasses = nil;
    }
    
    if (manager){
        [manager closeConnections];
        manager = nil;
    }
    [self setSessionID:nil];
        
    if (connectionAddress){
        connectionAddress = nil;
    }
}

#pragma mark -
#pragma mark XMLRPCConnectionDelegate protocol

/*
 * call back that is called every time the XMLRPCConnection receives a response
 */
- (void)request: (XMLRPCRequest *)request didReceiveResponse: (XMLRPCResponse *)response {
    
#ifdef displayLogging
    NSLog (@" ------ Received response to request %@ --------",[request method]);
#endif
    // dont process any responses if the connection is closing.
    if (closing){
        NSLog(@"Not processing response as connection is closing");
        if ([[request method] isEqualToString:CMD_SESSION_LOGOUT]){
            if (manager){
                [manager closeConnections];
                manager = nil;
                // clear the cache
                if (cachedHypObjects){
                    [cachedHypObjects removeAllObjects];
                }
            }
        }
        [self setSessionID:nil];
        return;
    }
   
    BOOL sucsess = YES;
    if ([response isFault]) {
        sucsess = NO;
        NSLog(@"Fault code: %@", [response faultCode]);
        NSLog(@"Fault string: %@", [response faultString]);
        [self setLastErrorFromXMLRPCResponse:response];
    } else {
        //NSLog(@"Parsed response: %@", [response object]);
    }
    //NSLog(@"Response body: %@", [response body]);

    NSDictionary *responseData =  [response object];
    if (![response isFault] && responseData != nil){
        NSString *status = [responseData valueForKey:@"Status"];
        sucsess = [status isEqualToString:@"Success"];
    }

    NSArray* errorDescription;
    int errorCode = 0;
    NSString* errorString;
    
    if (!sucsess){
        errorDescription = [responseData valueForKey:@"ErrorDescription"];
        errorString = [errorDescription objectAtIndex:0];
        // not always an error code.
        if ([errorDescription count] > 1){
            errorString = [NSString stringWithFormat:@"%@, %@",errorString, [errorDescription objectAtIndex:1]];
        }
        errorCode = -1;
    }
    
    // just fire a delegate saying got response
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (id theConnectionDelegate in copyOfDelegates){
        if ([theConnectionDelegate respondsToSelector: @selector(hypervisorResponseReceived:requestCmd:error:)]){
            NSError* error = nil;
            if (!sucsess){
                error = [NSError errorWithDomain:errorString code:errorCode userInfo:nil];
            }
            [theConnectionDelegate hypervisorResponseReceived:self requestCmd:[request method] error:error];
        }
    }

    if (!sucsess){
        NSArray* errorDescription = [responseData valueForKey:@"ErrorDescription"];
        NSString* errorCode = [errorDescription objectAtIndex:0];
        // if a request reports a different address to connect to as the specified server is a slave then
        // resend the request to the new address and ignore the error
        // otherwise pass the error on to be dealt wth.
        // this is only processed if the original request was http, otherwise it will not
        // work as the new address is only an ipAddress which is not enough for the redirect.
        // There is no solution for this in the htts world at present.
        if ([errorCode isEqualToString:ERROR_HOST_IS_SLAVE]){
            NSString *newAddress = [errorDescription objectAtIndex:1];
            NSString *newUrlString = [NSString stringWithFormat:@"%@://%@", [[self connectionAddress] scheme], newAddress];
            [self setConnectionAddress: [NSURL URLWithString:newUrlString]];
            // resend the request to the new address
            [self sendRequestWithMethod:[request method] andParamter:[[request parameters] objectAtIndex:0] useTimeout:YES];
            return;
        }
    }
    
    NSString *requestedOpaqueRef = nil;
        
    if ( [[request method] isEqualToString: CMD_SESSION_LOGIN]){
        [self processLoginResponse:responseData withSucsess:sucsess];
    }
    if ( [[request method] isEqualToString: CMD_EVENT_REGISTER]){
        [self processEventRegisterAllResponse:responseData withSucsess:sucsess];
    }
    if ( [[request method] isEqualToString: CMD_EVENT_UNREGISTER]){
        [self processEventUnregisterAllResponse:responseData withSucsess:sucsess];
    }
    if ( [[request method] isEqualToString: CMD_EVENT_NEXT]){
        [self processEventNextResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VM_GET_ALL]){
        [self processVMResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VM_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVMObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_HOST_GET_ALL]){
        [self processHostResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString:CMD_HOST_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleHostObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_HOST_CPUS_GET_ALL]){
        [self processHostCPUResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_HOST_CPUS_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleHostCPUObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_STORAGE_GET_ALL]){
        [self processStorageResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_STORAGE_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleStorageObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_NIC_GET_ALL]){
        [self processNICResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_NIC_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleNICObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_NETWORK_GET_ALL]){
        [self processNetworkResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_NETWORK_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleNetworkObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqual: CMD_HOST_METRICS_GET_ALL]){
        [self processHostMetricsResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_HOST_METRICS_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleHostMetricsObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_PBD_GET_ALL]){
        [self processPBDResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_PBD_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSinglePBDObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VM_GUESTMETRICS_GET_ALL]){
        [self processVMGuestMetricsResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VM_GUESTMETRICS_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVMGuestMetricsObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VM_METRICS_GET_ALL]){
        [self processVMMetricsResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VM_METRICS_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVMMetricsObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VBD_GET_ALL]){
        [self processVBDResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VBD_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVBDObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VDI_GET_ALL]){
        [self processVDIResponse:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_VDI_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleVDIObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_CONSOLE_GET_ONE]){
        requestedOpaqueRef = [[request parameters] objectAtIndex:1];
        [self processSingleConsoleObject:requestedOpaqueRef Response:responseData withSucsess:sucsess];
    }
    else if ([[request method] isEqualToString: CMD_CONSOLE_GET_ALL]){
        [self processConsoleResponse:responseData withSucsess:sucsess];
    }
    
    // if this was a response to a single object request then clean up the queue
    if (requestedOpaqueRef){
#ifdef displayLogging
        NSLog(@"Received data with ref %@",requestedOpaqueRef);
#endif
        // find the object in the request list
        NSArray* keys = [[outstandingSingleObjectRequestsByType allKeys] copy];
        for(NSNumber* num in keys){
            NSMutableArray* queuedReferences = [outstandingSingleObjectRequestsByType objectForKey:num];
            if ([queuedReferences count] >0){
                if ([queuedReferences containsObject:requestedOpaqueRef]){
                    [queuedReferences removeObject:requestedOpaqueRef];
#ifdef displayLogging
                    NSLog(@"Removing ref %@ of type %@",requestedOpaqueRef,num);
#endif
                }
            }
            if ([queuedReferences count] ==0) {
#ifdef displayLogging
                NSLog(@"No more outstanding requests for objects of type %@",num);
#endif
                [outstandingSingleObjectRequestsByType removeObjectForKey:num];
            }
        }
        
#if defined (logCommsData)
        NSLog(@"%@", outstandingSingleObjectRequestsByType);
#endif 
        keys = nil;
    }
    
    // precess any firther requests in the queue that were waiting for this response
    // TODO if requests fail listen to this in the loading and report the issue otherwise 
    // can be locked forever
    [self ProcessRequestQueue];
}


/*
 * call back that is called every time the XMLRPCConnection receives an error
 */
- (void)request: (XMLRPCRequest *)request didFailWithError: (NSError *)error{
    NSLog(@"didFailWitherror: %@", [error localizedDescription]);
    [self setLastError:error]; 
    
    if ([[request method] isEqualToString: CMD_SESSION_LOGIN]){
        [self setSessionID:nil];
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id theConnectionDelegate in copyOfDelegates){
            if ([theConnectionDelegate respondsToSelector: @selector(hypervisorConnectedToConnection:withResult:)]){
                [theConnectionDelegate hypervisorConnectedToConnection:self withResult:NO];
            }
        }
        copyOfDelegates = nil;
    }
    else if ([[request method] isEqualToString: CMD_EVENT_NEXT]){
        // if the timeout is on an event.next then resend one
#ifdef displayLogging
            NSLog(@"event.next timout --- If this happens the events stop working even if I register and again.  Think actually might have to log out and in again");
#endif
        [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID withTimeout:[NSNumber numberWithInt:INT32_MAX]];
    }else{
        if (hypervisorConnectionDelegates){
            for (id delegate in hypervisorConnectionDelegates){
                if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                    if ([[request method] isEqualToString: CMD_VM_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM | HYPOBJ_TEMPLATE withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_HOST_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_HOST withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_NIC_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_NIC withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_STORAGE_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_STORAGE withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_HOST_METRICS_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_HOST_METRICS withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_PBD_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_PBD withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_VM_GUESTMETRICS_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM_GUEST_METRICS withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_VM_METRICS_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM_METRICS withResult:NO];
                    }
                   else if ([[request method] isEqualToString: CMD_VBD_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VBD withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_VDI_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VDI withResult:NO];
                    }
                    else if ([[request method] isEqualToString: CMD_CONSOLE_GET_ALL]){
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_CONSOLES withResult:NO];
                    }
                }
            }
        }
    }
}

// for some reason and this probably needs some deeper thought the following allows for
// self signed certificates to work on the simulator
// but not the the iOS device itslef.  These breakpoints are never hit when on the real
// device
- (void)request: (XMLRPCRequest *)request didReceiveAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge{
    // to make this work like XenServer I should get the fingerpring here and store it if I have not had one before
    // if it is different ask if you want to accept it if not go ahead.  For now leave it like this, although it
    // is not very secure, but then self sighned certs are not anyway :)

    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)request: (XMLRPCRequest *)request didCancelAuthenticationChallenge: (NSURLAuthenticationChallenge *)challenge{
    NSLog(@"didCancelAuthenticationChallenge");
}

- (BOOL)request: (XMLRPCRequest *)request canAuthenticateAgainstProtectionSpace: (NSURLProtectionSpace *)protectionSpace{
    // this will allow self signed certificates accross https connecton
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}


#pragma mark -
#pragma mark XenHypervisorConnection

/*
 * Request the host performance data.  If it is already stored then return that data
 * otherwise fetch it fresh
 */
- (NSArray*) allHostPerformanceData{
    // if there are no hosts then cant get any data
    NSArray *allHosts = [self hypObjectsForType:HYPOBJ_HOST];
    if (allHosts && [allHosts count] > 0){
        if (!ignoreHostMetricRefreshUntil ||
            [[NSDate date] timeIntervalSince1970] > [ignoreHostMetricRefreshUntil timeIntervalSince1970]){
            ignoreHostMetricRefreshUntil = [[NSDate alloc] initWithTimeIntervalSinceNow:(1 * 60)];
            // always do this in the background
            [self performSelectorInBackground:@selector(refreshAllHostPerformanceData) withObject:nil];
        }
    }
    return allHostRRDs;
}


// get the last 10 minutes of data here
// maybe make this a configurable amount at a future point
// wget http://<server>/rrd_updates?session_id=OpaqueRef:<SESSION HANDLE>&start=10258122541&host=true
// see http://community.citrix.com/display/xs/Using+XenServer+RRDs
- (void) refreshAllHostPerformanceData{
    allHostRetrievalTime = [NSDate date];
    // reload the performance data from all the Hosts
    //startTime = Now - 10minutes (might be longer lets see) in epoch time (seconds since 1970)
    NSDate *startDate = [[NSDate alloc] initWithTimeIntervalSinceNow:-(10 * 60) ];
    NSTimeInterval timeSince1970 = [startDate timeIntervalSince1970];

    // get all the other host addresses
    NSMutableArray* hostURLs = [[NSMutableArray alloc] init];
    NSArray *allHosts = [self hypObjectsForType:HYPOBJ_HOST];
    for (XenHost* host in allHosts ) {
        NSString* address = [host address];
        NSString* URLString = [NSString stringWithFormat:@"http://%@/rrd_updates?session_id=%@&start=%.0f&host=true&cf=AVERAGE",address,[self sessionID],timeSince1970];        
        [hostURLs addObject:[NSURL URLWithString:URLString]];
    }
    
    @try{
        NSMutableArray* allHostData = [[NSMutableArray alloc] init];
        for (NSURL* url in hostURLs) {
            XportDatabase* hostRRD = [[XportDatabase alloc] initWithUrl:url];
            [allHostData addObject:hostRRD];
        }
        
        @synchronized(allHostRRDs){
            allHostRRDs = [NSArray arrayWithArray: allHostData];
        }
    }
    @catch (id theException) {
		NSLog(@"Error loading All Host RRD: %@", theException);
        @synchronized(allHostRRDs){
            allHostRRDs = nil;
        }
    }
}

/*
 * Request a snapshot is taken
 * The VMRef can be of a VM or a snapshot
 * Can return errors -- VM BAD POWER STATE, SR FULL, OPERATION NOT ALLOWED
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)RequestNewSnapshot:(NSString*)newName forVMReference:(NSString *)ref{
    //session_id s, VM ref vm, string new_name
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, newName, nil];
    [self sendRequestWithMethod:CMD_VM_SNAPSHOT andParamters:parameters useTimeout:NO];
}

/*
 * Request a change to the VM Other config data
 * This allows for things like change auto_boot settings (interstingly this normally needs a licenced version :)
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)SetVMOtherConfig:(NSDictionary*)configSettings forVMRef:(NSString*)objectRef{
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, configSettings, nil];
    [self sendRequestWithMethod:CMD_VM_SET_OTHERCONFIG andParamters:parameters useTimeout:NO];
}

/*
 * Request a change to the Max VM CPU Count
 */
- (void)SetVMvMaxCPUCount:(int)cpuCount forVMRef:(NSString*)objectRef{
    NSNumber* value = [NSNumber numberWithInt:cpuCount];
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, value, nil];
    [self sendRequestWithMethod:CMD_VM_SET_MAXCPUCOUNT andParamters:parameters useTimeout:NO];
}

/*
 * Request a change to the VM CPU Count
*/
- (void)SetVMvCPUCount:(int)cpuCount forVMRef:(NSString*)objectRef{
    NSNumber* value = [NSNumber numberWithInt:cpuCount];
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, value, nil];
    [self sendRequestWithMethod:CMD_VM_SET_CPUCOUNT andParamters:parameters useTimeout:NO];
}

-(void) SetVMNameForVMRef:(NSString*)objectRef name:(NSString*)name{
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, name, nil];
    [self sendRequestWithMethod:CMD_VM_SET_NAME_LABEL andParamters:parameters useTimeout:NO];

}

-(void) SetVMDescriptionForVMRef:(NSString*)objectRef name:(NSString*)description{
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, description, nil];
    [self sendRequestWithMethod:CMD_VM_SET_NAME_DESCRIPTION andParamters:parameters useTimeout:NO];
    
}

-(void) SetVMMemoryLimitsForVMRef:(NSString*)objectRef
                       staticMin:(long long)static_min
                       staticMax:(long long)static_max
                       dynamicMin:(long long)dynamic_min
                       dynamicMax:(long long)dynamic_max{

    NSString* static_minValue =  [NSString stringWithFormat:@"%lld",static_min];
    NSString* static_maxValue =  [NSString stringWithFormat:@"%lld",static_max];
    NSString* dynamic_minValue =  [NSString stringWithFormat:@"%lld",dynamic_min];
    NSString* dynamic_maxValue =  [NSString stringWithFormat:@"%lld",dynamic_max];
    
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, static_minValue,static_maxValue, dynamic_minValue,dynamic_maxValue, nil];
    [self sendRequestWithMethod:CMD_VM_SET_MEMORYLIMITS andParamters:parameters useTimeout:NO];
}
/*
 * Request a revert to snapshot
 * can return VM BAD POWER STATE, OPERATION NOT ALLOWED, SR FULL, VM REVERT FAILED
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)RequestRevertToSnapshotWithReference:(NSString *)snapshotRef{
    //session_id s, VM ref snapshot
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, snapshotRef, nil];
    [self sendRequestWithMethod:CMD_VM_REVERT andParamters:parameters useTimeout:NO];
}

- (void)RequestDestroyObjectType:(int)objectType WithReference:(NSString *)objectRef{
    //session_id s, obejct ref snapshot
    NSArray *parameters = [NSArray arrayWithObjects:sessionID, objectRef, nil];
    NSString* cmd;
    switch (objectType) {
        case HYPOBJ_SNAPSHOT:
            cmd = CMD_VM_DESTROY;
            break;
        default:
            break;
    }
    [self sendRequestWithMethod:cmd andParamters:parameters useTimeout:NO];
}

/*
 * Request a power operation for a VM.  No operation will be carried out if
 * the reference is not for a VM (e.g. it is for a template or a snapshot)
 * or the power operation requested is not one of the available ones
 * TODO - really should return some sort of error code in these circumstances
 */
- (void)RequestPowerOperation:(int)operation forVMReference:(NSString *)ref{
    // check that the reference is for a VM
    NSArray *vms = [self hypObjectsForType:HYPOBJ_VM withCondition:[XenBase XenBaseFor:ref]]; 
    if (vms != nil && [vms count] > 0){
        // check that the power operation is an available one
        XenVM *vm = [vms objectAtIndex:0];
        NSSet *powerOps = [vm availablePowerOperations];
        for (NSNumber *op in powerOps){
            if ([op intValue] == operation){
                // ok lets request it then
#ifdef displayLogging
                NSLog(@"Requesting powerOperation %d", operation);
#endif
                switch ([op intValue]) {
                    case XENPOWEROPERATION_START:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, kCFBooleanFalse , kCFBooleanTrue, nil];
                        [self sendRequestWithMethod:CMD_VM_START andParamters:parameters useTimeout:NO];
                        break;
                    }
                    case XENPOWEROPERATION_STOP_CLEAN:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_STOP_CLEAN andParamters:parameters useTimeout:NO];
                        break;
                    }
                    case XENPOWEROPERATION_STOP_FORCE:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_STOP_FORCE andParamters:parameters useTimeout:NO];
                        break;
                    }
                    case XENPOWEROPERATION_SUSSPEND:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref,  nil];
                        [self sendRequestWithMethod:CMD_VM_SUSSPEND andParamters:parameters useTimeout:NO];
                        break;
                    }
                    case XENPOWEROPERATION_RESUME:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, kCFBooleanFalse , kCFBooleanTrue, nil];
                        [self sendRequestWithMethod:CMD_VM_RESUME andParamters:parameters useTimeout:NO];
                        break;
                    }
                    case XENPOWEROPERATION_RESTART_CLEAN:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_RESTART_CLEAN andParamters:parameters useTimeout:NO];
                        break;
                    }
                    case XENPOWEROPERATION_RESTART_FORCE:{
                        NSArray *parameters = [NSArray arrayWithObjects:sessionID, ref, nil];
                        [self sendRequestWithMethod:CMD_VM_RESTART_FORCE andParamters:parameters useTimeout:NO];
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
        }
    }
}

-(void) LogInformationForType:(int)hypObjectType andReference:(NSString*)reference withMessage:(NSString*) message{
#ifdef displayLogging
    NSString *objectType = [XenBase ObjectNameForType:hypObjectType];

    if (reference){
        NSLog(@"%@ <<type %@ and reference %@>>",message,objectType, reference);
    }
    else{
        NSLog(@"%@ <<type %@>>",message,objectType);
    }
#endif
}

-(void) LogInformationForType:(int)hypObjectType withMessage:(NSString*)message{
    [self LogInformationForType:hypObjectType andReference:nil withMessage:message];
}

/*
 * Request data for the required hypervisor object types from the XenServer
 * Can request multiple types in one go by using the flags as required
 */
- (void)RequestHypObjectsForType:(int)hypObjectType{
    [self LogInformationForType:hypObjectType withMessage:@"Request all"];
    
    if (hypObjectType & HYPOBJ_HOST){
        [self setObjectTypeRequestPending:HYPOBJ_HOST];
        [self sendRequestWithMethod:CMD_HOST_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_VM || hypObjectType & HYPOBJ_TEMPLATE || hypObjectType & HYPOBJ_SNAPSHOT){   // for Xen this gets VMs, templates and snapshots
        [self setObjectTypeRequestPending:HYPOBJ_VM];
        [self setObjectTypeRequestPending:HYPOBJ_TEMPLATE];
        [self setObjectTypeRequestPending:HYPOBJ_SNAPSHOT];
        [self sendRequestWithMethod:CMD_VM_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_STORAGE){
        [self setObjectTypeRequestPending:HYPOBJ_STORAGE];
        [self sendRequestWithMethod:CMD_STORAGE_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_NIC){
        [self setObjectTypeRequestPending:HYPOBJ_NIC];
        [self sendRequestWithMethod:CMD_NIC_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_NETWORK){
        [self setObjectTypeRequestPending:HYPOBJ_NETWORK];
        [self sendRequestWithMethod:CMD_NETWORK_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_HOST_METRICS){
        [self setObjectTypeRequestPending:HYPOBJ_HOST_METRICS];
        [self sendRequestWithMethod:CMD_HOST_METRICS_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_HOST_CPU){
        [self setObjectTypeRequestPending:HYPOBJ_HOST_CPU];
        [self sendRequestWithMethod:CMD_HOST_CPUS_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_PBD){
        [self setObjectTypeRequestPending:HYPOBJ_PBD];
        [self sendRequestWithMethod:CMD_PBD_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_VM_GUEST_METRICS){
        [self setObjectTypeRequestPending:HYPOBJ_VM_GUEST_METRICS];
        [self sendRequestWithMethod:CMD_VM_GUESTMETRICS_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_VM_METRICS){
        [self setObjectTypeRequestPending:HYPOBJ_VM_METRICS];
        [self sendRequestWithMethod:CMD_VM_METRICS_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_VBD){
        [self setObjectTypeRequestPending:HYPOBJ_VBD];
        [self sendRequestWithMethod:CMD_VBD_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_VDI){
        [self setObjectTypeRequestPending:HYPOBJ_VDI];
        [self sendRequestWithMethod:CMD_VDI_GET_ALL andParamter:sessionID useTimeout:NO];
    }
    if (hypObjectType & HYPOBJ_CONSOLES){
        [self setObjectTypeRequestPending:HYPOBJ_CONSOLES];
        [self sendRequestWithMethod:CMD_CONSOLE_GET_ALL andParamter:sessionID useTimeout:NO];
    }
}

-(void) queueRequest:(NSString *)reference ForType:(int)hypObjectType{
    if (!outstandingSingleObjectRequestsByType){
        outstandingSingleObjectRequestsByType = [[NSMutableDictionary alloc] initWithCapacity:5];
    }
    
    NSMutableArray* typeList = [outstandingSingleObjectRequestsByType objectForKey:[NSNumber numberWithInt:hypObjectType]];
    if (!typeList){
        typeList = [[NSMutableArray alloc] init];
        [outstandingSingleObjectRequestsByType setObject:typeList forKey:[NSNumber numberWithInt:hypObjectType]];
    }

    // if the object request is already queued dont add it again.
    if (![typeList containsObject:reference]){
        [typeList addObject:[reference copy]];
    }
}

- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType{
    if (!reference || [reference isEqualToString: @""]){
        // should not really ever get here, but if it does dont process the request
        NSLog(@"Dont queue an object with a opaque reference that is nil or empty");
        return;
    }
    [self queueRequest:reference ForType:hypObjectType];
    [self LogInformationForType:hypObjectType andReference:reference withMessage:@"Queued Request Object"];
    [self ProcessRequestQueue];
}

-(void)ProcessRequestQueueForType:(int)type withMethod:(NSString*)method{

    // if already processing a request do nothing
    if (pendingUpdates != 0){
        NSLog(@"waiting --> Can't processing Request for type %d - request in progress",type);
        return;
    }

    [self setObjectTypeRequestPending:type];
    NSMutableArray* queuedReferences = [outstandingSingleObjectRequestsByType objectForKey:[NSNumber numberWithInt:type]];
    if ([queuedReferences count] >0){
#ifdef displayLogging
        NSLog(@"processing Request for type %d",type);
#endif
        NSString* reference = [queuedReferences objectAtIndex:0];
        NSArray *parameters = [NSArray arrayWithObjects:sessionID, reference, nil];
        [self sendRequestWithMethod:method andParamters:parameters useTimeout:NO];
    }
}
/*
 * Request data for the required hypervisor object types from the XenServer
 * Can request single type in one go as the reference can only be for a single type
 */
- (void)ProcessRequestQueue{

    if ([outstandingSingleObjectRequestsByType count] == 0)
    {
        // nothing queued
        return;
    }

    // only process the first queued request.
    // XenServer is not very good at processing requests in parallel
    // the requests time out and get lost
    NSArray* keys = [[outstandingSingleObjectRequestsByType allKeys] copy];
    int hypObjectType = [[keys objectAtIndex:0] intValue];
    switch (hypObjectType) {
        case HYPOBJ_VM:
            [self ProcessRequestQueueForType:HYPOBJ_VM withMethod:CMD_VM_GET_ONE];
            break;
        case HYPOBJ_TEMPLATE:
            [self ProcessRequestQueueForType:HYPOBJ_TEMPLATE withMethod:CMD_VM_GET_ONE];
            break;
        case HYPOBJ_SNAPSHOT:
            [self ProcessRequestQueueForType:HYPOBJ_SNAPSHOT withMethod:CMD_VM_GET_ONE];
            break;
        case HYPOBJ_HOST:
            [self ProcessRequestQueueForType:HYPOBJ_HOST withMethod:CMD_HOST_GET_ONE];
            break;
        case HYPOBJ_HOST_CPU:
            [self ProcessRequestQueueForType:HYPOBJ_HOST_CPU withMethod:CMD_HOST_CPUS_GET_ONE];
            break;
        case HYPOBJ_STORAGE:
            [self ProcessRequestQueueForType:HYPOBJ_STORAGE withMethod:CMD_STORAGE_GET_ONE];
            break;
        case HYPOBJ_NIC:
            [self ProcessRequestQueueForType:HYPOBJ_NIC withMethod:CMD_NIC_GET_ONE];
            break;
        case HYPOBJ_NETWORK:
            [self ProcessRequestQueueForType:HYPOBJ_NETWORK withMethod:CMD_NETWORK_GET_ONE];
            break;
        case HYPOBJ_HOST_METRICS:
            [self ProcessRequestQueueForType:HYPOBJ_HOST_METRICS withMethod:CMD_HOST_METRICS_GET_ONE];
            break;
        case HYPOBJ_PBD:
            [self ProcessRequestQueueForType:HYPOBJ_PBD withMethod:CMD_PBD_GET_ONE];
            break;
        case HYPOBJ_VM_GUEST_METRICS:
            [self ProcessRequestQueueForType:HYPOBJ_VM_GUEST_METRICS withMethod:CMD_VM_GUESTMETRICS_GET_ONE];
            break;
        case HYPOBJ_VM_METRICS:
            [self ProcessRequestQueueForType:HYPOBJ_VM_METRICS withMethod:CMD_VM_METRICS_GET_ONE];
            break;
        case HYPOBJ_VBD:
            [self ProcessRequestQueueForType:HYPOBJ_VBD withMethod:CMD_VBD_GET_ONE];
            break;
        case HYPOBJ_VDI:
            [self ProcessRequestQueueForType:HYPOBJ_VDI withMethod:CMD_VDI_GET_ONE];
            break;
        case HYPOBJ_CONSOLES:
            [self ProcessRequestQueueForType:HYPOBJ_CONSOLES withMethod:CMD_CONSOLE_GET_ONE];
            break;
        default:
            NSLog(@"dont recognise the type");
            break;
    }
    keys = nil;
}

// all objects in Xen have an opaque_ref property that defines their uniqueness
-(NSString *) uniqueObjectReferencePropertyForHypObjectType:(int)objectType{
    return @"opaque_ref";
}

//
//  Determine if there are updates available for the specified object type.
//  This assumes that in 
//      AutoUpdate mode that once all objects of a specified type have been requested that
//         the automatic update mechanism will keep the list up to date.
//      Manual update mode that the lists are never up to date and can always be retreived
- (BOOL)isUpdateAvailableForType:(int)hypObjectType{
    // if in auto mode it is yes for each type until a response for all objects of that type has been recieved then after that
    // it is no (unless a lost events message is received).
    if ([self isAutoUpdateEnabled]){
        return (availableUpdates & hypObjectType);
    }
    // if in manual mode this is always yes
    else {
        return YES;
    }
}

#pragma mark - 
#pragma mark private methods

/*
 * Send a request to the connected service with the specified parameters
 * Note URL was configured during initialization, when connection was established
 */
- (void)sendRequestWithMethod:(NSString *)method{
#ifdef displayLogging
    NSLog(@"Request sent with method: %@ and hypops specified timeout", method);
#endif
    XMLRPCRequest *request = [[XMLRPCRequest alloc] initWithURL: [self connectionAddress]];
    NSNumber *timeout = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectionTimeout"];
    // 30 seconds is the smallest timeout that should be allowed any less causes issues at connect time.
    if ([timeout intValue] < 30){
        timeout = [NSNumber numberWithInt:30];
    }
    [request setTimeoutInterval:[timeout intValue]];
    if (!manager){
        manager = [XMLRPCConnectionManager sharedManager];
    }
    [request setMethod: method];
    // NSLog(@"Request body: %@", [request body]);
    [manager spawnConnectionWithXMLRPCRequest:request delegate: self];
    request = nil;
}

- (void)sendRequestWithMethod:(NSString *)method andParamter:(NSString *)parameter withTimeout:(NSNumber*)timeoutSeconds{
#ifdef displayLogging
    NSLog(@"Request sent with method: %@ and timeout of %i seconds", method, [timeoutSeconds intValue]);
#endif
    XMLRPCRequest *request = [[XMLRPCRequest alloc] initWithURL: [self connectionAddress]];
    [request setTimeoutInterval:[timeoutSeconds intValue]];
    if (!manager){
        manager = [XMLRPCConnectionManager sharedManager];
    }
    [request setMethod: method withParameter: parameter];
    //NSLog(@"Request body: %@", [request body]);
    [manager spawnConnectionWithXMLRPCRequest:request delegate: self];
    request = nil;
}

/*
 * Send a request to the connected service with the specified parameter
 * Note URL was configured during initialization, when connection was established
 */
- (void)sendRequestWithMethod:(NSString *)method andParamter:(NSString *)parameter useTimeout:(bool)useTimeout{
    NSNumber *timeout = [NSNumber numberWithInt:INT32_MAX]; // default to max int
    if (useTimeout){
        timeout = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectionTimeout"];
    }
    // 30 seconds is the smallest timeout that should be allowed any less causes issues at connect time.
    if ([timeout intValue] < 30){
        timeout = [NSNumber numberWithInt:30];
    }
    [self sendRequestWithMethod:method andParamter:parameter withTimeout:timeout];
}

/*
 * Send a request to the connected service with the specified parameters
 * Note URL was configured during initialization, when connection was established
 */

- (void)sendRequestWithMethod:(NSString *)method andParamters:(NSArray *)parameters useTimeout:(bool)useTimeout{
    NSNumber* timeout = [NSNumber numberWithInt:INT32_MAX]; // default to max int
    if (useTimeout){
        timeout = [[NSUserDefaults standardUserDefaults] valueForKey:@"connectionTimeout"];
    }
    // 30 seconds is the smallest timeout that should be allowed any less causes issues at connect time.
    if ([timeout intValue] < 30){
        timeout = [NSNumber numberWithInt:30];
    }

#ifdef displayLogging
    NSLog(@"Request sent with method: %@ and timeout of %i seconds", method, [timeout intValue]);
#endif
    XMLRPCRequest *request = [[XMLRPCRequest alloc] initWithURL: [self connectionAddress]];
    [request setTimeoutInterval:[timeout intValue]];
    if (!manager){
        manager = [XMLRPCConnectionManager sharedManager];
    }
    [request setMethod: method withParameters: parameters];
    // NSLog(@"Request body: %@", [request body]);
    [manager spawnConnectionWithXMLRPCRequest: request delegate: self];
    request = nil;
}

/*
 set the last error from the XMLRPCResponse that was received.
 */
- (void)setLastErrorFromXMLRPCResponse: (XMLRPCResponse *)response{
    [self setLastError:[NSError errorWithDomain:[response faultString] code:(NSInteger)[response faultCode] userInfo:nil]];
}    

#pragma mark -
#pragma mark ResponseProcessing
- (void) processVMResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM request");
    if (sucsess){
        NSDictionary *vms = [responseData objectForKey:@"Value"];  // dictionary with key of VM opaqueRef
        NSMutableArray *vmResults = [[NSMutableArray alloc] initWithCapacity:[vms count]];
        NSMutableArray *ControlDomainResults = [[NSMutableArray alloc] initWithCapacity:5];
        NSMutableArray *templateResults = [[NSMutableArray alloc] initWithCapacity:10];
        NSMutableArray *snapshotResults = [[NSMutableArray alloc] initWithCapacity:10];
        NSArray *vmContentKeys = [vms allKeys];
        for(NSString *key in vmContentKeys){
            NSDictionary *vmContent = [vms valueForKey:key];
            XenVM *vm = [[XenVM alloc] initWithConnection:self Reference:key Dictionary: vmContent];

            if ([vm vmType] == XENVMTYPE_VM){
                if ([vm is_control_domain]){
                    [ControlDomainResults addObject:vm];
                }
                else{
                    [vmResults addObject:vm];
                }
            }
            if ([vm vmType] == XENVMTYPE_SNAPSHOT){
                [snapshotResults addObject:vm];
            }
            if ([vm vmType] == XENVMTYPE_TEMPLATE){
                [templateResults addObject:vm];
            }
            if (walkTreeMode){
                // go though the object graph for this object and check that all required data is loaded
                [self PopulateHypObject:vm];
            }
            if (hypervisorConnectionDelegates){
                // work on a copy of the delegates so that if anyone removes themselves whilst being used
                // the array is not mutated.
                NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
                for (id delegate in copyOfDelegates){
                    if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                        
                        [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:[vm vmType] withReference:[vm opaque_ref] withResult:sucsess];
                    }
                }
                copyOfDelegates = nil;
            }
            
            vm = nil;
        }
        
        [self updateCachedHypObjects:[NSArray arrayWithArray:ControlDomainResults] forObjectType:HYPOBJ_VM_CONTROL_DOMAIN isCompleteSet:YES];
        [self updateCachedHypObjects:[NSArray arrayWithArray:vmResults] forObjectType:HYPOBJ_VM isCompleteSet:YES];
        [self updateCachedHypObjects:[NSArray arrayWithArray:templateResults] forObjectType:HYPOBJ_TEMPLATE isCompleteSet:YES];
        [self updateCachedHypObjects:[NSArray arrayWithArray:snapshotResults] forObjectType:HYPOBJ_SNAPSHOT isCompleteSet:YES];
        vmResults = nil; 
        templateResults = nil; 
        snapshotResults = nil;
    }
    else {
        // assume last known list of VMs is still good
        [self setLastError:[NSError errorWithDomain:@"XenServer VM request failed" code:0 userInfo:nil]];
    }

    if (hypervisorConnectionDelegates){
        // work on a copy of the delegates so that if anyone removes themselves whilst being used
        // the array is not mutated.
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id delegate in copyOfDelegates){
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:HYPOBJ_VM| HYPOBJ_TEMPLATE withResult:sucsess];
            }
        }
        copyOfDelegates = nil;
    }
    
    // clear the object type from the available updates flag, eventing will keep it upto date
    // after this point.
    availableUpdates = availableUpdates & (0xffffffff ^ HYPOBJ_VM);  // clear the available bit
    availableUpdates = availableUpdates & (0xffffffff ^ HYPOBJ_TEMPLATE);  // clear the available bit
    availableUpdates = availableUpdates & (0xffffffff ^ HYPOBJ_SNAPSHOT);  // clear the available bit
}

// process get single record style requests that have only one object in response,
// these are disctionaries of values where the key is the object type all for one object
// these dont contain the opaque ref so this is passed in from the request
- (void) processObjectRef:(NSString *) requestedOpaqueRef Type:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    
    NSDictionary *objectContent = [responseData objectForKey:@"Value"];     
    XenBase *newObject = [self buildObjectType:objectType Reference:requestedOpaqueRef FromDictionary:objectContent];
    if ([newObject isKindOfClass:[XenVM class]]){
        if ([(XenVM *)newObject vmType] == XENVMTYPE_VM){
            objectType = HYPOBJ_VM;
        }else if ([(XenVM *)newObject vmType] == XENVMTYPE_SNAPSHOT){ 
            objectType = HYPOBJ_SNAPSHOT;
        }else if ([(XenVM *)newObject vmType] == XENVMTYPE_TEMPLATE){
            objectType = HYPOBJ_TEMPLATE;
        }
    }
    [self updateCachedHypObjects:[NSArray arrayWithObjects:newObject,nil] forObjectType:objectType isCompleteSet:NO];
    [self LogInformationForType:objectType andReference:[newObject opaque_ref] withMessage:@"Received Object"];

    if (hypervisorConnectionDelegates){
        // work on a copy of the delegates so that if anyone removes themselves whilst being used
        // the array is not mutated.
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id delegate in copyOfDelegates){
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                if (delegate){
                    [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withReference:[newObject opaque_ref] withResult:sucsess];
                }
            }
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                if (delegate){
                    [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withResult:sucsess];
                }
            }
        }
        copyOfDelegates = nil;
    }
}

- (XenBase *) buildObjectType:(int)objectType Reference:reference FromDictionary:objectContent{
    XenBase *newObject = nil;
    switch (objectType) {
        case HYPOBJ_HOST_CPU:
            newObject = [[NSClassFromString(@"XenHostCPU") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_HOST:
            newObject = [[NSClassFromString(@"XenHost") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_VM:
            newObject = [[NSClassFromString(@"XenVM") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_TEMPLATE:
            newObject = [[NSClassFromString(@"XenVM") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_SNAPSHOT:
            newObject = [[NSClassFromString(@"XenVM") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_STORAGE:
            newObject = [[NSClassFromString(@"XenStorage") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_NIC:
            newObject = [[NSClassFromString(@"XenNIC") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_NETWORK:
            newObject = [[NSClassFromString(@"XenNetwork") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_HOST_METRICS:
            newObject = [[NSClassFromString(@"XenHost_metrics") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_PBD:
            newObject = [[NSClassFromString(@"XenPBD") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_VM_GUEST_METRICS:
            newObject = [[NSClassFromString(@"XenVM_guestMetrics") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_VM_METRICS:
            newObject = [[NSClassFromString(@"XenVM_metrics") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_VBD:
            newObject = [[NSClassFromString(@"XenVBD") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_VDI:
            newObject = [[NSClassFromString(@"XenVDI") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        case HYPOBJ_CONSOLES:
            newObject = [[NSClassFromString(@"XenConsole") alloc] initWithConnection:self Reference:reference Dictionary:objectContent];
            break;
        default:
            break;
    }
    if (walkTreeMode){
        // go though the object graph for this object and check that all required data is loaded
        [self PopulateHypObject:newObject];
    }
    return newObject;
}

- (int) convertXenClassToObjectType:(NSString *)xenClass{
    NSNumber *resultObj = [eventClasses objectForKey:xenClass];
    if (resultObj){
       return [ resultObj intValue];
    }
    NSLog(@"Failed converting XenClass %@ to object type. XenClass type unrecognized",xenClass);
    return -1;
}

//process event response
- (void) processEventDataUsingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //inside value if sucsess
    //class = vm;
    //id = 265229;
    //operation = mod;
    //ref = "OpaqueRef:f90a07cb-1a17-3697-4281-67bcca6c4425";
    //snapshot = dictionary as normal object{
    //timestamp = "1301507667.";
    if (sucsess){
        NSArray *objects = [responseData objectForKey:@"Value"];  // array of objects
        for( NSDictionary *objectDetails in objects){
            NSDictionary *objectContent = [objectDetails valueForKey:@"snapshot"];
            int objectType = [self convertXenClassToObjectType:[objectDetails valueForKey:@"class"]];
            NSString *objectRef = [objectDetails valueForKey:@"ref"];
            NSString *operationType = [objectDetails valueForKey:@"operation"];
            if (objectType != -1){
                if ([operationType isEqualToString:@"del"]){
                    // remove object from cache
                    [self removeCachedHypObjectswithReference:objectRef forObjectType:objectType];
                }
                else{
                    XenBase *newObject = [self buildObjectType:objectType Reference:objectRef FromDictionary:objectContent];
                    if (newObject != nil){
                        if ([newObject isKindOfClass:[XenVM class]]){
                            if ([(XenVM *)newObject vmType] == XENVMTYPE_VM){  // ignore shaphosts here
                                if ([(XenVM *)newObject is_control_domain]){
                                    objectType = HYPOBJ_VM_CONTROL_DOMAIN;                               }
                                else{
                                    objectType = HYPOBJ_VM;
                                }
                            }
                            if ([(XenVM *)newObject vmType] == XENVMTYPE_SNAPSHOT){
                                objectType = HYPOBJ_SNAPSHOT;
                            }
                            if ([(XenVM *)newObject vmType] == XENVMTYPE_TEMPLATE){
                                objectType = HYPOBJ_TEMPLATE;
                            }
                            
                        }
                    }
                    // because this was not a requested update do not reset the pending state
                    [self updateCachedHypObjects:[NSArray arrayWithObjects:newObject,nil] forObjectType:objectType ResetPendingUpdates:NO  isCompleteSet:NO];
                }
                
                if (hypervisorConnectionDelegates){
                    // work on a copy of the delegates so that if anyone removes themselves whilst being used
                    // the array is not mutated.
                    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
                    for (id delegate in copyOfDelegates){
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withReference:objectRef withResult:sucsess];
                        }
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withResult:sucsess];
                        }
                    }
                    copyOfDelegates = nil;
                }
            }
        }
    }
}

// process get all style requests that have muliple objects in response,
// these are disctionaries of objects where the key is the opage ref
- (void) processType:(int)objectType usingResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    if (sucsess){
        [self LogInformationForType:objectType withMessage:@"Received data"];
        
        NSDictionary *objects = [responseData objectForKey:@"Value"];  // dictionary with key of host opaqueRef
        NSMutableArray *objectsResults = [[NSMutableArray alloc] initWithCapacity:[objects count]];
        NSArray *objectsContentKeys = [objects allKeys];
        for(NSString *key in objectsContentKeys){
            NSDictionary *objectContent = [objects valueForKey:key];
            XenBase *newObject = [self buildObjectType:objectType Reference:key FromDictionary:objectContent];
            if (newObject != nil){
                // special case for Network objects
                if ([newObject isKindOfClass:[XenNetwork class]]){
                    XenNetwork *network = (XenNetwork *)newObject;
                    if ([network isGuestInstallerNetwork])
                    {
                        continue;
                    }
                }
                
                [objectsResults addObject:newObject];
                if (hypervisorConnectionDelegates){
                    // work on a copy of the delegates so that if anyone removes themselves whilst being used
                    // the array is not mutated.
                    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
                    for (id delegate in copyOfDelegates){
                        if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                            [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withReference:[newObject opaque_ref] withResult:sucsess];
                        }
                    }
                    copyOfDelegates = nil;
                }
            }
        }
        
        [self updateCachedHypObjects:[NSArray arrayWithArray:objectsResults] forObjectType:objectType isCompleteSet:YES];
        objectsResults = nil; 
    }
    else {
        // assume last known list of Host Metrics is still good
        [self setLastError:[NSError errorWithDomain:@"XenServer request failed" code:0 userInfo:nil]];
    }
    
    if (hypervisorConnectionDelegates){
        // work on a copy of the delegates so that if anyone removes themselves whilst being used
        // the array is not mutated.
        NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
        for (id delegate in copyOfDelegates){
            if (delegate && [(NSObject *)delegate respondsToSelector: @selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)]){
                [delegate hypervisorObjectsUpdated:self updatesObjectsOfType:objectType withResult:sucsess];
            }
        }
        copyOfDelegates = nil;
    }
    
    // clear the object type from the available updates flag, eventing will keep it upto date
    // after this point.
    availableUpdates = availableUpdates & (0xffffffff ^ objectType);  // clear the available bit
}

// TODO need process single template ans saphot too.
- (void) processSingleVMObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VM usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processHostMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST METRICS request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_HOST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleHostMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST METRICS request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_HOST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processHostCPUResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST CPU request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_HOST_CPU usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleHostCPUObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST CPU request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_HOST_CPU usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processHostResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_HOST usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleHostObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to HOST request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_HOST usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processNICResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to NIC request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_NIC usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleNICObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to NIC request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_NIC usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processNetworkResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Network request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_NETWORK usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleNetworkObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Network request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_NETWORK usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processStorageResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Storage request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_STORAGE usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleStorageObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to Storage request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_STORAGE usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processPBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to PBD request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_PBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSinglePBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to PBD request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_PBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processVMGuestMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VM_GUEST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVMGuestMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VM_GUEST_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processVMMetricsResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VM_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVMMetricsObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VM_METRICS usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processVBDResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VBD request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVBDObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VBD usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processVDIResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VDI request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_VDI usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleVDIObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_VDI usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processConsoleResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VDI request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processType:HYPOBJ_CONSOLES usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processSingleConsoleObject:(NSString *)requestedOpaqueRef Response:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Parsing response to VM Guest Metrics request");
    // this check is in case there is a memory warning between the request and the response this can cause error
    if (self){
        [self processObjectRef:requestedOpaqueRef Type:HYPOBJ_CONSOLES usingResponse:responseData withSucsess:sucsess];
    }
}

- (void) processLoginResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    //NSLog(@"Got response to Login request");
    if (sucsess){
        [self setSessionID:[responseData valueForKey:@"Value"]];
        // set up the event registration
        if (autoUpdateEnabled){
            NSArray *registerClasses = [eventClasses allKeys];
            [self sendRequestWithMethod:CMD_EVENT_REGISTER andParamters:[NSArray arrayWithObjects:sessionID,[NSArray arrayWithArray:registerClasses],nil] useTimeout:NO];
        }
    }
    else {
        [self setSessionID:nil];
        [self setLastError:[NSError errorWithDomain:@"XenServer connection failed - possibly wrong username or password" code:0 userInfo:nil]];
    }
    
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorConnectedToConnection:withResult:)])
        {
            [connectionDelegateToCall hypervisorConnectedToConnection:self withResult:sucsess];
        }
    }
    copyOfDelegates = nil;
    
    resuming = NO;
    suspended = NO;
    suspending = NO;
}

- (void) processEventRegisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    // if sucsess set the process event loop flag and then do get events request
    if (sucsess){
        // call the event.next method here
        [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID withTimeout:[NSNumber numberWithInt:INT32_MAX]];
    }
}

- (void) processEventNextResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    if (sucsess){
        [self processEventDataUsingResponse:responseData withSucsess:sucsess];
        if (autoUpdateEnabled){
            // call the event.next method here
            [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID withTimeout:[NSNumber numberWithInt:INT32_MAX]];
        }
    }
    else
    {
        NSLog(@"Hypervisor event data lost.");
        if (autoUpdateEnabled){
            // missing events detected? assume that this the only error that can happen here for now
            if (hypervisorConnectionDelegates){

                // work on a copy of the delegates so that if anyone removes themselves whilst being used
                // the array is not mutated.
                NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
               for (id delegate in copyOfDelegates){
                    if ([delegate respondsToSelector:@selector(hypervisorEventDataLost) ]){
                        //[delegate hypervisorEventDataLost];
                    }
                }
                copyOfDelegates = nil;
            }
            // carry on by calling the event.next method here, it is up to the app to
            // respond to the event to reset the cache and get back in sync
            // as who knows which objects have changed and what the UI is currently relying on being
            // in the cache.
            [self sendRequestWithMethod:CMD_EVENT_NEXT andParamter:sessionID withTimeout:[NSNumber numberWithInt:INT32_MAX]];
        }
    }
}

- (void) processEventUnregisterAllResponse:(NSDictionary *)responseData withSucsess:(BOOL)sucsess{
    // clear the event loop flag
    // regardless of the sucsess of the event un register
}

+ (BOOL) isAutoUpdateConfigured{
    NSNumber *eventingOn = [[NSUserDefaults standardUserDefaults] valueForKey:@"XenAutoUpdate"];
    if (eventingOn == nil)
    {
        return YES;
    }
    if ([eventingOn intValue] == 1){
        return YES;
    }
    return NO;
}

- (NSSortDescriptor*) sortForhypObjectType:(int)hypObjectType{
    return [XenBase defaultSortForhypObjectType:hypObjectType];
}

@end
