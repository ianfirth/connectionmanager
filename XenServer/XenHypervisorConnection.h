//
//  untitled.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypervisorConnectionFactory.h"
#import "XMLRPCConnectionDelegate.h"
#import "XMLRPCConnectionManager.h"
#import "XportDatabase.h"

// defines for the messages that can be sent to the XenServer XMLPRC connection
#define CMD_SESSION_LOGIN @"session.login_with_password"
#define CMD_SESSION_LOGOUT @"session.logout"
#define CMD_POOL_GET_ALL @"VM.get_all_records"
#define CMD_POOL_GET_ONE @"VM.get_record"
#define CMD_VM_GET_ALL @"VM.get_all_records"
#define CMD_VM_GET_ONE @"VM.get_record"
#define CMD_HOST_GET_ALL @"host.get_all_records"
#define CMD_HOST_GET_ONE @"host.get_record"
#define CMD_STORAGE_GET_ALL @"SR.get_all_records"
#define CMD_STORAGE_GET_ONE @"SR.get_record"
#define CMD_NIC_GET_ALL @"PIF.get_all_records"
#define CMD_NIC_GET_ONE @"PIF.get_record"
#define CMD_NETWORK_GET_ALL @"network.get_all_records"
#define CMD_NETWORK_GET_ONE @"network.get_record"
#define CMD_HOST_METRICS_GET_ALL @"host_metrics.get_all_records"
#define CMD_HOST_METRICS_GET_ONE @"host_metrics.get_record"
#define CMD_PBD_GET_ALL @"PBD.get_all_records"
#define CMD_PBD_GET_ONE @"PBD.get_record"
#define CMD_VM_GUESTMETRICS_GET_ALL @"VM_guest_metrics.get_all_records"
#define CMD_VM_GUESTMETRICS_GET_ONE @"VM_guest_metrics.get_record"
#define CMD_VM_METRICS_GET_ALL @"VM_metrics.get_all_records"
#define CMD_VM_METRICS_GET_ONE @"VM_metrics.get_record"
#define CMD_VBD_GET_ALL @"VBD.get_all_records"
#define CMD_VBD_GET_ONE @"VBD.get_record"
#define CMD_VDI_GET_ALL @"VDI.get_all_records"
#define CMD_VDI_GET_ONE @"VDI.get_record"
#define CMD_CONSOLE_GET_ALL @"console.get_all_records"
#define CMD_CONSOLE_GET_ONE @"console.get_record"
#define CMD_EVENT_REGISTER @"event.register"
#define CMD_EVENT_UNREGISTER @"event.unregister"
#define CMD_EVENT_NEXT @"event.next"
#define CMD_HOST_CPUS_GET_ALL @"host_cpu.get_all_records"
#define CMD_HOST_CPUS_GET_ONE @"host_cpu.get_record"
// -- VM Power operations
#define CMD_VM_START @"VM.start"
#define CMD_VM_STOP_CLEAN @"VM.clean_shutdown"
#define CMD_VM_STOP_FORCE @"VM.hard_shutdown"
#define CMD_VM_RESTART_CLEAN @"VM.clean_reboot"
#define CMD_VM_RESTART_FORCE @"VM.hard_reboot"
#define CMD_VM_SUSSPEND @"VM.suspend"
#define CMD_VM_RESUME @"VM.resume"
// -- VM Snapshot management
#define CMD_VM_SNAPSHOT @"VM.snapshot"
#define CMD_VM_REVERT @"VM.revert"
#define CMD_VM_DESTROY @"VM.destroy"

// -- VM update
#define CMD_VM_SET_OTHERCONFIG @"VM.set_other_config"
#define CMD_VM_SET_CPUCOUNT @"VM.set_VCPUs_at_startup"
#define CMD_VM_SET_MAXCPUCOUNT @"VM.set_VCPUs_max"
#define CMD_VM_SET_MEMORYLIMITS @"VM.set_memory_limits"
#define CMD_VM_SET_NAME_LABEL @"VM.set_name_label"
#define CMD_VM_SET_NAME_DESCRIPTION @"VM.set_name_description"


// defines the error strings that can be received
#define ERROR_HOST_IS_SLAVE @"HOST_IS_SLAVE"

// declare forward reference to the classes used in the protocol
@class XenHypervisorConnection;
@class XenBase;

/**
 Class to represent a connection to a XenServer Hypervisor.
 The XenServer connection is a caching connection and supports eventing to keep the cache up to date.
 The eventing can be turned off by the user (so as not to consume too much power on the device), this is undertaken by
 the properties of the application which is controllable via the settings page for the application.
 
 The connection uses an eventing model regardless of if the cache is maintained by events from the hypervisor.  Each time data
 objects are recieved from the hypervisor servers (be if by eventing or a user based request), then the methods on any objects
 registered (declaring implemtentation of the XenHypervisorCOnnectionDelegate protocol) will be called.
 Many delegates can be registered with the connection object at once, and each of them is notified of changes in turn.
 
 The consumer of the connection can request data from the hypervisor, these are async requests as described above.
 The connection is intended to support any version of XenServer from v 4.0 onwards.  The resulting data objects may have some
 properties left empty (nil) if the hypervisor being connected to does not support them.  It is up to the caller to determine the
 values and ensure that the code does not requre them.  The objects documentation should reflect the version of the XenServer 
 required for the data to be populated, however if there is no comment it should be assumed that the data will be available in 
 all versions from v4.0 onwards.
 
 The values for the Xen hypervisor objects are
   VM               ==> 1
   STORAGE (SR)     ==> 2
   HOST             ==> 4
   TEMPLATE         ==> 8  (note these are still VM objects in Xen)
   NIC (PIF)        ==> 16      
   HOST METRICS     ==> 32
   NETWORK (VIF)    ==> 64
   PBD              ==> 128
   SNAPSHOT         ==> 256 (note these are still VM objects in Xen)
   VM GUEST METRICS ==> 512
*/
@interface XenHypervisorConnection : HypervisorConnectionFactory <XMLRPCConnectionDelegate> {
    
    /**
     contains an Array of object refs keyed by type of requests that are waiting to be processed
     This enables the connection to only process one request for each object type at any one time
     */
    NSMutableDictionary* outstandingSingleObjectRequestsByType;
    
    /**
     The XenServer connection session identifier
     */
    NSString *sessionID;
   
    /**
     The connection address for the current connection.
     */
    NSURL *connectionAddress;
    
    /**
     The connection username for the current connection.
     */
    NSString *connectionUsername;

    /**
     The connection password for the current connection.
     */
    NSString *connectionPassword;

    /**
     Indicates that the connection is closing and no further responses should be processed.
     */
    BOOL closing;
        
    /** 
     The XML RPC manager object
     */
    XMLRPCConnectionManager *manager;
        
    /**
     indicates that the connection is suspending
     */
    BOOL suspending;

    /**
     indicates that the connection is suspended
     */
    BOOL suspended;

    /**
     indicates that the connection is resuming
     */
    BOOL resuming;
    
    NSArray* allHostRRDs;
    NSDate* allHostRetrievalTime;

}

#pragma mark -
#pragma mark Connection Methods
/*
 * Provides Access to the connection Address for the XenServer
  */
- (NSURL*) connectionAddress;

/*
 * Provides Access to the session ID for the connection to the XenServer
 */
- (NSString*) sessionID;

/*
 * Defines if the configuraton indicates that autoUpdate is enabled
 */
+ (BOOL) isAutoUpdateConfigured;

/*
 * Request a power operation for a VM.  No operation will be carried out if
 * the reference is not for a VM (e.g. it is for a template or a snapshot)
 * or the power operation requested is not one of the available ones
 * TODO - really should return some sort of error code in these circumstances
 */
- (void)RequestPowerOperation:(int)operation forVMReference:(NSString *)ref;

/*
 * Request a snapshot is taken
 * The VMRef must be of a VM (not a snapshot or template
 * Can return errors -- VM BAD POWER STATE, SR FULL, OPERATION NOT ALLOWED
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)RequestNewSnapshot:(NSString*)newName forVMReference:(NSString *)ref;

/*
 * Request a revert to snapshot
 * can return VM BAD POWER STATE, OPERATION NOT ALLOWED, SR FULL, VM REVERT FAILED
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
- (void)RequestRevertToSnapshotWithReference:(NSString *)snapshotRef;

/*
 * Request to delete an object of the specified type
 * can return ??????
 * TODO - really should be able to cope with the errors here (i.e. not shutdown etc..)
 */
-(void)RequestDestroyObjectType:(int)objectType WithReference:(NSString *)objectRef;

/**
 Request all data objects for a specific type from the XenHypervisor connection.  This is an async request.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObjectsForType:(int)hypObjectType;

/**
 Request specific data object for a specific type from the XenHypervisor connection.  This is an async request.
 @param reference The opaque reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType;

/**
 Request that the otherconfig data for a VM is updated  This is an async request.
 @param configSettings The dictionary of config settings that should be set.
 @param forVMRef The opaque reference for the VM object to requrest.
 */
- (void)SetVMOtherConfig:(NSDictionary*)configSettings forVMRef:(NSString*)objectRef;

/**
Request that the memory settings for the VM are updated  This is an async request.
@param forVMRef The opaque reference for the VM object to requrest.
@param static min
@param static max
@param dynamic min
@param dynamic max
*/
-(void) SetVMMemoryLimitsForVMRef:(NSString*)objectRef
                        staticMin:(long long)static_min
                        staticMax:(long long)static_max
                       dynamicMin:(long long)dynamic_min
                       dynamicMax:(long long)dynamic_max;
/**
 Request that the CPU count for the VM is updated  This is an async request.
 @param cpuCount The number of CPUs that should be set.
 @param forVMRef The opaque reference for the VM object to requrest.
 */
- (void)SetVMvCPUCount:(int)cpuCount forVMRef:(NSString*)objectRef;

- (void)SetVMvMaxCPUCount:(int)cpuCount forVMRef:(NSString*)objectRef;

-(void) SetVMNameForVMRef:(NSString*)objectRef name:(NSString*)name;

-(void) SetVMDescriptionForVMRef:(NSString*)objectRef name:(NSString*)description;

/**
 Close the connection.
 This will close the connection, by sending a request to stop listenting to events and sending a connection close to the hypervisor.
 The responses to these requests (and any other outstanding requests) will not be processed.  It is a assumed that the connection close
 will sucseed or if not time out at a later point.  There is no check that the close was compelted sucsesfully.
 */
- (void)closeConnection;

// gets all the permormance data for all hosts for the recent timeframe
// this is used for key performance metric information only.
// use a default value for how often this is retrived...
- (NSArray*) allHostPerformanceData;

#pragma mark -
#pragma mark Cache reading

@property BOOL closing;

@end

