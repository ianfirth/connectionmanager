//
//  XenHost_metrics.m
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHost_metrics.h"

@implementation XenHost_metrics

@synthesize totalMemory, freeMemory;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)hostMeticsProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_HOST_METRICS Reference:reference Dictionary:hostMeticsProperties];
    if (self){
        NSNumberFormatter * myNumFormatter = [[NSNumberFormatter alloc] init];
        [myNumFormatter setLocale:[NSLocale currentLocale]]; // happen by default?
        [myNumFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        // next line is very important!
        [myNumFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // crucial

        // totalMemory
        NSString *totalMemoryStr = [hostMeticsProperties valueForKey:@"memory_total"];  
        NSNumber *tempNum = [myNumFormatter numberFromString:totalMemoryStr];
        [self setTotalMemory : tempNum];

        // freeMemory
        NSString *freeMemoryStr = [hostMeticsProperties valueForKey:@"memory_free"];  
        tempNum = [myNumFormatter numberFromString:freeMemoryStr];
        [self setFreeMemory : tempNum];

        myNumFormatter = nil;  

    }
    return self;
}


@end
