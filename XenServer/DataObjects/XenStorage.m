//
//  XenStorage.m
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenStorage.h"


@implementation XenStorage
@synthesize tags , contentType , type , shared , physicalSize, physicalUtilization, virtualAllocation, local_cache_enabled;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)storgageProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_STORAGE Reference:reference Dictionary:storgageProperties];
    if (self){
        // localization allows other thousands separators, also.
        NSNumberFormatter * myNumFormatter = [[NSNumberFormatter alloc] init];
        [myNumFormatter setLocale:[NSLocale currentLocale]]; // happen by default?
        [myNumFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        // next line is very important!
        [myNumFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; // crucial

        // tags
        [self setTags : [storgageProperties objectForKey:@"tags"]];  
        // shared
        [self setShared : ([[storgageProperties valueForKey:@"shared"] isEqual:[NSNumber numberWithInt:1]])];
        
        // local cache enabled
        [self setLocal_cache_enabled:([[storgageProperties valueForKey:@"local_cache_enabled"] isEqual:[NSNumber numberWithInt:1]])];
        
        // usage
        NSString *tempStr = [storgageProperties objectForKey:@"physical_utilisation"];  
        NSNumber *tempNum = [myNumFormatter numberFromString:tempStr];
        [self setPhysicalUtilization : tempNum];
        // size
        tempStr = [storgageProperties objectForKey:@"physical_size"];  
        tempNum = [myNumFormatter numberFromString:tempStr];
        [self setPhysicalSize : tempNum];
        // allocation
        tempStr = [storgageProperties objectForKey:@"virtual_allocation"];  
        tempNum = [myNumFormatter numberFromString:tempStr];
        [self setVirtualAllocation : tempNum];
    
        // type
        [self setType : [storgageProperties objectForKey:@"type"]];
        // contenttype
        [self setContentType  : [storgageProperties objectForKey:@"content_type"]];
    }
    return self;
}



@end
