//
//  XenNetwork.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenBase.h"

@interface XenNIC : XenBase 

- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)networkProperties;

@property (copy) NSString *ip;
@property (copy) NSString *device;
@property (copy) NSString *mac; 

@end
