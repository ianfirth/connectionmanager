//
//  XenBase.m
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenBase.h"

@implementation XenBase

@synthesize connectionID, uuid, opaque_ref, references, objectType;

#pragma mark -
#pragma mark Predicates
// a predicate to define the object with the specified opaque reference
+ (NSPredicate *) XenBaseFor:(NSString *)reference{
     return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"opaque_ref like '%@'",reference]];
}

+ (NSPredicate *) XenBaseForUUID:(NSString *)uuid{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uuid like '%@'",uuid]];
}


+ (NSString*) ObjectNameForType:(int)hypObjectType{
    NSString* objectType = nil;
    
    if (hypObjectType & HYPOBJ_VM){   // for Xen this gets VMs, templates and snapshots
        objectType = @"HYPOBJ_VM";
    }
    if (hypObjectType & HYPOBJ_VM_CONTROL_DOMAIN){   // for Xen this gets VMs
        objectType = @"HYPOBJ_VM";
    }
    if (hypObjectType & HYPOBJ_TEMPLATE){   // for Xen this gets VMs, templates and snapshots
        objectType = @"HYPOBJ_TEMPLATE";
    }
    if (hypObjectType & HYPOBJ_SNAPSHOT){   // for Xen this gets VMs, templates and snapshots
        objectType = @"HYPOBJ_SNAPSHOT";
    }
    if (hypObjectType & HYPOBJ_HOST){
        objectType = @"HYPOBJ_HOST";
    }
    if (hypObjectType & HYPOBJ_HOST_CPU){
        objectType = @"HYPOBJ_HOST_CPU";
    }
    if (hypObjectType & HYPOBJ_STORAGE){
        objectType = @"HYPOBJ_STORAGE";
    }
    if (hypObjectType & HYPOBJ_NIC){
        objectType = @"HYPOBJ_NIC";
    }
    if (hypObjectType & HYPOBJ_NETWORK){
        objectType = @"HYPOBJ_NETWORK";
    }
    if (hypObjectType & HYPOBJ_HOST_METRICS){
        objectType = @"HYPOBJ_HOST_METRICS";
    }
    if (hypObjectType & HYPOBJ_PBD){
        objectType = @"HYPOBJ_PBD";
    }
    if (hypObjectType & HYPOBJ_VM_GUEST_METRICS){
        objectType = @"HYPOBJ_VM_GUEST_METRICS";
    }
    if (hypObjectType & HYPOBJ_VM_METRICS){
        objectType = @"HYPOBJ_VM_METRICS";
    }
    if (hypObjectType & HYPOBJ_VBD){
        objectType = @"HYPOBJ_VBD";
    }
    if (hypObjectType & HYPOBJ_VDI){
        objectType = @"HYPOBJ_VDI";
    }
    if (hypObjectType & HYPOBJ_CONSOLES){
        objectType = @"HYPOBJ_CONSOLES";
    }
    return objectType;
}

/**
 * provides a sort descriptor for each object type
 */
+ (NSSortDescriptor*) defaultSortForhypObjectType:(int)hypObjectType{
    switch (hypObjectType) {
        case HYPOBJ_HOST:
        case HYPOBJ_NETWORK:
        case HYPOBJ_STORAGE:
        case HYPOBJ_VM:
        case HYPOBJ_VDI:
            return [[NSSortDescriptor alloc] initWithKey:@"name_label" ascending:YES];
            break;
        case HYPOBJ_VBD:
            return [[NSSortDescriptor alloc] initWithKey:@"devicePositionName" ascending:YES];
        case HYPOBJ_NIC:
            return [[NSSortDescriptor alloc] initWithKey:@"device" ascending:YES];
            break;
        case HYPOBJ_SNAPSHOT:
            return [[NSSortDescriptor alloc] initWithKey:@"snapshot_time" ascending:YES];
            break;
        case HYPOBJ_HOST_METRICS:
        case HYPOBJ_HOST_CPU:
        case HYPOBJ_VM_GUEST_METRICS:
        case HYPOBJ_VM_METRICS:
        case HYPOBJ_PBD:
        default:
            return nil;
            break;
    }
}

- (id) initWithConnection:(XenHypervisorConnection *)xenConnection objectType:(int)hypObjectType Reference:(NSString *)reference Dictionary:(NSDictionary *)properties{
    self = [super init];
    if (self){
        objectType = hypObjectType;
        [self setOpaque_ref:reference];
        [self setUuid :[properties valueForKey:@"uuid"]];
        [self setReferences :[NSMutableDictionary dictionaryWithCapacity:0]];
        [self setConnectionID: [xenConnection connectionID]];
    }
    return self;
}

-(XenHypervisorConnection *) hypervisorConnection{
    return (XenHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_XEN connectonID:[self connectionID]];
}

- (void) addReference:(NSString *)opaqueReference forObjectType:(int)hypObjectType{
    if (opaqueReference != nil && 
        ![opaqueReference isEqualToString:@""] &&
        ![opaqueReference isEqualToString:@"OpaqueRef:NULL"]){
        NSNumber *key = [NSNumber numberWithInt:hypObjectType];
        if ([references objectForKey:key] == nil){
            NSMutableArray *refs = [NSMutableArray arrayWithCapacity:1];
            [references setObject:refs forKey:key];
        }
        
        NSMutableArray *existingRefs = [references objectForKey:key];
        [existingRefs addObject:opaqueReference];
    }
}

- (void) addReferences:(NSArray *)opaqueReferences forObjectType:(int)hypObjectType{
    NSNumber *key = [NSNumber numberWithInt:hypObjectType];
    if ([references objectForKey:key] == nil){
        NSMutableArray *refs = [NSMutableArray arrayWithCapacity:1];
        [references setObject:refs forKey:key];
    }
    NSMutableArray *existingRefs = [references objectForKey:key];
    // remove opaqueRef nulls here
    for (NSString *opaqueReference in opaqueReferences){
        if (![opaqueReference isEqualToString:@"OpaqueRef:NULL"] &&
            ![opaqueReference isEqualToString:@""]){
            [existingRefs addObject:opaqueReference];
        }    
    }
}

- (NSArray *) referencesForType:(int)hypObjectType{
    NSNumber *key = [NSNumber numberWithInt:hypObjectType];
    NSArray* existingRefs = [references objectForKey:key];
    return existingRefs;
}


#pragma mark HypObjReferencesProtocol
- (NSString *)uniqueReference{
    return opaque_ref;
}

-(NSString*) description{
    return [NSString stringWithFormat:@"%@:%@",[XenBase ObjectNameForType:[self objectType]], opaque_ref];
}

@end
