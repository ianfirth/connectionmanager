//
//  XenBase.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

// definition of the types of objects that can be stored in the cache
// these are flags so that they can be used in combination
#define HYPOBJ_VM 0x1
#define HYPOBJ_STORAGE 0x2
#define HYPOBJ_HOST 0x4
#define HYPOBJ_TEMPLATE 0x8
#define HYPOBJ_NIC 0x10
#define HYPOBJ_HOST_METRICS 0x20
#define HYPOBJ_NETWORK 0x40
#define HYPOBJ_PBD 0x80
#define HYPOBJ_SNAPSHOT 0x100
#define HYPOBJ_VM_GUEST_METRICS 0x200
#define HYPOBJ_VM_METRICS 0x400
#define HYPOBJ_HOST_CPU 0x800
#define HYPOBJ_VBD 0x1000
#define HYPOBJ_VDI 0x2000
#define HYPOBJ_CONSOLES 0x4000
#define HYPOBJ_VM_CONTROL_DOMAIN 0x8000
#define HYPOBJ_POOL 0x10000

#import "XenHypervisorConnection.h"
#import "HypObjReferencesProtocol.h"

/**
 Class to represent a base XenServer object.
 A great deal of the objects in XenServer API contain common properties.  This class abstracts the common ones
 and forma a base for other objects to extend.
 
 Objects that are representing Xen Hypervisor objects should not hold onto references to other objects, they
 should store the opaque_reference for the objects and access them back via the connection which has a cache of
 all these objects.  This allows for memory to be freed if required and the object to be refetched on demand.
 */
@interface XenBase : NSObject<HypObjReferencesProtocol> 
  
/**
 Create a new instance of the class with the specified properties dictionary.
 @param vmProperties The dictionary of properties retreived via the XenServer API for the object.
 @returns a newly initialized NSPredicate
 */
- (id) initWithConnection:(XenHypervisorConnection *)xenConnection objectType:(int)hypObjectType Reference:(NSString *)reference Dictionary:(NSDictionary *)hostProperties;

/**
  Store a referenece to another Xen object opaque_ref
  @param opaqueReference The opaque reference for the object being referenced.
  @param objectType The type of hypervisor object the reference is for.
 */
- (void) addReference:(NSString *)opaqueReference forObjectType:(int)objectType;

/**
 Store refereneces to other Xen object opaque_refs
 @param opaqueReferences The array of opaque references for the object being referenced.
 @param objectType The type of hypervisor object the references are for.
 */
- (void) addReferences:(NSArray *)opaqueReferences forObjectType:(int)objectType;

/**
 Retreive a referenec to another Xen object opaque_refs
 @param objectType The type of hypervisor object reference to be retreived.
 */
- (NSArray *) referencesForType:(int)objectType;

/**
 Create a Predicate that defines a specific opaque reference.
 @param reference The opaque reference to predicate on
 @returns the new predeicate
 */
+ (NSPredicate *) XenBaseFor:(NSString *)reference;

/**
 Create a Predicate that defines a specific uuid.
 @param the uuid reference to predicate on
 @returns the new predeicate
 */
+ (NSPredicate *) XenBaseForUUID:(NSString *)uuid;

/**
 * provides a sort descriptor for each object type
 */
+ (NSSortDescriptor*) defaultSortForhypObjectType:(int)hypObjectType;

/**
 * provides a helper for converting the object type to a string
 * This is primarily for debugging aid and not for display to user.
 */
+ (NSString*) ObjectNameForType:(int)hypObjectType;

/**
 returns the hpervisor connection for the object.
 @returns XenHypervisorConnection
 */
-(XenHypervisorConnection *) hypervisorConnection;

/**
 The identifier for the object for its entire lifetime.
 */
@property (copy) NSString *uuid; 

/**
 The identifier for the object in this session.
 */
@property (copy) NSString *opaque_ref;

/**
 The Dictionary of other objects that this object references and requires to be considered complete.
 This is keyed on the hypervisor object type and contains an NSMutable array of objects.  This should not store
 all references, otherwise when one object is loaded the tree will be very large and end up pulling in
 all objects into the graph.
 */
@property (strong) NSMutableDictionary *references; 

/**
 The type of the object
 */
@property (readonly) int objectType; 

/**
 The identifier for the hypervisor Connection that this object was creted from.
 */
@property (copy) NSString *connectionID;

@end
