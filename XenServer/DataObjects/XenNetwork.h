//
//  XenNetwork.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"


// TODO need to store the list of VIFs and PIFs for the Network.

@interface XenNetwork : XenDescriptiveBase

- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)networkProperties;

/** 
 * identifies if the netwoek is a GuestInstallerNetwork.  These are internal to Xen and generally not
 * required to be seen in any user interface
 * @return YES id the is_guest_installer_network property in the other_config settings are set.
 */
- (BOOL) isGuestInstallerNetwork;

/**
 The Dictionary of other config data for the Network.
 */
@property (strong) NSDictionary *other_config;


@end
