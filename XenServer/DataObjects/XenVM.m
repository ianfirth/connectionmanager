//
//  VM.m
//  hypOps
//
//  Created by Ian Firth on 26/02/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVM.h"

/**
 Adds some convenience methods to the XenVM class for internal use only
 */
@interface XenVM ()
/**
 Create a XenPowerState enum from the NSString that the XenAPI provides
 @param powerStateString The power state string representation
 @returns The XenPowerState enum value for the string.
 */
- (XenPowerState) powerStateFromString:(NSString *)powerStateString;
@end

@implementation XenVM

@synthesize vmType, power_state, tags , is_control_domain, other_config, bios_strings, boot_params, allowed_operations;
@synthesize memory_dynamic_max, memory_dynamic_min, memory_overhead, memory_static_max, memory_static_min, memory_target;
@synthesize snapshots;
@synthesize snapshot_time, parent;
@synthesize VCPUs_at_startup, VCPUs_max;

#pragma mark -
#pragma mark Predicates
// a predicate to define the VMs in the specified state
+ (NSPredicate *) VM_PowerStateFor:(XenPowerState)powerState{
    NSNumber *stateValue =  [NSNumber numberWithInt:powerState];
    return [NSPredicate predicateWithFormat:@"power_state == %@",stateValue];
}

+ (NSPredicate *) VM_Tagged{
    return [NSPredicate predicateWithFormat:@"tags_count > 0"];
}

+ (NSPredicate *) VM_TaggedWith:(NSString*)tag{
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        XenVM *vm = (XenVM *)evaluatedObject;
        int count = 0;
        while (count < [[vm tags] count]){
            NSString* vmTag = [[vm tags] objectAtIndex:count];
            if ([vmTag isEqualToString:tag])
            {
                return YES;
            }
            count ++;
        }
        return NO;
    }];
}

- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vmProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_VM Reference:reference Dictionary:vmProperties];
    if (self){
        // is_a_snapshot available since v5.0
        // is_a_template available since v4.0
        if ([vmProperties valueForKey:@"is_a_snapshot"] && [[vmProperties valueForKey:@"is_a_snapshot"] isEqual:[NSNumber numberWithInt:1]]){  
            [self setVmType:XENVMTYPE_SNAPSHOT];
            [self setSnapshot_time:[vmProperties valueForKey:@"snapshot_time"]];
        }
        else if ([vmProperties valueForKey:@"is_a_snapshot"] && [[vmProperties valueForKey:@"is_a_template"] isEqual:[NSNumber numberWithInt:1]]){
            [self setVmType:XENVMTYPE_TEMPLATE];
        }
        else{
            [self setVmType:XENVMTYPE_VM];
        }
        
        // set the control domain property available since v4.0
        [self setIs_control_domain : [[vmProperties valueForKey:@"is_control_domain"] isEqual:[NSNumber numberWithInt:1]]];
        
        // power_state
        [self setPower_state : [self powerStateFromString:[vmProperties valueForKey:@"power_state"]]];
        
        // Memory values ( all available since v4
        if ([[vmProperties allKeys] containsObject:@"memory_dynamic_max"]){
            [self setMemory_dynamic_max:[vmProperties valueForKey:@"memory_dynamic_max"]];
        }

        if ([[vmProperties allKeys] containsObject:@"memory_dynamic_min"]){
            [self setMemory_dynamic_min:[vmProperties valueForKey:@"memory_dynamic_min"]];
        }
        
        if ([[vmProperties allKeys] containsObject:@"memory_overhead"]){
            [self setMemory_overhead:[vmProperties valueForKey:@"memory_overhead"]];
        }

        if ([[vmProperties allKeys] containsObject:@"memory_static_max"]){
            [self setMemory_static_max:[vmProperties valueForKey:@"memory_static_max"]];
        }

        if ([[vmProperties allKeys] containsObject:@"memory_static_min"]){
            [self setMemory_static_min:[vmProperties valueForKey:@"memory_static_min"]];
        }

        if ([[vmProperties allKeys] containsObject:@"memory_target"]){
            [self setMemory_target:[vmProperties valueForKey:@"memory_target"]];
        }

        if ([[vmProperties allKeys] containsObject:@"parent"]){
            [self setParent:[vmProperties valueForKey:@"parent"]];
        }
        
        // VM CPU information all available since v4
        [self setVCPUs_at_startup:[[vmProperties valueForKey:@"VCPUs_at_startup"] intValue]];
        [self setVCPUs_max:[[vmProperties valueForKey:@"VCPUs_max"] intValue]];

        // set the control domain property available since v5.0
        [self setTags : [vmProperties objectForKey:@"tags"]];  
        // other_config
        [self setOther_config : [vmProperties objectForKey:@"other_config"]];
        // bios_strings
        [self setBios_strings : [vmProperties objectForKey:@"bios_strings"]];
        // bios_strings
        [self setBoot_params:[vmProperties objectForKey:@"HVM_boot_params"]];
        // allowed_operations
        [self setAllowed_operations: [vmProperties objectForKey:@"allowed_operations"]];
        //snapshots
        [self addReferences:[vmProperties valueForKey:@"snapshots"] forObjectType:HYPOBJ_SNAPSHOT];
        //vm guest metrics
        [self addReference:[vmProperties valueForKey:@"guest_metrics"] forObjectType:HYPOBJ_VM_GUEST_METRICS];
        // vbds
        [self addReferences:[vmProperties valueForKey:@"VBDs"] forObjectType:HYPOBJ_VBD];
        // vbds
        [self addReference:[vmProperties valueForKey:@"metrics"] forObjectType:HYPOBJ_VM_METRICS];
        // host
        [self addReference:[vmProperties valueForKey:@"resident_on"] forObjectType:HYPOBJ_HOST];
        // consoles
        [self addReferences:[vmProperties objectForKey:@"consoles"] forObjectType:HYPOBJ_CONSOLES];
    }    
    return self;
}

- (void) dealloc{
    if (vmRRD){
        vmRRD = nil;
    }
    
    lastPreformanceDataUpdateTime = nil;
}

/**
 Obtains the Performance data for the VM from the server
 */
- (RoundRobinDatabase*) vmPerformanceData{
    if (vmRRD == nil){
        [self refreshPerformanceData];
    }
    return vmRRD;
}

- (void) clearVmPerformanceData{
    lastPreformanceDataUpdateTime = nil;
    vmRRD = nil;
}

- (NSDate*) lastPreformanceDataUpdateTime{
    return lastPreformanceDataUpdateTime;
}

-(BOOL) refreshPerformanceData{
    // keep last 5 mins separatly for everythig else that way no need to keep all data for all VMs I dont think right now
    // clear other VM performance data so as not to over use the memory
    NSArray* vms = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_VM];
    for (XenVM* vm in vms) {
        NSString* opaqueRef = [vm opaque_ref];
        if (![opaqueRef isEqualToString:[self opaque_ref]]){
            [vm clearVmPerformanceData];
        }
    }

    vmRRD = nil;
    // reload the performance data from the Host 
    // build the URL here
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_XEN];
    NSDictionary* connections = [[XenHypervisorConnection connectionDictionary] objectForKey:hypervisorId];
    hypervisorId = nil;
    NSString* theConnectionID = [self connectionID];
    XenHypervisorConnection* connection = [connections objectForKey:theConnectionID]; 
    NSURL* connectionAddress = [connection connectionAddress];
    NSString* host = [connectionAddress host];
    NSNumber* port = [connectionAddress port];
    
    //wget http://<server>/vm_rrd?session_id=OpaqueRef:<SESSION HANDLE>&uuid=<VM UUID>
    NSString *URLString;
    if (port){
        URLString = [NSString stringWithFormat:@"http://%@:%@/vm_rrd?session_id=%@&uuid=%@",host,port,[connection sessionID], [self uuid]];
    }else
    {
        URLString = [NSString stringWithFormat:@"http://%@/vm_rrd?session_id=%@&uuid=%@",host,[connection sessionID],[self uuid]];
    }
    @try{
        vmRRD = [[RoundRobinDatabase alloc] initWithUrl:[NSURL URLWithString:URLString] withFullDataSet:NO];
    }
    @catch (id theException) {
		NSLog(@"Error loading Host RRD: %@", theException);
        vmRRD = nil;
        lastPreformanceDataUpdateTime = nil;
        return NO;
    }
    lastPreformanceDataUpdateTime = [NSDate date];
    
    return YES;
}

- (NSUInteger) tags_count{
    return [[self tags] count];
}

- (BOOL) name_descriptionIsNilOrEmpty{
    return ([self name_description] == nil || [[self name_description] isEqual:@""]);
}

- (BOOL) auto_power_on{
    return ([[[self other_config] valueForKey:@"auto_poweron"] isEqual:@"true"]);
}

- (BOOL) are_bios_strings_set{
    if ([self bios_strings] == nil){
        return NO;
    }
    return ([[[self bios_strings] allKeys] count] >0);
}

- (BOOL) is_bios_reflected_inVM{
    NSString* biosVendor = [[self bios_strings] valueForKey:@"bios-vendor"];
    if (biosVendor == nil || [biosVendor isEqualToString:@"Xen"]){
        return NO;
    }
    return YES;
}


- (NSArray *) bootOrder{
    NSMutableArray *result = nil;
    if ([self boot_params] != nil){
        result = [NSMutableArray arrayWithCapacity:3];
        NSString *bootOrder = [boot_params valueForKey:@"order"];
        for (int i=0 ; i <[bootOrder length]; i ++){
            switch ([bootOrder characterAtIndex:i]) {
                case 'n':
                    [result addObject:@"Network"];
                    break;
                case 'c':
                    [result addObject:@"Hard-Disk"];
                    break;
                case 'd':
                    [result addObject:@"DVD Drive"];
                    break;
               default:
                    break;
            }
            
        }
    }
    return result;
}

-(bool) isMemoryDynamic{
    return ([[self memory_static_max] longLongValue] != [[self memory_dynamic_max] longLongValue]) ||
    ([[self memory_static_max] longLongValue] != [[self memory_dynamic_min] longLongValue]);
}


- (NSSet *) availablePowerOperations{
    NSMutableSet *availableOps = [[NSMutableSet alloc] initWithCapacity:1];
   
    for (NSString *op in [self allowed_operations]) {
        if ([op isEqualToString:@"hard_reboot"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_RESTART_FORCE]];
        } 
        else if ([op isEqualToString:@"clean_reboot"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_RESTART_CLEAN]];  
        }
        else if ([op isEqualToString:@"hard_shutdown"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_STOP_FORCE]];
        }
        else if ([op isEqualToString:@"clean_shutdown"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_STOP_CLEAN]];  
        }
        else if ([op isEqualToString:@"suspend"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_SUSSPEND]];
        }
        else if ([op isEqualToString:@"start"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_START]];
        }
        else if ([op isEqualToString:@"resume"]){
            [availableOps addObject:[NSNumber numberWithInt:XENPOWEROPERATION_RESUME]];
        }
    }

    return availableOps;
}

- (NSArray *) snapshots{
    return [self referencesForType:HYPOBJ_SNAPSHOT];
}

#pragma mark -
#pragma mark Private Methods

/*
 * obtain the powerState enum from the XenServer string representation
 */
- (XenPowerState) powerStateFromString:(NSString *)powerStateString{
    if ([powerStateString isEqual:@"Running"]){
        return XENPOWERSTATE_RUNNING;
    }
    if ([powerStateString isEqual:@"Halted"]){
        return XENPOWERSTATE_HALTED;
    }
    if ([powerStateString isEqual:@"Suspended"]){
        return XENPOWERSTATE_SUSPENDED;
    }
    if ([powerStateString isEqual:@"Paused"]){
        return XENPOWERSTATE_PAUSED;
    }

    // should never get to this
    return XENPOWERSTATE_HALTED;
}
@end
