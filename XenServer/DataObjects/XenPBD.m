//
//  XenPBD.m
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenPBD.h"

@implementation XenPBD

@synthesize currentlyAttached;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)pbdProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_PBD Reference:reference Dictionary:pbdProperties];
    if (self){
        // currentlyAttached
        currentlyAttached = ([[pbdProperties valueForKey:@"currently_attached"] isEqual:[NSNumber numberWithInt:1]]);
        //SR 
        [self addReference:[pbdProperties valueForKey:@"SR"] forObjectType:HYPOBJ_STORAGE];        
        //HOST 
        [self addReference:[pbdProperties valueForKey:@"host"] forObjectType:HYPOBJ_HOST];        
    }
    return self;
}


@end
