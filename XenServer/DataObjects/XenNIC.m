//
//  XenNetwork.m
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenNIC.h"


@implementation XenNIC

@synthesize ip, device, mac;

- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)networkProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_NIC  Reference:reference Dictionary:networkProperties];
    if (self){
        // IP Address (can be nil if the card is not enabled)
        NSString *ipAddress = [networkProperties valueForKey:@"IP"];
        if (!ipAddress){
           ipAddress = @"";
        }
        [self setIp:ipAddress];
        // device name
        [self setDevice:[networkProperties valueForKey:@"device"]];
        // MAC address
        [self setMac:[networkProperties valueForKey:@"MAC"]];
    }
    return self;
}

@end
