//
//  XenVBD.m
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVBD.h"


@implementation XenVBD

@synthesize type, devicePositionName, device;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vbdProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_VBD Reference:reference Dictionary:vbdProperties];
    if (self){
        NSNumberFormatter * myNumFormatter = [[NSNumberFormatter alloc] init];
        [myNumFormatter setLocale:[NSLocale currentLocale]]; 
        [myNumFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        // next line is very important!
        [myNumFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; 
        
        // type
        [self setType:[vbdProperties objectForKey:@"type"]];

        // device
        [self setDevice:[vbdProperties objectForKey:@"device"]];

        // position
        // if its empty then put pending (needs reboot to take affect)
        // read this. If its a number then change it to hdx (where x=a for 0 etc.)
        // when reading these order them by hd first then sd and see if that matches
        // display 0 to x on the screen
        // do the same for hte disks too.
        NSString* initialStr = [vbdProperties objectForKey:@"userdevice"];
        NSString *result = initialStr;
        // if its a number change it to an hdx format
        NSNumber *tempNum = [myNumFormatter numberFromString:initialStr];
        if (tempNum){
            // it is a number so deal with it and convert to hd?
            char newc = (0x61+[tempNum intValue]);
            result = [NSString stringWithFormat:@"hd%c", newc];
        }
        [self setDevicePositionName:result];
        
        // VDI
        [self addReference:[vbdProperties valueForKey:@"VDI"] forObjectType:HYPOBJ_VDI];
    }
    return self;
}


@end
