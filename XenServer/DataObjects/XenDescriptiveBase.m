//
//  XenDescriptiveBase.m
//  hypOps
//
//  Created by Ian Firth on 13/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"


@implementation XenDescriptiveBase

@synthesize name_label, name_description;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection objectType:(int)hypObjectType Reference:(NSString *)reference Dictionary:(NSDictionary *)properties{
    self = [super initWithConnection:xenConnection objectType:hypObjectType Reference:reference Dictionary:properties];
    if (self){
        [self setName_label : [properties valueForKey:@"name_label"]];
        [self setName_description : [properties valueForKey:@"name_description"]];
    }
    return self;
}

+ (NSPredicate *) nameBeginsWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name_label beginsWith [c]'%@'",name]];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name_label beginsWith [c]'%@' OR name_label contains [c]' %@'",name,name]];
}


@end
