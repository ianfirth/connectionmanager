//
//  XenVBD.h
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenBase.h"

@interface XenVBD : XenBase 

@property (strong) NSString *type;
@property (strong) NSString *devicePositionName;
@property (copy) NSString *device;

@end
