//
//  XenConsole.m
//  hypOps
//
//  Created by Ian Firth on 24/10/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "XenConsole.h"

@implementation XenConsole

@synthesize protocol, location;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vbdProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_CONSOLES Reference:reference Dictionary:vbdProperties];
    if (self){
        // protocol
        [self setProtocol:[vbdProperties objectForKey:@"protocol"]];
        // location
        [self setLocation:[vbdProperties objectForKey:@"location"]];
    }
    return self;
}


@end
