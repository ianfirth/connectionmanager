//
//  NetworkAddress.m
//  hypOps
//
//  Created by Ian Firth on 29/09/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "NetworkAddress.h"

@implementation NetworkAddress

@synthesize address,deviceID;

- (id)initWithDeviceID:(int)theDeviceID andAddress:(NSString*) theAddress
{
    self = [super init];
    if (self) {
        // Initialization code here.
        deviceID = theDeviceID;
        address = [theAddress copy];
    }
    
    return self;
}

@end
