//
//  XenVM_metrics.h
//  hypOps
//
//  Created by Ian Firth on 22/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "XenBase.h"

@interface XenVM_metrics : XenBase

@property  NSDate* start_time;

@property  NSNumber* memory_actual;

@end
