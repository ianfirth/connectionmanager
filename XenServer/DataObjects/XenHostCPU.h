//
//  XenHostCPU.h
//  hypOps
//
//  Created by Ian Firth on 19/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenBase.h"

@interface XenHostCPU : XenBase 
/**
 The cpu vendor.
 */
@property (copy) NSString *vendor; 

/**
 The cpu model.
 */
@property (copy) NSString *modelName; 

@end
