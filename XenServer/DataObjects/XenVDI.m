//
//  XenVDI.m
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVDI.h"


@implementation XenVDI

@synthesize virtual_size, read_only, location;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vdiProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_VDI Reference:reference Dictionary:vdiProperties];
    if (self){
        NSNumberFormatter * myNumFormatter = [[NSNumberFormatter alloc] init];
        [myNumFormatter setLocale:[NSLocale currentLocale]]; 
        [myNumFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        // next line is very important!
        [myNumFormatter setNumberStyle:NSNumberFormatterDecimalStyle]; 

        // virtual size
        NSString *tempStr = [vdiProperties objectForKey:@"virtual_size"];  
        NSNumber *tempNum = [myNumFormatter numberFromString:tempStr];
        [self setVirtual_size : tempNum];

        // location
        [self setLocation:[vdiProperties valueForKey:@"location"]];
        // readonly
        [self setRead_only:([[vdiProperties valueForKey:@"read_only"] isEqual:[NSNumber numberWithInt:1]])];
        
        // SR
        [self addReference:[vdiProperties valueForKey:@"SR"] forObjectType:HYPOBJ_STORAGE];

    }
    return self;
}


@end
