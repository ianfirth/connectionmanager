//
//  XenVDI.h
//  hypOps
//
//  Created by Ian Firth on 23/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"

@interface XenVDI : XenDescriptiveBase

/**
 The size of the disk (in bytes)
 */
@property (strong) NSNumber *virtual_size;
/**
 Indicates if this disk is readonly
 */
@property BOOL read_only;
/**
 Indicates the disk locaton
 */
@property (copy) NSString *location;

@end
