//
//  VM.h
//  hypOps
//
//  Created by Ian Firth on 26/02/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"
#import "RoundRobinDatabase.h"

/**
  Enum to represent the differnt types of VM object available 
 */
typedef enum {
    XENVMTYPE_VM,          // This is a VM
    XENVMTYPE_SNAPSHOT,    // This is a snapshot
    XENVMTYPE_TEMPLATE     // This is a template
} XenVMType;


/**
 Enum to represent the different VM Power States available 
 */
typedef enum {
    XENPOWERSTATE_HALTED,    // the VM is halted
    XENPOWERSTATE_PAUSED,    // the VM is paused
    XENPOWERSTATE_RUNNING,   // the VM is running
    XENPOWERSTATE_SUSPENDED  // the VM is suspended
} XenPowerState;


/**
 Enum to represent the different VM Power States available 
 */
typedef enum {
    XENPOWEROPERATION_START,          // VM can be started
    XENPOWEROPERATION_STOP_CLEAN,     // VM can be stopped cleanly (requires tools installed)
    XENPOWEROPERATION_STOP_FORCE,     // VM can be stopped forcably
    XENPOWEROPERATION_SUSSPEND,       // VM can be susspended
    XENPOWEROPERATION_RESUME,         // VM can be resumed
    XENPOWEROPERATION_RESTART_CLEAN,  // VM can be restarted cleanly (required tools to be installed)
    XENPOWEROPERATION_RESTART_FORCE,  // VM can be restarted forcibly
} XenPowerOperation;


/**
 Class to represent a Virtual Machine stored in a XenServer hypervisor.
 In a XenServer, templates and snapshots are also considered to be VM objects aswell as VMs.
 Objects of this class contain details read from the objects in the hypervisor.
 */
@interface XenVM : XenDescriptiveBase{
    
    // VCPU data
    int VCPUs_at_startup;
    int VCPUs_max;
    
    /**
     The RED for the VM 
     This stores the performance data for the VM
     */
    RoundRobinDatabase* vmRRD;
    
    /**
     The date the that performance data was loaded/requested
     */
    NSDate* lastPreformanceDataUpdateTime;
    
}
/**
 Create a Predicate that defines a specific VM power state.
 @param powerState The power state to predicate on
 @returns a newly initialized NSPredicate
 */
+ (NSPredicate *) VM_PowerStateFor:(XenPowerState)powerState;

/**
 Create a Predicate that defines if a VM is has any tags associated with it.
 @returns a newly initialized NSPredicate
 */
+ (NSPredicate *) VM_Tagged;

/**
 Create a Predicate that defines if a VM is has the specified tag associated with it.
 @returns a newly initialized NSPredicate
 */
+ (NSPredicate *) VM_TaggedWith:(NSString*)tag;

/**
 Create a newly initialized object from the defined properties.
 @vmProperties The dictionary of properties read from a XenServer.  This is expected
               to be in the structure returned from the xmlRpc response from the Xen API.
 @returns a newly initialized XenVM object.
 */
- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vmProperties;

/**
 Provides the number of tags that the VM has associated with it.
 @returns The number of tags
 */
- (NSUInteger) tags_count;

/**
 indicates if the VM description is not populated
 @returns YES if the VM description is empty otherwise returns NO
 */
- (BOOL) name_descriptionIsNilOrEmpty;

/**
 indicates if the VM is configured to auto power on when the host is started.
 @returns YES if the VM is configured to auto start otherwise returns NO
 */
- (BOOL) auto_power_on;

/**
 indicates if the VM has any bios settings */
- (BOOL) are_bios_strings_set;

/**
 indicates if the VM is confiured to pass though the bios strings from the host.
 @returns YES if the VM is configured to pass the bios strings though to the VM otherwise returns NO
 */
- (BOOL) is_bios_reflected_inVM;

/**
 Returns a set of XenPowerOperation values indicating the available power transitions
 @returns set of XenPowerOperation
 */
- (NSSet *) availablePowerOperations;

/**
 Returns an array of strings for the boot order
 @returns array of boot order strings.
 */
- (NSArray *) bootOrder;

/**
 * provides the performance data for the VM
 * This is a lazy read, so if this has not been requested before then
 * reading this will request the data from the host.
 * If the data has already been read then the data will contain the previously read data
 */
- (RoundRobinDatabase *)vmPerformanceData;

- (void) clearVmPerformanceData;

- (NSDate*) lastPreformanceDataUpdateTime;

-(BOOL) refreshPerformanceData;

// returns true if the VM has balooning enabled.
-(bool) isMemoryDynamic;

/**
 The type of objects, this can be a VM, Template or a snapshot.
 */
@property XenVMType vmType; 

/**
 The current power state of the VM.
 */
@property XenPowerState power_state;

/**
 The Array of tags that are associated with the VM.
 */
@property (strong) NSArray *tags;

/**
 Indicates if the VM is a control VM (these are not shown in the UI).
 */
@property BOOL is_control_domain;

/**
 The Dictionary of other config data for the VM.
 */
@property (strong) NSDictionary *other_config;

/**
 The bios strings for the VM.
 */
@property (strong) NSDictionary *bios_strings;

/**
 The boot params for the VM.
 */
@property (strong) NSDictionary *boot_params;

/**
 The allowed operations for the VM.
 */
@property (strong) NSArray *allowed_operations;
@property (strong) NSNumber* memory_dynamic_max;
@property (strong) NSNumber* memory_dynamic_min;
@property (strong) NSNumber* memory_overhead;
@property (strong) NSNumber* memory_static_max;
@property (strong) NSNumber* memory_static_min;
@property (strong) NSNumber* memory_target;
@property int VCPUs_at_startup;
@property int VCPUs_max;
@property (readonly, strong) NSArray* snapshots;

/** 
 The time that the snapshot was taken (only relevent for snapshots
 */
@property (strong) NSDate* snapshot_time;
/**
 The reference to the parent snapshot
 */
@property (strong) NSString* parent;

@end
