//
//  XenStorage.h
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"

@interface XenStorage : XenDescriptiveBase

@property (strong) NSArray *tags;
@property (copy) NSString *contentType;
@property (copy) NSString *type;
@property BOOL shared;
@property (strong) NSNumber *physicalSize;
@property (strong) NSNumber *physicalUtilization;
@property (strong) NSNumber *virtualAllocation;
@property BOOL local_cache_enabled;
@end
