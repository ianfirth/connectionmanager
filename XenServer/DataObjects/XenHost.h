//
//  XenHost.h
//  hypOps
//
//  Created by Ian Firth on 01/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenDescriptiveBase.h"
#import "XenHypervisorConnection.h"
#import "RoundRobinDatabase.h"

@interface XenHost : XenDescriptiveBase {
   
    RoundRobinDatabase* hostRRD;
    NSDate* lastPreformanceDataUpdateTime;
}

/**
 Create a newly initialized object from the defined properties.
 @vmProperties The dictionary of properties read from a XenServer.  This is expected
 to be in the structure returned from the xmlRpc response from the Xen API.
 @returns a newly initialized XenHost object.
 */
- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)hostProperties;

/**
 indicates if the host is in maintanance mode */

-(BOOL) isInMaintananceMode;

/**
 indicates if there is an SR attached to the host for intellicahce
 */
-(BOOL) hasCacheStoreage;

/**
 provides the iScsi iqn for the host
 */
- (NSString*) iscsi_iqn;

/**
 provides the time the host was last started
 */
- (NSDate *) serverUpTime;

/**
 provides the time the tools stack was last started on the host
 */
- (NSDate *) toolStackUpTime;

/**
 provides the product version string for the host.
 This reads the 'product_version_text_short' value from the software version.
 */
-(NSString *) productVersionText;

/**
 provides the product version string for the host.
 This reads the 'product_version' value from the software version.
 This is only available in 5.6 or later.
 */
-(NSString *) productVersionNumber;

/**
 provides the build number string for the host.
 This reads the 'build_number' value from the software version.
 */
-(NSString *) buildNumber;

/**
 provides the license edtition string for the host.
 This reads the 'sku_marketing_name' value from the license params.
 */
-(NSString *) licenseEdition;

/**
 provides the license expiry date.
 This reads the 'expiry' value from the license params.
 */
- (NSDate *) licenseExipryDate;

/**
 provides the build number string for the host.
 This reads the 'address' value from the license server.
 */
-(NSString *) licenseAddress;

/**
 provides the build number string for the host.
 This reads the 'port' value from the license server.
 */
-(NSString *) licensePort;

/**
 provides the count of CPUs for the host.
 This reads the 'cpu_count' value from the cpu info.
 */
-(NSString *) cpuCount;

/**
 provides the vendor of CPUs for the host.
 This reads the 'vendor' value from the cpu info.
 */
-(NSString *) cpuVendor;

/**
 provides the model of CPUs for the host.
 This reads the 'modelName' value from the cpu info.
 */
-(NSString *) cpuModel;

/**
 provides the total amount of memory in the host.
 This reads the 'memory_total' value form hostMetrics object.
 */
-(NSNumber *) totalMemory;

/**
 provides the amount of free memory in the host.
 This reads the 'memory_free' value form hostMetrics object.
 */
-(NSNumber *) freeMemory;

/**
 * provides the performance data for the host
 * This is a lazy read, so if this has not been requested before then
 * reading this will request the data from the host.
 * If the data has already been read then the data will contain the previously read data
 */
- (RoundRobinDatabase *)hostPerformanceData;

- (void) clearHostPerformanceData;

- (NSDate*) lastPreformanceDataUpdateTime;

-(BOOL) refreshPerformanceData;

+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat;

/**
 The hostname for the Host.
 */
@property (copy) NSString *hostName;

/**
 The IP Address for the Host.
 */
@property (copy) NSString *address;

/**
 The Array of tags that are associated with the Host.
 */
@property (strong) NSArray *tags;

/**
 The Dictionary of other config data for the Host.
 */
@property (strong) NSDictionary *other_config;

/**
 The Dictionary of software version data for the Host.
 this contains the following structure:
 "build_number" = 39265p;
 date = "2010-11-30";
 dbv = "2010.0521";
 "hg_id" = "";
 hostname = "skinny-2";
 linux = "2.6.32.12-0.7.1.xs5.6.100.307.170586xen";
 "network_backend" = bridge;
 "product_brand" = XenServer;
 "product_version" = "5.6.100";
 "product_version_text" = "5.6 Feature Pack 1";
 "product_version_text_short" = "5.6 FP1";
 xapi = "1.3";
 xen = "3.4.2";
 "xencenter_max" = "1.8";
 "xencenter_min" = "1.8";
 "xs:main" = "Base Pack, version 5.6.100, build 39265p";
 "xs:xenserver-transfer-vm" = "XenServer Transfer VM, version 5.6.100, build 39215p";
 */
@property (strong) NSDictionary *software_version;

/**
 The Dictionary of license server data for the Host.
 this contains the following structure:
 address = "camesvwcls01.eng.citrite.net";
 port = 27000;
 */
@property (strong)NSDictionary *license_server;

/**
 The Dictionary of license params data for the Host.
 this contains the following structure:
 address1 = "";
 address2 = "";
 city = "";
 company = "";
 country = "";
 "enable_xha" = true;
 expiry = "20300101T00:00:00Z";
 grace = no;
 name = "";
 "platform_filter" = false;
 postalcode = "";
 productcode = "";
 "regular_nag_dialog" = false;
 "restrict_checkpoint" = false;
 "restrict_connection" = false;
 "restrict_cpu_masking" = false;
 "restrict_dmc" = false;
 "restrict_email_alerting" = false;
 "restrict_equalogic" = false;
 "restrict_historical_performance" = false;
 "restrict_lab" = false;
 "restrict_marathon" = false;
 "restrict_netapp" = false;
 "restrict_pool_attached_storage" = false;
 "restrict_pooling" = false;
 "restrict_qos" = false;
 "restrict_rbac" = false;
 "restrict_stage" = false;
 "restrict_storagelink" = false;
 "restrict_storagelink_site_recovery" = false;
 "restrict_vlan" = false;
 "restrict_vmpr" = false;
 "restrict_vswitch_controller" = false;
 "restrict_wlb" = false;
 serialnumber = "";
 "sku_marketing_name" = "Citrix XenServer Platinum Edition";
 "sku_type" = platinum;
 sockets = 1;
 state = "";
 version = "5.6.100";
 */
@property (strong)NSDictionary *license_params;

/**
 The Dictionary of CPU info data for the Host.
 this contains the following structure:
 "cpu_count" = 2;
 family = 6;
 features = "009ce3bd-bfebfbff-00000001-28100800";
 "features_after_reboot" = "009ce3bd-bfebfbff-00000001-28100800";
 flags = "fpu de tsc msr pae mce cx8 apic sep mtrr mca cmov pat clflush acpi mmx fxsr sse sse2 ss ht nx constant_tsc nonstop_tsc aperfmperf pni vmx est ssse3 sse4_1 sse4_2 popcnt hypervisor tpr_shadow vnmi flexpriority ept vpid";
 maskable = full;
 model = 26;
 modelname = "Intel(R) Xeon(R) CPU           W3503  @ 2.40GHz";
 "physical_features" = "009ce3bd-bfebfbff-00000001-28100800";
 speed = "2400.000";
 stepping = 5;
 vendor = GenuineIntel;
 
 */
@property (strong) NSDictionary *cpu_info;

/** stores the reference string for the local cache SR
 This is not stored as a reference that needs to be resolved at present
 as it will start to pull in the SR and its tree and this property is only
 used currently to determine if there is a cache SR for the host
*/
 @property (copy) NSString* local_cache_sr;

@end
