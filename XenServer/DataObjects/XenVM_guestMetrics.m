//
//  XenVM_guestMetrics.m
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenVM_guestMetrics.h"
#import "NetworkAddress.h"

@interface XenVM_guestMetrics(private)
    - (void)BuildNetworkAddressArray:(NSDictionary*)rawData;
@end;

@implementation XenVM_guestMetrics

@synthesize PV_drivers_version, OS_version,networkAddress ;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vmGuestMetricsProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_VM_GUEST_METRICS Reference:reference Dictionary:vmGuestMetricsProperties];
    if (self){
        // PV drivers version
        [self setPV_drivers_version : [vmGuestMetricsProperties objectForKey:@"PV_drivers_version"]];
        // Guest OS Version 
        [self setOS_version : [vmGuestMetricsProperties objectForKey:@"os_version"]];
        // network addresses
        networkAddress = [[NSMutableArray alloc] init];
        [self BuildNetworkAddressArray:[vmGuestMetricsProperties objectForKey:@"networks"]];
    }
    return self;
}

- (NSString *)PVDriversVersionString{
    return [NSString stringWithFormat:@"%@.%@", 
                [[self PV_drivers_version] valueForKey:@"major"],
                [[self PV_drivers_version] valueForKey:@"minor"]];
}

- (NSString *)OSVersionName{
    
    NSString *fullName = [[self OS_version] valueForKey:@"name"];
    NSRange prefixRange = [fullName rangeOfString:@"|" options:(NSCaseInsensitiveSearch)];
    // return a parsed name if possible.
    // Windows has the partition information separateed by |
    // Linux (tools installed) does not have this format
    if (prefixRange.length > 0){
        return [fullName substringToIndex:prefixRange.location];
    }
    return fullName;

}

/**
 The Networks dictionary (raw data)
 Example is:
 "networks" =     {
 "0/ip" = "172.27.221.24";
 "1/ip" = "172.27.221.20";
 };
 */
- (void)BuildNetworkAddressArray:(NSDictionary*)rawData{
    for (NSString* key in rawData) {
        // split on /
        NSRange prefixRange = [key rangeOfString:@"/" options:(NSCaseInsensitiveSearch)];
        if (prefixRange.length >0){
            NSString* position = [key substringToIndex:prefixRange.location];
            NSString* type = [key substringFromIndex:prefixRange.location];
            // check type
            if ([type isEqualToString:@"/ip"]){
                int intPos = [position intValue];
                NSString* ipAddress = [rawData objectForKey:key];
                NetworkAddress *address = [[NetworkAddress alloc] initWithDeviceID:intPos andAddress:ipAddress];
                [[self networkAddress] addObject:address];
                address = nil;
            }
        }
    }
}


@end
