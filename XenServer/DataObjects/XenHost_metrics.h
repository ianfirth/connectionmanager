//
//  XenHost_metrics.h
//  hypOps
//
//  Created by Ian Firth on 23/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenBase.h"


@interface XenHost_metrics : XenBase 

/**
 The total memory that the host has (in bytes).
 */
@property (copy) NSNumber *totalMemory;

/**
 The free memory that the host has (in bytes).
 */
@property (copy) NSNumber *freeMemory;

@end
