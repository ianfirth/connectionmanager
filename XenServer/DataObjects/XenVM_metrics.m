//
//  XenVM_metrics.m
//  hypOps
//
//  Created by Ian Firth on 22/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "XenVM_metrics.h"
#import "XenHost.h"

@implementation XenVM_metrics

@synthesize start_time;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)vmMetricsProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_VM_METRICS Reference:reference Dictionary:vmMetricsProperties];
    if (self){

        if ([[vmMetricsProperties allKeys] containsObject:@"start_time"]){
            [self setStart_time: [vmMetricsProperties valueForKey:@"start_time"]];
        }
        // get available memory here too
        
        if ([[vmMetricsProperties allKeys] containsObject:@"memory_actual"]){
            [self setMemory_actual:[vmMetricsProperties valueForKey:@"memory_actual"]];
        }
    }
    return self;
}

@end
