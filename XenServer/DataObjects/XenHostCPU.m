//
//  XenHostCPU.m
//  hypOps
//
//  Created by Ian Firth on 19/04/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHostCPU.h"


@implementation XenHostCPU

@synthesize vendor, modelName;
- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)hostMeticsProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_HOST_METRICS Reference:reference Dictionary:hostMeticsProperties];
    if (self){
        // vendor
        [self setVendor :[hostMeticsProperties valueForKey:@"vendor"]];
        // model
       [self setModelName : [hostMeticsProperties valueForKey:@"modelname"]]; 
    }
    return self;
}


@end
