//
//  XenHost.m
//  hypOps
//
//  Created by Ian Firth on 01/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenHost.h"
#import "XenHost_metrics.h"
#import "XenHostCPU.h"
#import "XenVM.h"

@interface XenHost ()
+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat;
@end

@implementation XenHost

@synthesize tags, hostName, address, other_config,software_version,license_server,license_params,cpu_info, local_cache_sr;

- (id)initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)hostProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_HOST Reference:reference Dictionary:hostProperties];
    if (self){
         [self setLocal_cache_sr:[hostProperties valueForKey:@"local_cache_sr"]];  
        // tags
        [self setTags : [hostProperties objectForKey:@"tags"]];  
        // other_config
        [self setOther_config : [hostProperties objectForKey:@"other_config"]];
        // software_version
        [self setSoftware_version : [hostProperties objectForKey:@"software_version"]];
        // license_server
        [self setLicense_server : [hostProperties objectForKey:@"license_server"]];
        // license_params
        [self setLicense_params : [hostProperties objectForKey:@"license_params"]];
        // cpu_info // only available in 5.6 or later
        // before this it is in the separate cpu_info objects that will need retreiveing separatly
        // could make this work with earlier xenServer by getting this instead if host version is too early
        [self setCpu_info: [hostProperties objectForKey:@"cpu_info"]];
        // if before 5.6 then use the other method
        if (![self cpu_info]){
            [self addReferences:[hostProperties valueForKey:@"host_CPUs"] forObjectType:HYPOBJ_HOST_CPU];
        }
        // hostName
        [self setHostName : [hostProperties valueForKey:@"hostname"]];
        // address
        [self setAddress : [hostProperties valueForKey:@"address"]];
        // add reference to the HOST Guest Metrics
        [self addReference:[hostProperties valueForKey:@"metrics"] forObjectType:HYPOBJ_HOST_METRICS];
        //PIFs (physical network interfaces for the Host i.e. its NICS)
        [self addReferences:[hostProperties valueForKey:@"PIFs"] forObjectType:HYPOBJ_NIC];
        //PIFs (physical network interfaces for the Host i.e. its NICS)
        [self addReferences:[hostProperties valueForKey:@"PBDs"] forObjectType:HYPOBJ_PBD];
        //Control domain VM 
        [self addReferences:[self domainVMRefWithConnection:xenConnection hostRef:reference] forObjectType:HYPOBJ_VM_CONTROL_DOMAIN];
        
    }
    return self;
}

- (NSMutableDictionary*) references{
    NSNumber *key = [NSNumber numberWithInt:HYPOBJ_VM_CONTROL_DOMAIN];
    NSMutableDictionary* result = [super references];
    // re-evaluate the control domain entires here
    if (![result objectForKey:key] || [[result objectForKey:key] count] == 0){
         NSMutableArray* existingRefs =  [NSMutableArray arrayWithArray:[self domainVMRefWithConnection:[self hypervisorConnection] hostRef:[self opaque_ref]]];
         [result setObject:existingRefs forKey:key];
    }
    return result;
}

- (NSArray *) referencesForType:(int)hypObjectType{
    NSArray* existingRefs = [super referencesForType:hypObjectType];
    // add in the domain control references if they are not already present
    if (hypObjectType == HYPOBJ_VM_CONTROL_DOMAIN){
        existingRefs = [self domainVMRefWithConnection:[self hypervisorConnection] hostRef:[self opaque_ref]];
    }
    return existingRefs;
}

-(NSArray*)domainVMRefWithConnection:(XenHypervisorConnection *)xenConnection hostRef:(NSString*)reference{
    NSArray *refs = [xenConnection hypObjectsForType:HYPOBJ_VM_CONTROL_DOMAIN];
    for (XenVM* vm in refs) {
        NSString* hostRef = [[vm referencesForType:HYPOBJ_HOST] objectAtIndex:0];
        if ([hostRef isEqualToString:reference]){
            // found my Dom0 VM
            return [[NSArray alloc] initWithObjects:[vm opaque_ref],nil];
        }
    }
    return [[NSArray alloc] init];
}

- (void) dealloc{
    if (hostRRD){
        hostRRD = nil;
    }
    
    lastPreformanceDataUpdateTime = nil;
}

// retreive the host performance data.  If it is already stored then return that data
// otherwise fetch it fresh
- (RoundRobinDatabase*) hostPerformanceData{
    if (hostRRD == nil){
        [self refreshPerformanceData];
    }
    return hostRRD;
}

// remove the host performance data from memory
- (void) clearHostPerformanceData{
    lastPreformanceDataUpdateTime = nil;
    hostRRD = nil;
}

// gets the time that the last performance update was requested
- (NSDate*) lastPreformanceDataUpdateTime{
    return lastPreformanceDataUpdateTime;
}

// reloads the performcance data from the xenserver
-(BOOL) refreshPerformanceData{
// actually I dont think so, I will store a separate last 5 mins data for key metrics
// and perhaps a separate highest value for last hour (or something like this)
    // clear other Host performance data so as not to over use the memory
    // this data is for all hosts too, so really needs to be static for all instances of this class
    NSArray* hosts = [[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST];
    for (XenHost* host in hosts) {
        if (![[host opaque_ref] isEqualToString:[self opaque_ref]]){
            [host clearHostPerformanceData];
        }
    }

    hostRRD = nil;
    // reload the performance data from the Host 
    // build the URL here
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_XEN];
    NSDictionary* connections = [[XenHypervisorConnection connectionDictionary] objectForKey:hypervisorId];
    hypervisorId = nil;
    NSString* theConnectionID = [self connectionID];
    XenHypervisorConnection* connection = [connections objectForKey:theConnectionID]; 
    NSURL* connectionAddress = [connection connectionAddress];
    NSString* host = [connectionAddress host];
    NSNumber* port = [connectionAddress port];
    
    //http://<server>/host_rrd?session_id=OpaqueRef:<SESSION HANDLE>    
    NSString *URLString;
    if (port){
        URLString = [NSString stringWithFormat:@"http://%@:%@/host_rrd?session_id=%@",host,port,[connection sessionID]];
    }else
    {
        URLString = [NSString stringWithFormat:@"http://%@/host_rrd?session_id=%@",host,[connection sessionID]];
    }
    @try{
        hostRRD = [[RoundRobinDatabase alloc] initWithUrl:[NSURL URLWithString:URLString] withFullDataSet:NO];
    }
    @catch (id theException) {
		NSLog(@"Error loading Host RRD: %@", theException);
        hostRRD = nil;
        lastPreformanceDataUpdateTime = nil;
        return NO;
    }
    lastPreformanceDataUpdateTime = [NSDate date];

    return YES;
}

// this is effectivly intellicache enabled I think
-(BOOL) hasCacheStoreage{
    return ([self local_cache_sr] != nil);  
}

-(BOOL) isInMaintananceMode{
   NSString* value  =  [other_config valueForKey:@"MAINTENANCE_MODE"];
    return ([value isEqualToString:@"true"]);
}

- (NSString*) iscsi_iqn{
    return [other_config valueForKey:@"iscsi_iqn"];
}

- (NSDate *) serverUpTime{
    NSString* secondsSince1970String = [other_config valueForKey:@"boot_time"]; // this is a seconds stince 1970
    return [NSDate dateWithTimeIntervalSince1970:[secondsSince1970String doubleValue]];
}

- (NSDate *) toolStackUpTime{
    NSString* secondsSince1970String = [other_config valueForKey:@"agent_start_time"];// this is a seconds stince 1970 
    return [NSDate dateWithTimeIntervalSince1970:[secondsSince1970String doubleValue]];
}


-(NSString *) productVersionNumber{
    return [software_version valueForKey:@"product_version"];
}

- (NSDate *) licenseExipryDate{
    NSString *dateString = [license_params valueForKey:@"expiry"];
    if (!dateString) return nil;
    if ([dateString hasSuffix:@"Z"]) {
        dateString = [[dateString substringToIndex:(dateString.length-1)] stringByAppendingString:@"-0000"];
    }
    return [XenHost dateFromString:dateString withFormat:@"yyyyMMdd'T'HH:mm:ssZ"];
}

// constructs a dateonject from a string provovided by XenServer
+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSLocale *locale = [[NSLocale alloc] 
                        initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale];
    locale = nil;
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    dateFormatter = nil;
    return date;
}

// only available in 5.6 and above
-(NSString *) productVersionText{
    return [software_version valueForKey:@"product_version_text_short"];
}

-(NSString *) buildNumber{
    return [software_version valueForKey:@"build_number"];
}

-(NSString *) licenseEdition{
    return [license_params valueForKey:@"sku_marketing_name"];
}

-(NSString *) licenseAddress{
    return [license_server valueForKey:@"address"];
}

-(NSString *) licensePort{
    return [license_server valueForKey:@"port"];
}

-(NSString *) cpuCount{
    if (cpu_info){
        return [cpu_info valueForKey:@"cpu_count"];  
    }else{
        NSUInteger num = [[self referencesForType:HYPOBJ_HOST_CPU] count];
        return [NSString stringWithFormat:@"%d",num];
    }
}

-(NSString *) cpuVendor{
    if (cpu_info){
        return [cpu_info valueForKey:@"vendor"];  
    }else{
        NSArray *refs = [self referencesForType:HYPOBJ_HOST_CPU];
        NSString *ref = [refs objectAtIndex:0];
        XenHostCPU *cpus = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST_CPU withCondition:[XenBase XenBaseFor:ref]] objectAtIndex:0];
        return [cpus vendor];
    }
}

-(NSString *) cpuModel{
    if (cpu_info){
        return [cpu_info valueForKey:@"modelname"];  
    }else{
        NSArray *refs = [self referencesForType:HYPOBJ_HOST_CPU];
        NSString *ref = [refs objectAtIndex:0];
        XenHostCPU *cpus = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST_CPU withCondition:[XenBase XenBaseFor:ref]] objectAtIndex:0];
        return [cpus modelName];
    }
}

-(NSNumber *) totalMemory{
    NSArray *refs = [self referencesForType:HYPOBJ_HOST_METRICS];
    NSString *ref = [refs objectAtIndex:0];
    XenHost_metrics *metrics = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST_METRICS withCondition:[XenBase XenBaseFor:ref]] objectAtIndex:0];
    return [metrics totalMemory];
}

-(NSNumber *) freeMemory{
    NSArray *refs = [self referencesForType:HYPOBJ_HOST_METRICS];
    NSString *ref = [refs objectAtIndex:0];
    XenHost_metrics *metrics = [[[self hypervisorConnection] hypObjectsForType:HYPOBJ_HOST_METRICS withCondition:[XenBase XenBaseFor:ref]] objectAtIndex:0];
    return [metrics freeMemory];
}


@end
