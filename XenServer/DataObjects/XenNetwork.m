//
//  XenNetwork.m
//  hypOps
//
//  Created by Ian Firth on 03/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenNetwork.h"


@implementation XenNetwork

@synthesize other_config;

- (id) initWithConnection:(XenHypervisorConnection *)xenConnection Reference:(NSString *)reference Dictionary:(NSDictionary *)networkProperties{
    self = [super initWithConnection:xenConnection objectType:HYPOBJ_NETWORK Reference:reference Dictionary:networkProperties];
    if (self){
        // other_config
        [self setOther_config : [networkProperties objectForKey:@"other_config"]];
    }
    return self;
}

- (BOOL) isGuestInstallerNetwork{
    return ([[[self other_config] valueForKey:@"is_guest_installer_network"] isEqual:@"true"]);
}

@end
