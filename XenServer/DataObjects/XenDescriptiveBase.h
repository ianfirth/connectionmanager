//
//  XenDescriptiveBase.h
//  hypOps
//
//  Created by Ian Firth on 13/03/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "XenBase.h"

/**
 Class to represent a base XenServer objects that have descriptive content.
 A great deal of the objects in XenServer API contain the common descriptive properties.
 These properties are the name and the description (name_label ad name_description).  This class abstracts the common ones
 and forma a base for other objects to extend.
 */

@interface XenDescriptiveBase : XenBase

/**
 Create a Predicate that defines if the name is like the specified string
 @param name the Name to compare
 @returns a newly initialized NSPredicate
 */
+ (NSPredicate *) nameBeginsWith:(NSString *)name;
+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

/**
 The description of the object.
 */
@property (copy) NSString *name_description;

/**
 The name of the object.
 */
@property (copy) NSString *name_label;

@end
