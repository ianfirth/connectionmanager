//
//  XenConsole.h
//  hypOps
//
//  Created by Ian Firth on 24/10/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "XenBase.h"

@interface XenConsole : XenBase

@property (strong) NSString *protocol;
@property (copy) NSString *location;

@end