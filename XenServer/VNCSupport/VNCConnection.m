//
//  VNCConnection.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VNCConnection.h"

#define SSLConnection_None 0
#define SSLConnection_Initial 1
#define SSLConnection_connecting 2

@interface VNCConnection (){
    int sslState;
}
  - (void) openSSLConnection;
@end

@implementation VNCConnection
@synthesize delegate,isConnected;

- (id)initWithConnectionForVMLocation:(NSString*)theLocation andSessionID:(NSString*)theSessionId{
    if (self = [super init]) {   // store the properties
        sessionID = theSessionId;
        vmLocation = theLocation;
        NSURL* url = [NSURL URLWithString:theLocation];
        hostName = [url host];
    }
    return self;
}

NSThread* sslConnectionThread;

- (void) openConnection{
    // reset the dincomming data so its not processed next time the connection is used
    _data = [NSMutableData data];
    if (sslState != SSLConnection_None){
        NSLog(@"COnnection still opening.... will not open again");
    }
    else{
        NSLog(@"Opening new SSL Connection");
        sslConnectionThread = [[NSThread alloc] initWithTarget:self selector:@selector(runSSLConnection) object:nil];
        [sslConnectionThread start];
    }
}

-(void)runSSLConnection{
    @try {
       [self openSSLConnection];
        while (![[NSThread currentThread] isCancelled]){
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"SSL thread Exception = %@", exception);
    }
    @finally {
        [inStream close];
        [outStream close];
        
        [inStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [outStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        [inStream setDelegate:nil];
        [outStream setDelegate:nil];
        
        inStream = nil;
        outStream = nil;
        
        sslState = SSLConnection_None;
        _isConnected = NO;
    }
}

// send an event when connection is opened using a delegate
- (void) dealloc{
    // close the connection
    [self closeConnection];
}

-(void) closeConnection{
    [sslConnectionThread cancel];
    while ( sslConnectionThread && ![sslConnectionThread isFinished]){
        [NSThread sleepForTimeInterval:0.1];
    }
    sslConnectionThread = nil;
}

- (void)openSSLConnection
{
    sslState = SSLConnection_Initial;
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    bool useSSL = YES;
    
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)hostName, 443, &readStream, &writeStream);
    
    inStream = (__bridge_transfer NSInputStream *)readStream;
    outStream = (__bridge_transfer NSOutputStream *)writeStream;
    
    if (useSSL){

        [inStream setProperty:NSStreamSocketSecurityLevelNegotiatedSSL
                       forKey:NSStreamSocketSecurityLevelKey];

        [outStream setProperty:NSStreamSocketSecurityLevelNegotiatedSSL
                        forKey:NSStreamSocketSecurityLevelKey];
        

        CFReadStreamSetProperty((CFReadStreamRef)inStream, kCFStreamPropertySSLSettings,
                                (__bridge CFTypeRef)([NSMutableDictionary dictionaryWithObject:(NSString *)kCFBooleanFalse forKey:(NSString *)kCFStreamSSLValidatesCertificateChain]));
        
        [inStream setProperty:[NSDictionary dictionaryWithObjectsAndKeys:
                                        (id)kCFBooleanFalse, kCFStreamSSLValidatesCertificateChain, nil] forKey:(NSString *)kCFStreamPropertySSLSettings];
                
    }

    [inStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    inStream.delegate = self;
    outStream.delegate = self;

    if ([inStream streamStatus] == NSStreamStatusNotOpen)
        [inStream open];
    
    if ([outStream streamStatus] == NSStreamStatusNotOpen)
        [outStream open];
    
    _isConnected = YES;

}

#pragma mark NSStream delegate
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent{
    switch(streamEvent) {
        case NSStreamEventOpenCompleted:{
            if ( theStream  == outStream){
                NSLog(@"outSream Open Completed");
            }
            if (theStream  == inStream){
                NSLog(@"inStream Open Completed");
            }
            break;
        }
        case NSStreamEventHasSpaceAvailable:{
            if ( theStream  == outStream){
                NSLog(@"outSream has space available");
                if (sslState == SSLConnection_Initial){
                    // send the connect message
                    NSRange startFromRange = [vmLocation rangeOfString:@"console?"];
                    NSString *vmID = [vmLocation substringFromIndex:startFromRange.location];
                    NSString* txtConnect = [NSString stringWithFormat:@"CONNECT /%@&session_id=%@ HTTP/1.1\r\nHost: %@\r\n\r\n", vmID, sessionID, hostName ];
                    
                    NSData* connectData = [txtConnect dataUsingEncoding:NSUTF8StringEncoding];
                    NSLog(@"sending connect string - %@",txtConnect);
                    sslState = SSLConnection_connecting;
                    int written = [outStream write:[connectData bytes] maxLength:[connectData length]];
                    NSLog(@"Wrote %i",written);
               }
            }
            break;
        }
        case NSStreamEventEndEncountered:
        case NSStreamEventErrorOccurred:{
            NSError * error = [inStream streamError];
            NSLog(@"error %@ / %zd", [error domain], (ssize_t) [error code]);
            // https://developer.apple.com/library/mac/#documentation/security/Reference/secureTransportRef/Reference/reference.html
            // -9807 invalid certificate chain
            [[self delegate] ConnectionFailed:[error domain]];
            break;
        }
        case NSStreamEventHasBytesAvailable:{
            if(!_data) {
                _data = [NSMutableData data];
            }
            uint8_t buf[1024];
            unsigned int len = 0;
            len = [(NSInputStream *)theStream read:buf maxLength:1024];
            if(len) {
                [_data appendBytes:(const void *)buf length:len];
                NSString *myString = [[NSString alloc] initWithData:_data encoding:NSUTF8StringEncoding];
                NSLog(@"received:%@",myString);
                // if all goes well would expect something like this.
                // Fire a callback here so that the connection can be used by the VNC components
                // need to make sure that connections are cleaned up as expected though.
                
                //HTTP/1.1 200 OK
                //Connection: keep-alive
                //Cache-Control: no-cache, no-store
                // and a couple of carage returns
                // read this then pass the streams to the VNC view as it send RFB 003.008 and
                NSArray* strings = [myString componentsSeparatedByString:@"\n"];
                for (NSString* aString in strings){
                    NSString* aTrimmedString = [aString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if ([aTrimmedString hasPrefix:@"<html>"] && [aTrimmedString hasSuffix:@"</html>"]){
                        // this means that it went wrong
                        [[self delegate] ConnectionFailed:aTrimmedString];
                    }
                    if ([aTrimmedString hasPrefix:@"RFB"]){
                        // thats the beginning of the VNC protocol....
                        if (delegate){
                            // inform any delegate that the connection is complete
                            [[self delegate] ConnectionEsatblishedWithInputStream:inStream andOutputStream:outStream RFBVersion:aTrimmedString];
                        }
                    }
                }
            } else {
                NSLog(@"no buffer!");
            }
            break;
        }
        case NSStreamEventNone:
            break;
    }
}
@end
