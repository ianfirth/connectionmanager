//
//  VNCInteractionPacket.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VNCInteractionPacket.h"

@implementation VNCInteractionPacket

@synthesize messageType = _messageType;

-(id)initWithInputStream:(NSInputStream*)theinputStream andOutputStream:(NSOutputStream*)theOutputStream{
    self =  [super initWithInputStream:theinputStream andOutputStream:theOutputStream andPacketLength:1];
    if (self){
        // anyting specific to this class here
    }
    return self;
}

#pragma mark implementation of the packet abstract methods
-(bool) requestData{
    // don't think that there is anything needed to request data for this,
    // it just waits for data
    return YES;
}

// this only gets called when there is enough data to do anything
-(bool)processPacket:(uint8_t*) packetData{
    _messageType = [self readU8AtPosition:0];     // get the first available word
 //   NSLog(@"Got interaction packet messageType %i",_messageType);
    return YES;  // complete
}
@end
