//
//  VncController.h
//  VNC Client
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

typedef enum VncConnectionStateTypes
{
    VNC_CONNECTION_NONE,
    VNC_CONNECTION_REREDERING,
    VNC_CONNECTION_NOCONNECTIONAVAILABLE,
    VNC_CONNECTION_FAILED,
    VNC_CONNECTION_CONNECTING,
    VNC_CONNECTION_CONNECTED
} VncConnectionState;

#import "VNCRenderer.h"

/**
 * Class definition for the VncController class
 * This class is responsible for the communications with the VNC server via the input and opuput streams.
 * it understands the protocol and sends and recieves packets that are appropriate at the various 
 * states of the connection.
 * Any packets that provide graphical information are passed to the VNCRenderer that is defined for the
 * controller at the point that the packet is received.  This enables the renderer to be switched to a 
 * different one at any point without the connection to the VNC server needing to be closed and reconnected.
 */
@interface VncController : NSObject<NSStreamDelegate>{
    
    // the actual streams of data to and from the server
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    // comms channel flags
    bool outputReady;
    bool inputReady;
    bool txSpaceAvailable;
    
    // store the server version of protocol that is provided
    int vNC_Protocol_Highest_Version_Major;
    int vNC_Protocol_Highest_Version_Minor;
    
    // defines the current overall state of the connection
    VncConnectionState theVncConnectionState;
    bool needsFullFrame;
    
    // the renderer that will be used to provide display
    id<VNCRenderer> renderer;

    // the renderer that will be used to provide a secondary display
    id<VNCRenderer> secondaryRenderer;
}

-(id) initWithRendeder:(id<VNCRenderer>)theRenderer andUsingLowQuality:(BOOL)useLowQuality;

// allows change to the rendeder after connection made.
-(void) setRenderer:(id<VNCRenderer>)theRenderer;
-(void) setSecondaryRenderer:(id<VNCRenderer>)theRenderer;

/** this can be used by callers to inject their own streams that have been created as
 * part of an alternate connection intiailization
 */
- (void) assignDelegateToStreams:(NSInputStream*) theInputStream and:(NSOutputStream*)theOutputStream withRFBVersion:(NSString*)rfbVersion;

- (void) respondToHandshakeof:(NSString*)theServerHandshakeString;

// in the future this can support middle button and wheel if required but this is sufficient for now
-(void) sendMouseInformationwithX:(int)x Y:(int)y
                leftButtonPressed:(BOOL)leftPressed
               rightButtonPressed:(BOOL)rightPressed;

// methods to send key presses
-(void) sendKeyDownForKey:(int)keySym;
-(void) sendKeyUpForKey:(int)keySym;
-(void) close;

- (void)updateScreenBufferWithData:(uint8_t*)theData withLength:(int)length andRect:(CGRect)rect;

-(void)setNeedsFullScreenUpdate;

@property (readonly) id<VNCRenderer> renderer;
@property (readonly) id<VNCRenderer> secondaryRenderer;

@end
