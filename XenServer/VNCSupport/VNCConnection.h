//
//  VNCConnection.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <Foundation/Foundation.h>

@protocol VncConnectionDelegate <NSObject>
-(void)ConnectionEsatblishedWithInputStream:(NSInputStream*)theInputStream andOutputStream:(NSOutputStream*)theOutputStream RFBVersion:(NSString*)rfbVersion;
  -(void)ConnectionFailed:(NSString*)theReason;
@end

@interface VNCConnection : NSObject<NSStreamDelegate>{
    NSString* sessionID;
    NSString* hostName;
    NSString* vmLocation;
    
    NSInputStream* inStream;
    NSOutputStream* outStream;
    
    NSMutableData* _data;
    
    bool _isConnected;
}

- (id)initWithConnectionForVMLocation:(NSString*)theLocation andSessionID:(NSString*)theSessionId;
- (void) openConnection;
- (void) closeConnection;

@property id<VncConnectionDelegate> delegate;
@property bool isConnected;


@end
