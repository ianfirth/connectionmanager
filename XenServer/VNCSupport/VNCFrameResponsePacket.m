//
//  VNCFrameResponsePacket.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define state_init 0
#define state_header 1
#define state_rectangle 2

#import "VNCFrameResponsePacket.h"

@interface VNCFrameResponsePacket (){
    int state;
    int rectangleCount;
    
    int currentFrame_xpos;
    int currentFrame_ypos;
    int currentFrame_width;
    int currentFrame_height;
    int currentFrame_xpos_left;
    
    int bytesexpectedDataRemaining;
    
    int redShift;
    int greenShift;
    int blueShift;

    int redMax;
    int blueMax;
    int greenMax;
    
    uint8_t * rawBuffer;
    
    VncController* vncController;
}
@end

@implementation VNCFrameResponsePacket

-(id)initWithInputStream:(NSInputStream*)theinputStream andOutputStream:(NSOutputStream*)theOutputStream andBitsPerPixel:(int)theBitsPerPixel VncController:(VncController*) thevncController usingRedShift:(int)theredShift greenShift:(int)thegreenShift blueShift:(int)theblueShift redMax:(int)theRedMax greenMax:(int)theGreenMax blueMax:(int)theBlueMax{
    self =  [super initWithInputStream:theinputStream andOutputStream:theOutputStream andPacketLength:3];
    if (self){
        // anyting specific to this class here
        state = state_init;
        rectangleCount = 0;
        bitsPerPixel = theBitsPerPixel;
        vncController = thevncController;
        blueShift = theblueShift;
        greenShift = thegreenShift;
        redShift = theredShift;
        
        // these could be passed in later but for now assume that they are all one byte
        redMax = theRedMax;
        blueMax = theBlueMax;
        greenMax = theGreenMax;
        freeBufferOnDealloc = YES;  // data is transferred to a new buffer so needs to be freed
    }
    return self;

}

#pragma mark implementation of the packet abstract methods

// this only gets called when there is enough data to do anything
-(bool)processPacket:(uint8_t*) packetData{
    
    if (state == state_init){
        // 1st byte is padding
        rectangleCount = [self readU16AtPosition:1];  // next 2 are the number of rectangles
        //NSLog(@"Got Frame with %i rectangles", rectangleCount);
        
        [self resetBufferForPacketLength:12];
        state = state_header;
        return NO;
    }
    else if (state == state_header){
       // getting a rectangle
        currentFrame_xpos = [self readU16AtPosition:0];
        currentFrame_ypos = [self readU16AtPosition:2];
        currentFrame_width = [self readU16AtPosition:4];
        currentFrame_height = [self readU16AtPosition:6];
        currentFrame_xpos_left = currentFrame_xpos;
        //NSLog(@"rectangle %i has frame x:%i,y:%i width:%i height:%i", rectangleCount,currentFrame_xpos,currentFrame_ypos,currentFrame_width, currentFrame_height);
        int encodingType = [self readU32AtPosition:8];
        if (encodingType == 0){   // raw encoding
            bytesexpectedDataRemaining = (bitsPerPixel/8) * currentFrame_width * currentFrame_height;
        }
        else{
            NSLog(@"Unsupported encoding type -- only raw encoding supported currently");
        }
        state = state_rectangle;
        // this is the whole rectangle could do a line? later on
        [self resetBufferForPacketLength:bytesexpectedDataRemaining];
        return NO;
    }
    else if (state == state_rectangle){
        // process the buffer and render the rectangle here
        CGRect rect = CGRectMake(currentFrame_xpos, currentFrame_ypos, currentFrame_width, currentFrame_height);
        // get the bytes in the right order and set alpha level
        
        // move along in number of bytes per pixel (i.e bitsPerPixel /8)
        // and only read 8 if its 8 for example not 32
        int bytesPerPixel = bitsPerPixel/8;
        
        // so need to work out how many pixels there are here
        int pixelCount = packetLength/bytesPerPixel;
        
        // output is always going to have 4 bytes per pixel
        int outputBufferRequired = pixelCount * 4;
        
        rawBuffer = (uint8_t *)calloc(outputBufferRequired, sizeof(uint8_t));
        [self processRawFrameUpdateContent:rawBuffer];
        
        //NSLog(@"SendToView x:%i,y:%i width:%i height:%i",currentFrame_xpos,currentFrame_ypos,currentFrame_width, currentFrame_height);
        [vncController updateScreenBufferWithData:rawBuffer withLength:outputBufferRequired andRect:rect];
                
        free(rawBuffer);
        
        rectangleCount --;
        if (rectangleCount > 0){
            [self resetBufferForPacketLength:12];
            state = state_header;
            return NO;
        }
    }
    return YES;  // complete
}

// this could do loads at a time i Think
// this might speed things up loads
-(void) processRawFrameUpdateContent:(uint8_t *)outbuffer{

    // move along in number of bytes per pixel (i.e bitsPerPixel /8)
    // and only read 8 if its 8 for example not 32
    int bytesPerPixel = bitsPerPixel/8;
    
    // so need to work out how many pixels there are here
    int pixelCount = packetLength/bytesPerPixel;
    
    for (int i = 0; i<pixelCount ; i ++){
        int data = 0;
        if (bytesPerPixel == 4){
           data = [self readU32LittleAtPosition:i*4];
        }
        if (bytesPerPixel == 1){
            data = [self readU8AtPosition:i];
        }
        unsigned char red = data >> redShift;
        red = red & redMax;
        unsigned char green = data >> greenShift;
        green = green & greenMax;
        unsigned char blue = data >> blueShift;
        blue = blue & blueMax;
        
        int baseByte = i*4;

        // put r g b values back into screen and make use full client size range
        outbuffer[baseByte] = red * (255/redMax);
        outbuffer[baseByte+1] = green * (255/greenMax);
        outbuffer[baseByte+2] = blue * (255/blueMax);
        outbuffer[baseByte+3] = 255;  // alpha
    }
}

@end
