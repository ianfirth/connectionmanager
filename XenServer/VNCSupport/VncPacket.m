//
//  Packet.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VncPacket.h"

@implementation VncPacket

-(id)initWithInputStream:(NSInputStream*)theInputStream andOutputStream:(NSOutputStream*)theOutputStream andPacketLength:(int)thePacketLength{
    self = [super init];
    if (self){
        inputStream = theInputStream;
        outputStream = theOutputStream;
        packetLength = thePacketLength;
        buffer = (uint8_t *)calloc(thePacketLength, sizeof(uint8_t));
        bufferPosition = 0;
        freeBufferOnDealloc = YES;
    }
    return self;
}

-(void) dealloc{
    // clear up any resources
    if (freeBufferOnDealloc){
        free(buffer);
        buffer = nil;
    }
    else{
        NSLog(@"Not freeing buffer assuming it will be freed elsewhere");
    }
    inputStream = nil;
    outputStream = nil;
}

-(bool) processData{
    //NSLog(@"Process data is waiting for %i bytes", packetLength);
    
    // how many more bytes does this packet currently need
    int remainingSpace = packetLength - bufferPosition;
    //NSLog(@"Process data is still waiting for %i bytes", remainingSpace);
    
    if (remainingSpace == 0){
        // no space left this should not consume more data
        return [self processPacket:buffer];
    }
    unsigned int len = 0;
    len = [inputStream read:(buffer + bufferPosition) maxLength:remainingSpace];
    bufferPosition = bufferPosition + len;
    // if the buffer is full then call the process packet method to determine what to do next
    if (bufferPosition == packetLength){
        return [self processPacket:buffer];
    }
    
    // return NO, its not complete wait for more data
    return NO;
}

// assume passing buffer to someone else for free
-(void) resetBufferForPacketWithoutFreeAndLength:(int)length{
    packetLength = length;
    buffer = (uint8_t *)calloc(length, sizeof(uint8_t));
    bufferPosition = 0;
}

-(void) resetBufferForPacketLength:(int)length{
    // free any current buffer
    free(buffer);
    buffer = nil;
    packetLength = length;
    // create new buffer
    
    buffer= (uint8_t *)calloc(length, sizeof(uint8_t));
    bufferPosition = 0;
}

#pragma mark helper methods
- (int) readU8AtPosition:(int)position{
    return buffer[position];
}

- (int) readU16AtPosition:(int)position{
    return CFSwapInt16BigToHost(*(int*)(buffer + position));  // hope it does not matter that there are more that 2 bytes here
}

- (int) readU32AtPosition:(int)position{
    return CFSwapInt32BigToHost(*(int*)(buffer + position));  // hope it does not matter that there are more that 4 bytes here
}

- (int) readU32LittleAtPosition:(int)position{
    return CFSwapInt32LittleToHost(*(int*)(buffer + position));  // hope it does not matter that there are more that 4 bytes here
}

@end
