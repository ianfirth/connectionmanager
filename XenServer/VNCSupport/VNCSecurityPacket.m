//
//  VNCSecurityPacket.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//#import <CommonCrypto/CommonCrypto.h>
#import "VNCSecurityPacket.h"

@interface VNCSecurityPacket(){
    bool challenge;
    NSString* vncPassword;
}
@end

@implementation VNCSecurityPacket

@synthesize securityType = _securityType;

-(id)initWithInputStream:(NSInputStream*)theinputStream andOutputStream:(NSOutputStream*)theOutputStream{
    self =  [super initWithInputStream:theinputStream andOutputStream:theOutputStream andPacketLength:4];
    if (self){
        // anyting specific to this class here
        challenge = NO;
        vncPassword = @"testtest";  // need to be able to set this for now hard code
    }
    return self;
}

#pragma mark implementation of the packet abstract methods

// this only gets called when there is enough data to do anything
-(bool)processPacket:(uint8_t*) packetData{
    // in 3.3 this can only be a single word (4 bytes)  ---- watch out if negotiate 3.7 or later this changes
    // 0 = invalid and is followed by a reason string
    // 1 = None
    // 2 = VNC Authentication
    if (!challenge){
        _securityType = [self readU32AtPosition:0];     // get the first available word
         NSLog(@"VNC Security type:%i",_securityType);

         // this is VNC auth
        if (_securityType == 2){
             // wait for another 16 bytes that is the challenge
            [self resetBufferForPacketLength:16];
            challenge = YES;
            return NO;
        }
    }
    else{
        //NSData* result = [self DESEncryptChallengeWithKey:vncPassword andChallenge:buffer];
        // put this into a variable so it can be read externally
    }
    return YES;  // complete
}




// encrypts a 16 byte challenge from the server using the password supplied and returns the
// 16 byte response.
// This is used for VNC authentication
// no need to support VNC auth at the moment
/*  
-(NSData*)DESEncryptChallengeWithKey:(NSString*)password andChallenge:(uint8_t*)challenge{
    
    // tested against these test vectors
    // https://www.cosic.esat.kuleuven.be/nessie/testvectors/bc/des/Des-64-64.test-vectors
    
    // truncate password to 8 chars long
    if ([password length] > 8){
        password = [password substringToIndex:8];
    }
    
    // make sure that there are 8 bytes with the key in it padded with zeros if not that long
    unsigned char *key = (unsigned char *)calloc(8, sizeof(unsigned char));
    bzero( key, sizeof(key) ); // fill with zeroes (for padding)
    
    NSData* passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [passwordData getBytes:key length:[passwordData length]];
    
    // flip the bits in each byte of the password here
    for (int i =0 ; i <8 ; i++){
        unsigned char byteIn = key[i];
        unsigned char byteOut = 0;
        // flip bits
        for (int p = 0; p<8 ; p++){
            unsigned char lowestBit = (byteIn & 1);
            lowestBit = lowestBit << 7-p;
            byteOut = byteOut | lowestBit;
            byteIn = byteIn >> 1 ;
        }
        key[i] = byteOut;
    }
    
    // the result
    unsigned char *result1 = (unsigned char *)calloc(8, sizeof(unsigned char));
    int resultSize1 = 8;
    bzero( result1, sizeof(result1) ); // fill with zeroes (for padding)
    size_t numBytesEncrypted1 = 0;
    
    unsigned char *result2 = (unsigned char *)calloc(8, sizeof(unsigned char));
    int resultSize2 = 8;
    bzero( result2, sizeof(result2) ); // fill with zeroes (for padding)
    size_t numBytesEncrypted2 = 0;
    
    // do 2 passes one for first 8 bytes and one for second 8 then join together and return 16 byte result
    // try in one go first :)
    unsigned char challengePass1[8];
    NSRange first8 = NSMakeRange(0,8);
    [challenge getBytes:challengePass1 range:first8];
    unsigned char challengePass2[8];
    NSRange second8 = NSMakeRange(8,8);
    [challenge getBytes:challengePass2 range:second8];
    
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
                                          kCCAlgorithmDES,
                                          kCCOptionECBMode,
                                          key,
                                          kCCKeySizeDES,
                                          NULL,
                                          challengePass1,
                                          8,
                                          result1,
                                          resultSize1,
                                          &numBytesEncrypted1);
    
    CCCryptorStatus cryptStatus2 = CCCrypt(kCCEncrypt,
                                           kCCAlgorithmDES,
                                           kCCOptionECBMode,
                                           key,
                                           kCCKeySizeDES,
                                           NULL,
                                           challengePass2,
                                           8,
                                           result2,
                                           resultSize2,
                                           &numBytesEncrypted2);
    
    
    NSMutableData* resultData = [NSMutableData dataWithBytes:result1 length:numBytesEncrypted1];
    [resultData appendBytes:result2 length:numBytesEncrypted2];
    
    // todo check the cryptstatus properly here and throw exception if failure
    if (cryptStatus != 0 || cryptStatus2 != 0)
    {
        NSLog(@"DES encryption failed with crypt1 status:%i and crypt2 status:%i",cryptStatus,cryptStatus2);
        // todo throw an excepption here
    }
    free(key);
    free(result1);
    free(result2);
    return resultData;
}
 */
@end
