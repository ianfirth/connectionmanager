//
//  Packet.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import <Foundation/Foundation.h>

@interface VncPacket : NSObject{
    NSInputStream* inputStream;
    NSOutputStream* outputStream;
    int packetLength;
    
    uint8_t* buffer;
    int bufferPosition;
    bool freeBufferOnDealloc;
}

-(id)initWithInputStream:(NSInputStream*)inputStream andOutputStream:(NSOutputStream*)outputStream andPacketLength:(int)thePacketLength;

- (bool) processData;
- (void) resetBufferForPacketLength:(int)length;
- (void) resetBufferForPacketWithoutFreeAndLength:(int)length;

- (int) readU8AtPosition:(int)position;
- (int) readU16AtPosition:(int)position;
- (int) readU32AtPosition:(int)position;
- (int) readU32LittleAtPosition:(int)position;
@end

@interface VncPacket (AbstractMethods)

// Define Abstract methods which should be implemented by subclasses:
-(id)initWithInputStream:(NSInputStream*)inputStream andOutputStream:(NSOutputStream*)outputStream;

// this can do things like change the number of bytes being waited for
// if there is nothing more to do then return YES (i.e. complete) otherwise return NO
-(bool)processPacket:(uint8_t*) packetData;

@end
