//
//  VNCHandshakePacket.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VNCHandshakePacket.h"

@implementation VNCHandshakePacket

@synthesize handshakeString = _handshakeString;

-(id)initWithInputStream:(NSInputStream*)theinputStream andOutputStream:(NSOutputStream*)theOutputStream{
    self =  [super initWithInputStream:theinputStream andOutputStream:theOutputStream andPacketLength:12];
    if (self){
        // anyting specific to this class here
    }
    return self;
}

#pragma mark implementation of the packet abstract methods

// this only gets called when there is enough data to do anything
-(bool)processPacket:(uint8_t*) packetData{
    _handshakeString = [[NSString alloc] initWithBytes:packetData length:12 encoding:NSUTF8StringEncoding];
    return YES;  // complete
}

@end
