//
//  VNCClientInitPacket.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VNCClientInitPacket.h"
@interface VNCClientInitPacket (){
    int nameLength;
    bool initialPass;
}
@end

@implementation VNCClientInitPacket

@synthesize name = _name;

@synthesize screenWidth = _screenWidth;
@synthesize screenHeight = _screenHeight;
@synthesize bitsPerPixel = _bitsPerPixel;
@synthesize depth = _depth;
@synthesize bigEndianFlag = _bigEndianFlag;
@synthesize trueColorFlag = _trueColorFlag;
@synthesize redMax = _redMax;
@synthesize greenMax = _greenMax;
@synthesize blueMax = _blueMax;
@synthesize redShift = _redShift;
@synthesize greenShift = _greenShift;
@synthesize blueShift = _blueShift;


-(id)initWithInputStream:(NSInputStream*)theinputStream andOutputStream:(NSOutputStream*)theOutputStream{
    self =  [super initWithInputStream:theinputStream andOutputStream:theOutputStream andPacketLength:24];
    if (self){
        // anyting specific to this class here
        initialPass = YES;
    }
    return self;
}

#pragma mark implementation of the packet abstract methods


// this only gets called when there is enough data to do anything
-(bool)processPacket:(uint8_t*) packetData{
    if (initialPass){
        _screenWidth = [self readU16AtPosition:0];
        _screenHeight = [self readU16AtPosition:2];
        // pixel format is the next 16 bytes
        _bitsPerPixel = [self readU8AtPosition:4];
        _depth = [self readU8AtPosition:5];
        _bigEndianFlag = [self readU8AtPosition:6];
        _trueColorFlag = [self readU8AtPosition:7];
        _redMax = [self readU16AtPosition:8];
        _greenMax = [self readU16AtPosition:10];
        _blueMax = [self readU16AtPosition:12];
        _redShift = [self readU8AtPosition:14];
        _greenShift = [self readU8AtPosition:15];
        _blueShift = [self readU8AtPosition:16];

        // ignore 3 bytes padding
        
        nameLength = [self readU32AtPosition:20];
        [self resetBufferForPacketLength:nameLength];
        initialPass = NO;
        NSLog(@"ServerInit screenwidth is %i",_screenWidth);
        NSLog(@"ServerInit screenHeight is %i",_screenHeight);
        NSLog(@"ServerInit bitsPerPixel is %i",_bitsPerPixel);
        NSLog(@"ServerInit depth is %i",_depth);
        NSLog(@"ServerInit bigEndianFlag is %i",_bigEndianFlag);
        NSLog(@"ServerInit trueColorFlag is %i",_trueColorFlag);
        NSLog(@"ServerInit redMax is %i",_redMax);
        NSLog(@"ServerInit greenMax is %i",_greenMax);
        NSLog(@"ServerInit blueMax is %i",_blueMax);
        NSLog(@"ServerInit redShift is %i",_redShift);
        NSLog(@"ServerInit greenShift is %i",_greenShift);
        NSLog(@"ServerInit blueShift is %i",_blueShift);
        
        return NO;   // still more to come
    }
    else{
        _name = [[NSString alloc] initWithBytes:packetData length:nameLength encoding:NSUTF8StringEncoding];
        NSLog(@"ServerInit Name is %@",_name);
    }
    
    return YES;  // complete
}

@end
