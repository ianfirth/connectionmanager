//
//  VNCClientInitPacket.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VncPacket.h"

@interface VNCClientInitPacket : VncPacket{
    NSString* _name;
    int _screenWidth;
    int _screenHeight;
    int _bitsPerPixel;
    int _depth;
    int _bigEndianFlag;
    int _trueColorFlag ;
    int _redMax;
    int _greenMax;
    int _blueMax;
    int _redShift;
    int _greenShift;
    int _blueShift;
}

@property (readonly) NSString* name;
@property (readonly) int screenWidth;
@property (readonly) int screenHeight;
@property (readonly) int bitsPerPixel;
@property (readonly) int depth;
@property (readonly) int bigEndianFlag;
@property (readonly) int trueColorFlag ;
@property (readonly) int redMax;
@property (readonly) int greenMax;
@property (readonly) int blueMax;
@property (readonly) int redShift;
@property (readonly) int greenShift;
@property (readonly) int blueShift;

@end
