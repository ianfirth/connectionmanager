//
//  VncController.m
//  VNC Client
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "VncController.h"
#import "VncPacket.h"
#import "VNCHandshakePacket.h"
#import "VNCSecurityPacket.h"
#import "VNCClientInitPacket.h"
#import "VNCFrameResponsePacket.h"
#import "VNCInteractionPacket.h"

#define VNCScreenRefreshBackupTimerInterval 15.0

@interface VncController (){
    VncPacket* currentPacket;
    int currentBitsPerPixel;
    int screenWidth;
    int screenHeight;
    
    int redShift;
    int greenShift;
    int blueShift;

    int redMax;
    int greenMax;
    int blueMax;

    bool packetComplete;
    
    int lastX;
    int lastY;
    
    BOOL useLowQuality;
    
    uint8_t* _screenBuffer;
    long long _screenBufferSize;
    int _screenRowByteCount;
}

  - (void) sendClientInitMessage;
  - (void) ProcessInput;
  - (void) sendString:(NSString*) txString;
  - (void) sendFullFrameBufferUpdateRequest;
  - (void) sendIncrementalFrameBufferUpdateRequest;
  - (void) populateU16Buffer:(unsigned char *)buffer atPostion:(int)position fromInt:(int)value;
/**
 * Request a screen update.  If specify fullFrameRequied, a full frame will be requested will be forced, otherwise
 * an incremental one will be requested.
 */
- (void) requestScreenUpdate:(BOOL)fullFrameNeeded;

/**
 * Request a screen update.  Will request a full frame if one is needed otherwise an incremental one will be
 * requested.
 */
- (void) requestScreenUpdate;

@end

@implementation VncController

@synthesize renderer;
@synthesize secondaryRenderer;

-(id) initWithRendeder:(id<VNCRenderer>)theRenderer andUsingLowQuality:(BOOL)wantToUseLowQuality{
    self = [super init];
    if (self){
        useLowQuality = wantToUseLowQuality;
        needsFullFrame = YES;
        renderer = theRenderer;
        runFrameRequestLoop = NO;
        frameRequestThread = [[NSThread alloc] initWithTarget:self selector:@selector(mainFrameRequestLoop) object:nil];
        [frameRequestThread start];
    }
    return self;
}

-(void)setNeedsFullScreenUpdate{
    needsFullFrame = YES;
}

-(void) close{
    runFrameRequestLoop = NO;
    [frameRequestThread cancel];
    while ([frameRequestThread isExecuting]){
        sleep(0.01);
    }
    inputStream = nil;
    outputStream = nil;
}

-(void) dealloc{
    [self setRenderer:nil];
    [self setSecondaryRenderer:nil];
    [self close];
    [self releaseScreenBuffer];
}

-(void) allocateScreenBufferForSize:(CGSize)size{
    _screenBufferSize = size.width * size.height * 4; 
    _screenBuffer = (uint8_t*) calloc(_screenBufferSize,sizeof(uint8_t));
    _screenRowByteCount = size.width * 4;
}

-(void) releaseScreenBuffer{
    if (_screenBuffer){
        free(_screenBuffer);
        _screenBuffer =nil;
        _screenRowByteCount = 0;
        _screenBufferSize = 0;
    }
    [renderer setScreenBuffer:nil withDataSize:0 bytesPerRow:0 remoteScreenSize:CGSizeZero];
    [secondaryRenderer setScreenBuffer:nil withDataSize:0 bytesPerRow:0 remoteScreenSize:CGSizeZero];
}

-(void) setRenderer:(id<VNCRenderer>)theRenderer{
    renderer = theRenderer;
    [renderer setScreenBuffer:_screenBuffer withDataSize:_screenBufferSize bytesPerRow:_screenRowByteCount remoteScreenSize:CGSizeMake(screenWidth, screenHeight)];
    [renderer updateDisplay];
}

-(void) setSecondaryRenderer:(id<VNCRenderer>)theRenderer{
    secondaryRenderer = theRenderer;
    [secondaryRenderer setScreenBuffer:_screenBuffer withDataSize:_screenBufferSize bytesPerRow:_screenRowByteCount remoteScreenSize:CGSizeMake(screenWidth, screenHeight)];
    [secondaryRenderer updateDisplay];
}

- (void) requestScreenUpdate{
    if (needsFullFrame){
        [self sendFullFrameBufferUpdateRequest];
    }
    else{
        [self sendIncrementalFrameBufferUpdateRequest];
    }
}

- (void) requestScreenUpdate:(BOOL)fullFrameNeeded{
    // keep needs Full frame set if its been previously requested but no result yet
    if (!needsFullFrame){
        needsFullFrame = fullFrameNeeded;
    }
    [self requestScreenUpdate];
}

// this can be used by callers to inject their own streams that have been created as
// part of an alternate connection intiailization
-(void)assignDelegateToStreams:(NSInputStream*) theInputStream and:(NSOutputStream*)theOutputStream withRFBVersion:(NSString*)rfbVersion{
    // this is called as assuming that we are at the begining of the stream
    // set the currentPacketType to the handshake to start the process
    currentPacket = [[VNCHandshakePacket alloc] initWithInputStream:inputStream andOutputStream:outputStream];
    inputStream = theInputStream;
    outputStream = theOutputStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [self respondToHandshakeof:rfbVersion];
}

-(void) sendMouseInformationwithX:(int)x Y:(int)y
                     leftButtonPressed:(BOOL)leftPressed
                    rightButtonPressed:(BOOL)rightPressed{

        lastX = x;
        lastY = y;    
        // there is no expected response to this it is just a fire and forget call.
        // this means that it does not impact the state model at all, stays in interaction mode
        // and the packet that is currently set to recieve data remains untoched.
        unsigned char *mouseMovementRequest = (unsigned char *)calloc(6, sizeof(unsigned char));
        mouseMovementRequest[0] = 5;  // message type is PointerEvent
        // Button Mask bits, 0=UP , 1=Down
        // bit 0 = Left Button
        // bit 1 = middle Button
        // bit 2 = right Button
        // bit 4 = Wheel up (press and release for a step)
        // bit 5 = wheel down (press and release for a step)
        unsigned char buttonMask = 0x0;
        buttonMask = buttonMask | leftPressed;
        buttonMask = buttonMask | (rightPressed << 2);
        mouseMovementRequest[1] = buttonMask;  // Button Mask
        [self populateU16Buffer:mouseMovementRequest atPostion:2 fromInt:x];  //x
        [self populateU16Buffer:mouseMovementRequest atPostion:4 fromInt:y];  // y
        [outputStream write:mouseMovementRequest maxLength:6];
        free(mouseMovementRequest);
}

-(void) sendKeyDownForKey:(int)keySym{
        // there is no expected response to this it is just a fire and forget call.
        // this means that it does not impact the state model at all, stays in interaction mode
        // and the packet that is currently set to recieve data remains untoched.
        unsigned char *keyDownRequest = (unsigned char *)calloc(8, sizeof(unsigned char));
        keyDownRequest[0] = 4;  // message type is KeyEvent
        keyDownRequest[1] = 1;  // non zero is down
        // 2 bytes padding in here
        [self populateU32Buffer:keyDownRequest atPostion:4 fromInt:keySym];
        [outputStream write:keyDownRequest maxLength:8];
        free(keyDownRequest);
}

-(void) sendKeyUpForKey:(int)keySym{
        // there is no expected response to this it is just a fire and forget call.
        // this means that it does not impact the state model at all, stays in interaction mode
        // and the packet that is currently set to recieve data remains untoched.
        unsigned char *keyUpRequest = (unsigned char *)calloc(8, sizeof(unsigned char));
        keyUpRequest[0] = 4;  // message type is KeyEvent
        keyUpRequest[1] = 0;  // zero is down
        // 2 bytes padding in here
        [self populateU32Buffer:keyUpRequest atPostion:4 fromInt:keySym];
        [outputStream write:keyUpRequest maxLength:8];
        free(keyUpRequest);
}

#pragma mark NSStreamDelegate
// this gets called when things happen on the stream in either direction
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent{
    switch(streamEvent) {
        case NSStreamEventOpenCompleted:
            if ( theStream  == outputStream){
                outputReady = YES;
                NSLog(@"Open Completed");
            }
            if (theStream  == inputStream){
                inputReady = YES;
                NSLog(@"Open Completed");
            }
            
            break;
        case NSStreamEventHasSpaceAvailable:
            if ( theStream  == outputStream){
                txSpaceAvailable = YES;
            }
            break;
        case NSStreamEventHasBytesAvailable:
        {
            [self ProcessInput];
            break;
        }
        case NSStreamEventEndEncountered:
        {
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop]
                                 forMode:NSDefaultRunLoopMode];
            if (theStream == inputStream){
                currentPacket = nil;
            }
            break;
        }
        default:{
            break;
        }
    }
}

-(void) respondToHandshakeof:(NSString*)theServerHandshakeString{
    NSLog(@"The Handshake is:%@",theServerHandshakeString);
    NSRange majorRange = NSMakeRange(4, 3);
    NSRange minorRange = NSMakeRange(8, 3);
    NSString* majorStr = [theServerHandshakeString substringWithRange:majorRange];
    NSString* minorStr = [theServerHandshakeString substringWithRange:minorRange];
    vNC_Protocol_Highest_Version_Major = [majorStr intValue];
    vNC_Protocol_Highest_Version_Major = [minorStr intValue];
    // respond to handshake here with my version.  This must not be higher that the version reported by the server.
    // I support 3.3 so as long as its higher than that respond with that otherwise crash out

    currentPacket = [[VNCSecurityPacket alloc] initWithInputStream:inputStream andOutputStream:outputStream];
    // Move state on from handshake to initialization mode here

    // this is the version that I support, and is compatible with the VNC for XenServer hosts and Guests (Guests use at least 003.008 and Host use 003.003
    [self sendString: @"RFB 003.003\n"];
}

-(void) sendClientInitMessage{
    @synchronized(self){
        currentPacket = [[VNCClientInitPacket alloc] initWithInputStream:inputStream andOutputStream:outputStream];
        unsigned char *clientInit = (unsigned char *)calloc(1, sizeof(unsigned char));
        clientInit[0] = 1;  // allow sharing of the desktop with other users if this is 0 it will try to disconnect other connections (exclueive access)
        [outputStream write:clientInit maxLength:1];
        free(clientInit);
    }
}

-(void) populateU16Buffer:(unsigned char *)buffer atPostion:(int)position fromInt:(int)value{
   // is this always this way round? at home it has to be like this.
    // is work the same, or do I need to check a flag somewhere.
    buffer[position] = value >> 8;
    buffer[position+1] = value & 0xff;
}

-(void) populateU32Buffer:(unsigned char *)buffer atPostion:(int)position fromInt:(int)value{
    // is this always this way round? at home it has to be like this.
    // is work the same, or do I need to check a flag somewhere.
    buffer[position] = value >> 24;
    buffer[position+1] = value >> 16;
    buffer[position+2] = value >> 8;
    buffer[position+3] = value & 0xff;
}

// sends a request for a full refresh, i.e. a non-incremental update
-(void) sendFullFrameBufferUpdateRequest{
    needsFullFrame = NO;
    NSLog(@"Request Full Frame Screen size 0,0,%i,%i", screenWidth,screenHeight);
    
    unsigned char *frameBufferUpdateRequest = (unsigned char *)calloc(10, sizeof(unsigned char));
    frameBufferUpdateRequest[0] = 3;  // message type is FrameBufferUpdateRequest
    frameBufferUpdateRequest[1] = 0;  // not incremental i.e. Full frame request
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:2 fromInt:0];  //x
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:4 fromInt:0];  // y
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:6 fromInt:screenWidth];
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:8 fromInt:screenHeight];
    [outputStream write:frameBufferUpdateRequest maxLength:10];
    free(frameBufferUpdateRequest);
}

// sends a request for an incremental refresh
-(void) sendIncrementalFrameBufferUpdateRequest{    
        //NSLog(@"Request incremental Frame");
        unsigned char *frameBufferUpdateRequest = (unsigned char *)calloc(10, sizeof(unsigned char));
        frameBufferUpdateRequest[0] = 3;  // message type is FrameBufferUpdateRequest
        frameBufferUpdateRequest[1] = 1;  // incremental i.e. Full frame request
        [self populateU16Buffer:frameBufferUpdateRequest atPostion:2 fromInt:0];  //x
        [self populateU16Buffer:frameBufferUpdateRequest atPostion:4 fromInt:0];  // y
        [self populateU16Buffer:frameBufferUpdateRequest atPostion:6 fromInt:screenWidth];
        [self populateU16Buffer:frameBufferUpdateRequest atPostion:8 fromInt:screenHeight];
        //        NSLog(@"Requesting incremental frameupdate");
        [outputStream write:frameBufferUpdateRequest maxLength:10];
        free(frameBufferUpdateRequest);
}

-(void) setEncoding{
    NSLog(@"Set Encoding to RAW");
    unsigned char *frameBufferUpdateRequest = (unsigned char *)calloc(8, sizeof(unsigned char));
    frameBufferUpdateRequest[0] = 2;  // message type is setEncondings
    frameBufferUpdateRequest[1] = 0;  // padding
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:2 fromInt:1];  // number of encodings
    [self populateU32Buffer:frameBufferUpdateRequest atPostion:4 fromInt:0];  // 0 = RAW
    [outputStream write:frameBufferUpdateRequest maxLength:8];
    free(frameBufferUpdateRequest);    
}

-(void) setPixelFormat{
    NSLog(@"Set PixelFormat to High");
    blueShift = 0;
    greenShift = 8;
    redShift = 16;
    blueMax = 255;
    redMax = 255;
    greenMax = 255;
    currentBitsPerPixel = 32;
    
    unsigned char *frameBufferUpdateRequest = (unsigned char *)calloc(20, sizeof(unsigned char));
    frameBufferUpdateRequest[0] = 0;  // message type is setPixelType
    frameBufferUpdateRequest[1] = 0;  // padding
    frameBufferUpdateRequest[2] = 0;  // padding
    frameBufferUpdateRequest[3] = 0;  // padding
    
    frameBufferUpdateRequest[4] = currentBitsPerPixel;   // bits per pixel
    frameBufferUpdateRequest[5] = 24;   // depth
    frameBufferUpdateRequest[6] = 0;   // big endian flag
    frameBufferUpdateRequest[7] = 1;   // true color flag
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:8 fromInt:redMax];  // red max
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:10 fromInt:greenMax];  // green max
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:12 fromInt:blueMax];  // blue max
    frameBufferUpdateRequest[14] = redShift;   // red shift
    frameBufferUpdateRequest[15] = greenShift;   // green shift
    frameBufferUpdateRequest[16] = blueShift;   // blue shift
    // 3 bytes padding
    
    [outputStream write:frameBufferUpdateRequest maxLength:20];
    free(frameBufferUpdateRequest);
    
    // set these values into the reciever too, so decoding happens as expected
}

-(void) setPixelFormatLow{
    NSLog(@"Set PixelFormat to Low");
    blueShift = 0;
    greenShift = 2;
    redShift = 5;
    blueMax = 3;
    redMax = 7;
    greenMax = 7;
    currentBitsPerPixel = 8;
    
    unsigned char *frameBufferUpdateRequest = (unsigned char *)calloc(20, sizeof(unsigned char));
    frameBufferUpdateRequest[0] = 0;  // message type is setPixelType
    frameBufferUpdateRequest[1] = 0;  // padding
    frameBufferUpdateRequest[2] = 0;  // padding
    frameBufferUpdateRequest[3] = 0;  // padding
    
    frameBufferUpdateRequest[4] = currentBitsPerPixel;   // bits per pixel
    frameBufferUpdateRequest[5] = 24;   // depth
    frameBufferUpdateRequest[6] = 0;   // big endian flag
    frameBufferUpdateRequest[7] = 1;   // true color flag
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:8 fromInt:redMax];  // red max
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:10 fromInt:greenMax];  // green max
    [self populateU16Buffer:frameBufferUpdateRequest atPostion:12 fromInt:blueMax];  // blue max
    frameBufferUpdateRequest[14] = redShift;   // red shift
    frameBufferUpdateRequest[15] = greenShift;   // green shift
    frameBufferUpdateRequest[16] = blueShift;   // blue shift
    // 3 bytes padding
    
    [outputStream write:frameBufferUpdateRequest maxLength:20];
    free(frameBufferUpdateRequest);
    
    // set these values into the reciever too, so decoding happens as expected
}

NSThread* frameRequestThread;
BOOL runFrameRequestLoop;

-(void)mainFrameRequestLoop{
    while (![frameRequestThread isCancelled]){
        if (runFrameRequestLoop){
            [self requestScreenUpdate];
        }
        [NSThread sleepForTimeInterval:0.1];
    }
}

-(void) ProcessInput{
    // NSLog(@"Process input");
    while ([inputStream hasBytesAvailable]){
        
        packetComplete = NO;
        
        // is there anything to do?
        if (currentPacket){
            packetComplete = [currentPacket processData];
        }
        
        // time to do the next thing
        if (packetComplete){
            // Just finished processing a VNCHandshakePacket
            if ([currentPacket isKindOfClass:[VNCHandshakePacket class]]){
                // got a handshake from the VNC Server
                VNCHandshakePacket* hsPacket = (VNCHandshakePacket*) currentPacket;
                NSString* s = [hsPacket handshakeString];
                [self respondToHandshakeof:s];  // this creates a newpacket
            }
            else if([currentPacket isKindOfClass:[VNCSecurityPacket class]]){
                VNCSecurityPacket* secPacket = (VNCSecurityPacket*) currentPacket;
                if ([secPacket securityType] == 1){
                    [self sendClientInitMessage];
                }
                else if ([secPacket securityType] == 2){
                    // send the result back to the server
                    // [outputStream write:[result bytes] maxLength:[result length]];
                    // [self setState:VNCMODE_Handshake_Security_Response];
                    //  the response packet is 0=ok, anything else = fail
                    // then send client init message
                }
            }
            else if([currentPacket isKindOfClass:[VNCClientInitPacket class]]){
                VNCClientInitPacket* initPacket = (VNCClientInitPacket*) currentPacket;
                // create the screen buffer
                [self allocateScreenBufferForSize:CGSizeMake([initPacket screenWidth], [initPacket screenHeight])];
                // pass the buffer to the renderers
                [[self renderer] setScreenBuffer:_screenBuffer withDataSize:_screenBufferSize bytesPerRow:_screenRowByteCount remoteScreenSize:CGSizeMake([initPacket screenWidth], [initPacket screenHeight])];
                if (secondaryRenderer){
                    [secondaryRenderer setScreenBuffer:_screenBuffer withDataSize:_screenBufferSize bytesPerRow:_screenRowByteCount remoteScreenSize:CGSizeMake([initPacket screenWidth], [initPacket screenHeight])];
                }
                currentBitsPerPixel = [initPacket bitsPerPixel];
                screenHeight = [initPacket screenHeight];
                screenWidth = [initPacket screenWidth];
                redShift = [initPacket redShift];
                greenShift = [initPacket greenShift];
                blueShift = [initPacket blueShift];
                redMax = [initPacket redMax];
                greenMax = [initPacket greenMax];
                blueMax = [initPacket blueMax];
                
                // send an initial frameBuffer update request
                [renderer setVncConnectionState:VNC_CONNECTION_REREDERING];
                if (secondaryRenderer){
                    [secondaryRenderer setVncConnectionState:VNC_CONNECTION_REREDERING];
                }
                currentPacket = [[VNCInteractionPacket alloc] initWithInputStream:inputStream andOutputStream:outputStream];
                [self setEncoding];
                
                if (useLowQuality){
                    [self setPixelFormatLow];   // use 8 bit
                }
                else{
                    [self setPixelFormat];    // use 24 bit
                }
                needsFullFrame = YES;
                runFrameRequestLoop = YES;
            }
            else if([currentPacket isKindOfClass:[VNCInteractionPacket class]]){
                VNCInteractionPacket* interactionPacket = (VNCInteractionPacket*) currentPacket;
                // send an incremental frameBuffer update request
                if ([interactionPacket messageType] == 0){
                    currentPacket = [[VNCFrameResponsePacket alloc] initWithInputStream:inputStream andOutputStream:outputStream andBitsPerPixel:currentBitsPerPixel VncController:self usingRedShift:redShift greenShift:greenShift blueShift:blueShift redMax:redMax greenMax:greenMax blueMax:blueMax];
                }
                if ([interactionPacket messageType] == 2){
                    // bell sound
                    currentPacket = [[VNCInteractionPacket alloc] initWithInputStream:inputStream andOutputStream:outputStream];
                }else{
                    //NSLog(@"Not expecting any packets that are not frame packets currently");
                }
            }
            else if([currentPacket isKindOfClass:[VNCFrameResponsePacket class]]){
                // finished the last interaction packet that was recieved, so set up for the next one
                currentPacket = [[VNCInteractionPacket alloc] initWithInputStream:inputStream andOutputStream:outputStream];
            }
        }
    }
}

#pragma mark public methods
- (void)updateScreenBufferWithData:(uint8_t*)theData withLength:(int)length andRect:(CGRect)rect{
    // copy the data into memory in the required location.
    // then later on can just create an image from the bit needed.
    long long yOffset = (rect.origin.y ) * _screenRowByteCount;
    long long xOffset = rect.origin.x * 4;
    long long copyLength = rect.size.width * 4;
    long long startLocation = xOffset + yOffset;
    
    //NSLog(@"RowSize = %lld x Offset = %lld yOffset = %lld", rowSize,xOffset,yOffset);
    for (long long row = 0; row < rect.size.height; row++) {
        //NSLog(@"copying from %lld for %lld bytes", startLocation,copyLength);
        memcpy(_screenBuffer + startLocation + (row * _screenRowByteCount) ,theData + (row * copyLength), copyLength );
    }
    
    NSValue* rectValue = [NSValue valueWithCGRect:rect];
    [renderer setNeedsDisplayInRemoteRect:rectValue];
    if (secondaryRenderer){
        [secondaryRenderer setNeedsDisplayInRemoteRect:rectValue];
    }
}

- (void) sendString:(NSString*) txString{
    if (![outputStream hasSpaceAvailable]){
        NSLog(@"outbuffer full");
        [self performSelector:@selector(sendString:) withObject:txString afterDelay:0.5];
    }
    else{
        NSData * dataToSend = [txString dataUsingEncoding:NSUTF8StringEncoding];
        // if output stream is not null of false ?
        if (outputStream) {
            // a length interator of somesort, supposedly for the character array?
            int remainingToWrite = [dataToSend length];
            // sends as a bytestream and sets up a generic pointer to it
            void * marker = (void *)[dataToSend bytes];
            [outputStream write:marker maxLength:remainingToWrite];
        }
    }
}

@end
