//
//  HypervisorConnectionFactory.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//
//  This factory class provides the 'template' for a hypervisor connection.  It enforces several principles that must be applicabale. 
//  These are;
//     1.  The hypervisor objects that are provided by a connection must (see HypObjReferencesProtocol).
//             a. Have a defined integer type. These types number can represent different objects for each hypervisor type, and can map
//                onto different enums as appropritate.
//             b. Have an NSString that defines a unique identifier for all hypervisor objects of that type within the connection. 
//             c. Be able to provide a list of other objectstypes and references on which it depends.  This is used to ensure that
//                an entire hypervisor object tree is loaded when required.
//     
//
#import "HypObjReferencesProtocol.h"

// declare forward reference to the class used in the protocol
@class HypervisorConnectionFactory;

@protocol HypervisorConnectionDelegate
@optional
- (void)hypervisorConnectedToConnection:(HypervisorConnectionFactory *)hypervisorConnection withResult:(BOOL)result;

/**
 Objects of a specific type in the hypervisor connection have been updated.  When collections of objects are updated in
 one message from the hypervisor a single on of these methods is called for each type.  If the caller is interested in the type
 only and not the specific identity of each object then this is the most efficient method to use.
 @param hypervisorConnection The Xen Hypervisor connection object that recieved the updated objects.
 @param objectType The type of object that has been updated.
 @param sucsess Indicates if the update was received without issues. 
 */
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withResult:(BOOL)sucsess;

/**
 Specific object in the hypervisor connection have been updated.  One of these methods is called for each object that is updated
 If the caller is interested in the type and the specific identity of each object then this is the method to use.
 @param hypervisorConnection The Xen Hypervisor connection object that recieved the updated objects.
 @param objectType The type of object that has been updated.
 @param objectRef The opaque reference of the object that has been updated.
 @param sucsess Indicates if the update was received without issues. 
 */
- (void)hypervisorObjectsUpdated:(HypervisorConnectionFactory *)hypervisorConnection updatesObjectsOfType:(int)objectType withReference:(NSString *)objectRef withResult:(BOOL)sucsess;

/**
 Called when a cmd response if received
 @param hypervisorConnection The Xen Hypervisor connection object that recieved the updated objects.
 @param requestCmd The cmd that was made.
 @param error The error if there was one
 */
- (void)hypervisorResponseReceived:(HypervisorConnectionFactory *)hypervisorConnection requestCmd:(NSString*)cmd error:(NSError*)error;
@end

@protocol ConnectionDefintionDelegate
- (int)numberOfProperties;
- (NSString *) titleForIndex:(int)index;
- (NSString *) connectionName;
- (NSString *) connectionAddress;
- (NSString *) connectionUsername;
- (NSString *) connectionPassword;
#if !TARGET_OS_IPHONE
- (NSView *) viewForIndex:(int) index;
#else
- (UIView *) viewForIndex:(int) index;
#endif
-(void)populateFields:(NSManagedObject *) fieldData;
-(void) saveDataToManagedObject:(NSManagedObject *) managedObject;

@end

typedef enum {
    HYPERVISOR_UNKNOWN,           // stops default values from non existant calls being troublesome
    HYPERVISOR_XEN,               // Xen Server connection
    HYPERVISOR_EC2,               // Amazon EC2 Cloud connection
    HYPERVISOR_CLOUDSTACK_NATIVE, // Citrix CloudStack connection (Direct no bridge required)    
    HYPERVISOR_CLOUDSTACK_AWS,    // Citrix CloudStack connection (AWS Compatible - for CloudBridge) not currently used or supported
} HypervisorType;

typedef enum {
     CONNECTION_UNCONNECTED,
     CONNECTION_CONNECTING,
     CONNECTION_CONNECTED,
     CONNECTION_SUSPENDING,
     CONNECTION_SUSPENDED,
     CONNECTON_RESUMING,
} ConnectionState;

@interface ConnectionDefinition : NSObject {
    HypervisorType hypervisorType;
    NSString *connectionString;
    NSString *_connectionaddress;
    NSString *_connectionName;
    NSString *_connectionUserName;
    NSString *_connectionPassword;

#if !TARGET_OS_IPHONE
    NSImage *icon;
#else
    UIImage *icon;
#endif
}

@property (readonly) HypervisorType hypervisorType;
@property (readonly) NSString *connectionString;
@property (readonly) NSString *connectionaddress;
@property (readonly) NSString *connectionName;
@property (readonly) NSString *connectionUserName;
@property (readonly) NSString *connectionPassword;

#if !TARGET_OS_IPHONE
@property (readonly) NSImage *icon;
#else
@property (readonly) UIImage *icon;
#endif

#if !TARGET_OS_IPHONE
-(id) initWithHypervisorType:(HypervisorType)type connectionName:(NSString *)connectionName icon:(NSImage *)image;
#else
-(id) initWithHypervisorType:(HypervisorType)type connectionName:(NSString *)connectionName icon:(UIImage *)image;
#endif
@end

@interface HypervisorConnectionFactory : NSObject {
    id<HypervisorConnectionDelegate> connectionDelegate;
    NSError *lastError;;
    NSString *connectionID;
    HypervisorType hypervisorConnectionType;
    /**
     The set of XenHypervisorConnectionDelegates to use when data has changed in the connection.
     */
    NSMutableSet *hypervisorConnectionDelegates;
    
    /**
     The cache of the objects stored for the current hypervisor connection
     keyed by the type, values are NSArray of objects appropriate for the specific
     connection type.
     */
    NSMutableDictionary *cachedHypObjects;
    
    /**
     Indicates if loaded objects should have their required references walked to ensure that the
     whole object graph is loaded.  If YES when populateObject is called any references to other
     XenBase objects will also be loaded if they are not determined to be in the cache.
     */
    BOOL walkTreeMode;
    
    /**
     A flag to indicate the object types that currently have data requests that have not yet received
     responses from the hypervisor server outstanding.
     */
    int pendingUpdates;
    
    /**
     A flag to indicate the object types that currently potentially have unRequested data.
     */
    int availableUpdates;
    
    /**
     remembers the current auto Update mode. Only set at initial connection time so as to ensure
     that changes made after this point do not get reflected in the connection without first disconnecting.
     */
    BOOL autoUpdateEnabled;
    
    /**
     Stores key value pairs for the specific connection
     */
    NSMutableDictionary* connectionMetadata;
    
    NSString* connectionUserName;
}

@property (strong) NSError *lastError;
@property (strong) NSString *connectionID;
@property (readonly) HypervisorType hypervisorConnectionType;
@property BOOL walkTreeMode;
@property (readonly) NSString *connectionUserName;


- (id) getConnectionMetadataForKey:(NSString*)key;
- (void) setConnectionMetadataVlue:(id) value ForKey:(NSString*)key;

/**
 * The dictionary of cachedHypObjects.  
 * keyed on type, values are NSArrays of the objects for that type
 * the types will be specific to the connection type.
 */
- (NSMutableDictionary*) cachedHypObjects;

/**
 * updates the cache of hypervisor objects for the specified type
 * the provided list will replce the current set of objects for the type.
 * @param objects the array of objects for the cache
 * @param the type specifier for the objects
 */
- (void) updateCachedHypObjects:(NSArray *)objects forObjectType:(int)objectType isCompleteSet:(BOOL)completeSet;

/**
 * updates the cache of hypervisor objects for the specified type
 * the provided list will replce the current set of objects for the type.
 * @param objects the array of objects for the cache
 * @param objectType the type specifier for the objects
 * @param resetPending indicates if the pending updates flag should also be cleared (i.e. this is the complete set of objects).
 */
- (void) updateCachedHypObjects:(NSArray *)objects forObjectType:(int)objectType ResetPendingUpdates:(BOOL)resetPending isCompleteSet:(BOOL)completeSet;

/**
 * removes the specified object from the chache
 * @param objectRef the reference for the object that uniquely defines the object of the specified type
 * @param objectType the type of the object to be removed.
 */
- (void) removeCachedHypObjectswithReference:(NSString *)objectRef forObjectType:(int)objectType;

/**
 Flushes the hypervisor Object cache
 @param object the object that is at the root of the tree that is required to be removed.
 */
- (void)flushHypObjectCache;

/**
 Determines if the hypervisor object type specified has an outstanding request waiting for a response
 @param hypObjectType The type of objects to requrest.
 @return YES if there is an outstanding request otherwise NO.
 */
- (BOOL)isUpdatePendingForType:(int)hypObjectType;

/**
 Clears all the outstanding request flags for the specified object type
 */
- (void) clearAllObjectTypeRequestPending;

/**
 Clears the outstanding request flags for the specified object type
 @param objectType The type of objects to requrest.
 */
- (void) clearObjectTypeRequestPending:(int) objectType;

/**
 sets the outstanding request flags for the specified object type
 @param objectType The type of objects to requrest.
 */
- (void) setObjectTypeRequestPending:(int) objectType;

/**
 Determine if there are updates available for the specified object type.
 This assumes that in 
 AutoUpdate mode that once all objects of a specified type have been requested that
 the automatic update mechanism will keep the list up to date.
 Manual update mode that the lists are never up to date and can always be retreived
 @param hypObjectType The type of objects to requrest.
 @return YES if there is an outstanding request otherwise NO.
 */
- (BOOL)isUpdateAvailableForType:(int)hypObjectType;

/**
Returns all the hypervisor object of the specified type from the cache.  
This does not cause further data to be featched from the servers themseleves, just reads the data that
is already stored in the cache.
@param hypObjectType the hjypervisor object type
@param predicate the predicate to apply to the objects being searched for.
@return the NSArray of XenBase objects that are located.
*/
- (NSArray *)hypObjectsForType:(int)hypObjectType;

/**
 Returns the hypervisor object of the specified type and condition from the cache. 
 This does not cause further data to be featched from the servers themseleves, just reads the data that
 is already stored in the cache.
 This allows reduced sets of objects to be returned with the rules as to which ones are returned
 defined by the consumer of the connection.  Some of the Xen object wrappers provide static methods
 to help provide the condition predicates for common conditons (i.e. the XenVM class).
 @param hypObjectType the hjypervisor object type
 @param predicate the predicate to applt to the objects being searched for.
 @return the NSArray of XenBase objects that are located.
 */
- (NSArray *)hypObjectsForType:(int)hypObjectType withCondition:(NSPredicate *)predicate;

/**
 * Provides the dictionary of avaialble connection types that the factory can provide
 * this is a list of all the values in the hypervisorType enum
 */
+(NSArray *) connectionDefinitions;

/**
 * provides the dictionary of connections that are currently made
 */
+(NSMutableDictionary*) connectionDictionary;

// Our class cluster's public API:
- (id)initWithHypervisorType:(HypervisorType)hypervisorType usingAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password delegate:(id<HypervisorConnectionDelegate>)delegate;
+ (HypervisorConnectionFactory *) getConnectionWithHypervisorType:(HypervisorType)hypervisorType connectonID:(NSString *)connID;

/**
 Add a delegate to the hypervisor connection that will be informed of cached data changes.
 @param delegate The XenHypervisorConnectionDelegate to register with the connection.
 */
- (void)addHypervisorConnectionDelegate:(id<HypervisorConnectionDelegate>)delegate;

/**
 Remove a delegate to the hypervisor connection.
 After removal the delegate will no longer be informed of cached data changes.
 @param delegate The XenHypervisorConnectionDelegate to un-register with the connection.
 */
- (void)removeHypervisorConnectionDelegate:(id<HypervisorConnectionDelegate>)delegate;

/**
 Remove all delegates to the hypervisor connection.
 After removal the delegates will no longer be informed of cached data changes.
 */
- (void)removeAllHypervisorConnectionDelegate;

/**
 *  release any memory used by the connection
 * basically this flushes the cached hyp objects.
 */
- (void)releaseMemory;

/**
 Ensures that a specific hypervisor object has all its required sub elements available.
 @param reference The unique object reference to check
 @param hypObjectType the type of object to check
 @param checkOnly if true only checks otherwise will actually populate the required objects into the cache
 */
- (BOOL) PopulateHypObjectreference:(NSString *)reference objectType:(int)hypObjectType checkOnly:(BOOL)checkOnly;

/**
 Checks that an object has its entire object graph loaded into the cache.  The objects in the cache may be out of date
 as there is no way to check this, but at least a version of all objects in the graph must be available. for it to return
 YES.
 @param object the object that is at the root of the tree that is required to be populated.
 @return YES if all objects in the graph are in the connection cache otherwise NO.
 */
- (BOOL) CheckPopulatedHypObject:(id<HypObjReferencesProtocol>)object;

- (void) PopulateHypObject:(id<HypObjReferencesProtocol>)object;

- (BOOL) isAutoUpdateEnabled;

- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password;

@end

#pragma mark -

@interface HypervisorConnectionFactory (AbstractMethods)
// Define Abstract methods which should be implemented by subclasses:

- (ConnectionState)GetConnectionState;
- (void)closeConnection;
- (void)reloadCoreData;
+ (BOOL) isAutoUpdateConfigured;
- (NSSortDescriptor*) sortForhypObjectType:(int)hypObjectType;
// a dictionary that can be used to store key value pairs associtated with this specific connection.
- (NSDictionary *)GetConnectionDictionary;

/**
 Ensures that a specific hypervisor object has all its required sub elements removed from the cache.
 @param object The HypObjReferencesProtocol hypervisor object to remove from the cache
 */
- (void) clearHypObjectTreeWithRoot:(id<HypObjReferencesProtocol>)object;
/**
 Request all data objects for a specific type from the Hypervisor connection.  This is an async request.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObjectsForType:(int)hypObjectType;

/**
 Request specific data object for a specific type from the Hypervisor connection.  This is an async request.
 @param reference The reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType;

/**
 * This method should do anything required to pause the connection so taht
 * whilst the application is inactive, the app does not crash on return
 * i.e. suspending the any eventing mechanism
 **/
- (void)suspendConnection;

/**
 * This method should do anything required to resume the connection after a susspend
 **/
- (void)resumeConnection;

/**
 * provides the propertyName of the unique key for the object of the specified type
 * @param objectType the type of object that the key is for
 * @return The property name that is used to provide uniquness for objects of the specified type
 */
-(NSString *) uniqueObjectReferencePropertyForHypObjectType:(int)objectType;

@end

#pragma mark -

@interface HypervisorConnectionFactory (SubclassUseOnly)
// Define Private, concrete methods which should only be used by subclasses:
// i.e. - (void)setSearchEngineName:(NSString *)newSearchEngineName;


@end
