//
//  CloudStackVMDetail.h
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
// Represents the details for a VM.  This is a small subset of the VM data is stored in VMCore. 
// The full details of a VM are obtained separatly on request and stored in this object

#import "CloudStackBase.h"

@interface CloudStackVMDetail : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* displayname;
@property (readonly) NSString* instancename;
@property (readonly) NSString* hypervisor;
@property (readonly) NSString* account;
@property (readonly) NSString* state;
@property (readonly) NSString* domain;
@property (readonly) NSString* guestosid;
@property (readonly) NSString* hostname;

@property (readonly) NSString* memory;
@property (readonly) NSString* cpunumber;
@property (readonly) NSString* cpuused;
@property (readonly) NSString* cpuspeed;
@property (readonly) NSString* forvirtualnetwork;
@property (readonly) NSString* networkkbsread;
@property (readonly) NSString* networkkbswrite;

+ (NSPredicate *) vmWithReference:(NSString *)vmId;

+ (NSPredicate *) vms_OwnedByMe:(NSString *)myAccountName;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByDisplayName:(CloudStackVMCore *)otherObject;


@end
