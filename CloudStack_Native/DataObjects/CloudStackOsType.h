//
//  CloudStackOsType.h
//  hypOps
//
//  Created by Ian Firth on 14/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackOsType : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* description;

+ (NSPredicate *) osTypeWithReference:(NSString *)osTypeId;

@end
