//
//  CloudStackUser.m
//  hypOps
//
//  Created by Ian Firth on 10/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackUser.h"

@implementation CloudStackUser

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_USER   Dictionary:properties];
    if (self){
        //_displayText = [properties valueForKey:@"displayText"];
        // this has a list of users associated with the account too.
        // perhaps I should split this into a separate object too
    }
    return self;
}

@end
