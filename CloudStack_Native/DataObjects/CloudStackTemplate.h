//
//  CloudStackTemplate.h
//  hypOps
//
//  Created by Ian Firth on 05/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackTemplate : CloudStackBase 

@property (readonly) NSString* displaytext;
@property (readonly) NSString* hypervisor;
@property (readonly) NSString* account;
@property (readonly) NSString* domain;
@property (readonly) NSString* ostypename;
@property (readonly) NSString* templatetype;

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

+ (NSPredicate *) templateWithReference:(NSString *)vmId;

+ (NSPredicate *) templates_OwnedByMe:(NSString *)myAccountName;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByName:(CloudStackVMCore *)otherObject;
@end