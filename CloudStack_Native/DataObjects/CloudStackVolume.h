//
//  CloudStackVolume.h
//  hypOps
//
//  Created by Ian Firth on 13/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackVolume : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* account;
@property (readonly) NSString* diskofferingname;
@property (readonly) NSString* serviceofferingname;
@property (readonly) NSString* domain;
@property (readonly) NSString* hypervisor;
@property (readonly) NSString* project;
@property (readonly) NSString* state;
@property (readonly) NSString* storagetype;
@property (readonly) NSString* type;
@property (readonly) NSString* vmdisplayname;
@property (readonly) NSString* created;
@property (readonly) NSString* destroyed;
@property (readonly) NSString* storage;
@property (readonly) NSString* size;

+ (NSPredicate *) volumeWithReference:(NSString *)volumeId;

+ (NSPredicate *) volumes_OwnedByMe:(NSString *)myAccountName;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByName:(CloudStackVolume *)otherObject;

@end
