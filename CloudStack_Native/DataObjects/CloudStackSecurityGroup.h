//
//  CloudStackSecurityGroup.h
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackSecurityGroup : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* description;
@property (readonly) NSString* account;
@property (readonly) NSString* domain;
@property (readonly) NSString* project;

+ (NSPredicate *) securityGroupWithReference:(NSString *)securityGroupId;

+ (NSPredicate *) securityGroups_OwnedByMe:(NSString *)myAccountName;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByName:(CloudStackSecurityGroup *)otherObject;

@end
