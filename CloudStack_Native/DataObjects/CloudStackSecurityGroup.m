//
//  CloudStackSecurityGroup.m
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackSecurityGroup.h"

@implementation CloudStackSecurityGroup

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_SECURITYGROUP Dictionary:properties];
    if (self){
    }
    return self;
}

-(NSString*) description{
    return [allProperties valueForKey:@"description"];
}

-(NSString*) account{
    return [allProperties valueForKey:@"account"];    
}

-(NSString*) domain{
    return [allProperties valueForKey:@"domain"];    
}

-(NSString*) project{
    return [allProperties valueForKey:@"project"];    
}

+ (NSPredicate *) securityGroupWithReference:(NSString *)securityGroupId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",securityGroupId]];
}

+ (NSPredicate *) securityGroups_OwnedByMe:(NSString *)myAccountName{
    return [NSPredicate predicateWithFormat:@"account like '%@'",myAccountName];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(CloudStackSecurityGroup *)otherObject
{
    return [[self name] compare:[otherObject name]];
}

@end
