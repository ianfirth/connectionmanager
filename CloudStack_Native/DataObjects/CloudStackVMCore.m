//
//  CloudStackVMCore.m
//  hypOps
//
//  Created by Ian Firth on 31/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackVMCore.h"

@implementation CloudStackVMCore

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_VM_CORE   Dictionary:properties];
    if (self){

        // add a reference to itself for all properties in extending object
        [self addReference:[self uniqueReference] forObjectType:HYPOBJ_CS_VM];
    }
    return self;
}

-(NSString*)displayname{
    return [allProperties valueForKey:@"displayname"];
}

-(NSString*)instancename{
    return [allProperties valueForKey:@"instancename"];
}

-(NSString*) hypervisor{
    return [allProperties valueForKey:@"hypervisor"];}

-(NSString*) account{
    return [allProperties valueForKey:@"account"];
}

-(NSString*) guestosid{
    return [allProperties valueForKey:@"guestosid"];
}

-(NSString*) state{
    return [allProperties valueForKey:@"state"];
}

-(NSString*) domain{
    return [allProperties valueForKey:@"domain"];
}

-(NSString*) hostname{
    return [allProperties valueForKey:@"hostname"];
}

-(NSString*) serviceofferingname{
    return [allProperties valueForKey:@"serviceofferingname"];
}

+ (NSPredicate *) vmWithReference:(NSString *)vmId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",vmId]];
}

+ (NSPredicate *) vms_OwnedByMe:(NSString *)myAccountName{
    return [NSPredicate predicateWithFormat:@"account like '%@'",myAccountName];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"displayname beginsWith [c]'%@' OR displayname contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"displayname beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"displayname == nil OR displayname ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByDisplayName:(CloudStackVMCore *)otherObject
{
    return [[self displayname] compare:[otherObject displayname]];
}

@end
