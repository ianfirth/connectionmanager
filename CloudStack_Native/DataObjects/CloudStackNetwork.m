//
//  CloudStackNetwork.m
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackNetwork.h"

@implementation CloudStackNetwork

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_NETWORK   Dictionary:properties];
    if (self){
    }
    return self;
}

-(NSString*) account{
    return [allProperties valueForKey:@"account"];
}

-(NSString*) displaytext{
    return [allProperties valueForKey:@"displaytext"];
}

-(NSString*) traffictype{
    return [allProperties valueForKey:@"traffictype"];    
}

-(NSString*) type{
    return [allProperties valueForKey:@"type"];    
}

-(NSString*) domain{
    return [allProperties valueForKey:@"domain"];    
}

-(NSString*) state{
    return [allProperties valueForKey:@"state"];    
}

-(NSString*) project{
return [allProperties valueForKey:@"project"];    
}

-(NSString*) networkofferingname{
return [allProperties valueForKey:@"networkofferingname"];    
}

-(NSString*) networkdomain{
return [allProperties valueForKey:@"networkdomain"];    
}

-(NSString*) physicalnetworkid{
return [allProperties valueForKey:@"physicalnetworkid"];    
}

-(NSString*) dns1{
return [allProperties valueForKey:@"dns1"];    
}

-(NSString*) dns2{
return [allProperties valueForKey:@"dns2"];    
}

-(NSString*) netmask{
return [allProperties valueForKey:@"netmask"];    
}

-(NSString*) vlan{
return [allProperties valueForKey:@"vlan"];    
}

-(NSString*) gateway{
return [allProperties valueForKey:@"gateway"];    
}

-(NSString*) broadcaseuri{
return [allProperties valueForKey:@"broadcaseuri"];    
}

-(NSString*) broadcasedomaintype{
return [allProperties valueForKey:@"broadcasedomaintype"];    
}

-(NSString*) acltype{
return [allProperties valueForKey:@"acltype"];    
}

-(NSString*) subdomainaccess{
return [allProperties valueForKey:@"subdomainaccess"];    
}

-(NSString*) restartrequired{
return [allProperties valueForKey:@"restartrequired"];    
}


+ (NSPredicate *) networkWithReference:(NSString *)networkId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",networkId]];
}

+ (NSPredicate *) networks_OwnedByMe:(NSString *)myAccountName{
    return [NSPredicate predicateWithFormat:@"account like '%@'",myAccountName];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(CloudStackNetwork *)otherObject
{
    return [[self name] compare:[otherObject name]];
}

@end
