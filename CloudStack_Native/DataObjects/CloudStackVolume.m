//
//  CloudStackVolume.m
//  hypOps
//
//  Created by Ian Firth on 13/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackVolume.h"

@implementation CloudStackVolume

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_VOLUME   Dictionary:properties];
    if (self){
    }
    return self;
}

-(NSString*) account{
    return [allProperties valueForKey:@"account"];
}

-(NSString*) diskofferingname{
    return [allProperties valueForKey:@"diskofferingname"];
}

-(NSString*) serviceofferingname{
    return [allProperties valueForKey:@"serviceofferingname"];
}

-(NSString*) domain{
    return [allProperties valueForKey:@"domain"];
}

-(NSString*) hypervisor{
    return [allProperties valueForKey:@"hypervisor"];
}

-(NSString*) project{
    return [allProperties valueForKey:@"project"];
}

-(NSString*) state{
    return [allProperties valueForKey:@"state"];
}

-(NSString*) storagetype{
    return [allProperties valueForKey:@"storagetype"];
}

-(NSString*) type{
    return [allProperties valueForKey:@"type"];
}

-(NSString*) vmdisplayname{
    return [allProperties valueForKey:@"vmdisplayname"];
}

-(NSString*) created{
    return [allProperties valueForKey:@"created"];
}

-(NSString*) destroyed{
    return [allProperties valueForKey:@"destroyed"];
}

-(NSString*) storage{
    return [allProperties valueForKey:@"storage"];
}

-(NSString*) size{
    return [allProperties valueForKey:@"size"];
}

+ (NSPredicate *) volumeWithReference:(NSString *)volumeId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",volumeId]];
}

+ (NSPredicate *) volumes_OwnedByMe:(NSString *)myAccountName{
    return [NSPredicate predicateWithFormat:@"account like '%@'",myAccountName];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(CloudStackVolume *)otherObject
{
    return [[self name] compare:[otherObject name]];
}

@end
