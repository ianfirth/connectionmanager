//
//  CloudStackOsType.m
//  hypOps
//
//  Created by Ian Firth on 14/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackOsType.h"

@implementation CloudStackOsType

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_OSTYPE Dictionary:properties];
    if (self){
    }
    return self;
}

-(NSString*) description{
    return [allProperties valueForKey:@"description"];
}

+ (NSPredicate *) osTypeWithReference:(NSString *)osTypeId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",osTypeId]];
}


@end
