//
//  CloudStackBase.m
//  hypOps
//
//  Created by Ian Firth on 30/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@implementation CloudStackBase


@synthesize connectionID, uniqueReference, references, objectType;

#pragma mark -
#pragma mark Predicates

+ (NSString*) ObjectNameForType:(int)hypObjectType{
    NSString* objectType = nil;
    
    if (hypObjectType & HYPOBJ_CS_VM_CORE){      
        objectType = @"HYPOBJ_CS_VM_CORE";
    }
    if (hypObjectType & HYPOBJ_CS_VM){      
        objectType = @"HYPOBJ_CS_VM";
    }
    if (hypObjectType & HYPOBJ_CS_HOST_CORE){      
        objectType = @"HYPOBJ_CS_HOST_CORE";
    }
    if (hypObjectType & HYPOBJ_CS_HOST){      
        objectType = @"HYPOBJ_CS_HOST";
    }
    if (hypObjectType & HYPOBJ_CS_ZONE){      
        objectType = @"HYPOBJ_CS_ZONE";
    }
    if (hypObjectType & HYPOBJ_CS_SECURITYGROUP){      
        objectType = @"HYPOBJ_CS_SECURITYGROUP";
    }
    if (hypObjectType & HYPOBJ_CS_NETWORK){      
        objectType = @"HYPOBJ_CS_NETWORK";
    }
    if (hypObjectType & HYPOBJ_CS_VOLUME){      
        objectType = @"HYPOBJ_CS_VOLUME";
    }
    if (hypObjectType & HYPOBJ_CS_OSTYPE){      
        objectType = @"HYPOBJ_CS_OSTYPE";
    }
    if (hypObjectType & HYPOBJ_CS_ACCOUNT){      
        objectType = @"HYPOBJ_CS_ACCOUNT";
    }
    if (hypObjectType & HYPOBJ_CS_NIC){      
        objectType = @"HYPOBJ_CS_NIC";
    }
    if (hypObjectType & HYPOBJ_CS_USER){      
        objectType = @"HYPOBJ_CS_USER";
    }

    return objectType;
}

/**
 * provides a sort descriptor for each object type
 */
+ (NSSortDescriptor*) defaultSortForhypObjectType:(int)hypObjectType{

    // by default sort the objects by name.  Some like the VM have a funny ID in name so need to sort by another property
    switch (hypObjectType) {
        case HYPOBJ_CS_VM_CORE:
            return [[NSSortDescriptor alloc] initWithKey:@"displayname" ascending:YES];
            break;
        case HYPOBJ_CS_NIC:
            return [[NSSortDescriptor alloc] initWithKey:@"macaddress" ascending:YES];
            break;
        default:
            return [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            break;
    }
}

- (id) initWithConnectionId: (NSString*)ConnectionId objectType:(int)hypObjectType Dictionary:(NSDictionary *)properties{
    self = [super init];
    if (self){
        objectType = hypObjectType;
        [self setUniqueReference:[properties valueForKey:@"id"]];
        allProperties = [NSMutableDictionary dictionaryWithDictionary:properties];
        [self setReferences :[NSMutableDictionary dictionaryWithCapacity:0]];
        [self setConnectionID: ConnectionId];
    }
    return self;
}

-(NSString*) name{
    return [allProperties valueForKey:@"name"];
}

-(NSDictionary*) properties{
    return  allProperties;
}

-(CloudStackNativeHypervisorConnection *) hypervisorConnection{
    return (CloudStackNativeHypervisorConnection *)[HypervisorConnectionFactory getConnectionWithHypervisorType:HYPERVISOR_CLOUDSTACK_NATIVE connectonID:[self connectionID]];
}

- (void) addReference:(NSString *)reference forObjectType:(int)hypObjectType{
    if (reference != nil && 
        ![reference isEqualToString:@""]){
        NSNumber *key = [NSNumber numberWithInt:hypObjectType];
        if ([references objectForKey:key] == nil){
            NSMutableArray *refs = [NSMutableArray arrayWithCapacity:1];
            [references setObject:refs forKey:key];
        }
        
        NSMutableArray *existingRefs = [references objectForKey:key];
        [existingRefs addObject:reference];
    }
}

- (void) addReferences:(NSArray *)theReferences forObjectType:(int)hypObjectType{
    NSNumber *key = [NSNumber numberWithInt:hypObjectType];
    if ([references objectForKey:key] == nil){
        NSMutableArray *refs = [NSMutableArray arrayWithCapacity:1];
        [references setObject:refs forKey:key];
    }
    NSMutableArray *existingRefs = [references objectForKey:key];
    // remove nulls here
    for (NSString *theReference in theReferences){
        if (![theReference isEqualToString:@""]){
            [existingRefs addObject:theReference];
        }    
    }
}

- (NSArray *) referencesForType:(int)hypObjectType{
    NSNumber *key = [NSNumber numberWithInt:hypObjectType];
    NSArray* existingRefs = [references objectForKey:key];
    return existingRefs;
}

@end
