//
//  CloudStackVMDetail.m
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackVMDetail.h"
#import "CloudStackSecurityGroup.h"
#import "CloudStackNIC.h"

@implementation CloudStackVMDetail

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_VM   Dictionary:properties];
    if (self){
        // parse the specific properties
        
        // parse out the ID for the security groups so that the already loaded object can be linked to.
        NSArray* sgs = [properties valueForKey:@"securitygroup"];
        for (NSDictionary* sg in sgs){
            //replace any existing security group with this one as this has all the data (up to date)
            CloudStackSecurityGroup* cs_sg = [[CloudStackSecurityGroup alloc] initWithConnectionId:ConnectionId Dictionary:sg];
            [[self hypervisorConnection] updateCachedHypObjects:[NSArray arrayWithObject:cs_sg] forObjectType:HYPOBJ_CS_SECURITYGROUP ResetPendingUpdates:NO isCompleteSet:NO];        
            
            NSString* sgId = [sg valueForKey:@"id"];
            [self addReference:sgId forObjectType:HYPOBJ_CS_SECURITYGROUP];
        }

        // parse out the ID for the security groups so that the already loaded object can be linked to.
        NSArray* nics = [properties valueForKey:@"nic"];
        for (NSDictionary* nic in nics){
            //replace any existing nics with this one as this has all the data (up to date)
            CloudStackNIC* cs_nic = [[CloudStackNIC alloc] initWithConnectionId:ConnectionId Dictionary:nic];
            [[self hypervisorConnection] updateCachedHypObjects:[NSArray arrayWithObject:cs_nic] forObjectType:HYPOBJ_CS_NIC ResetPendingUpdates:NO isCompleteSet:NO];        
            
            NSString* nicId = [nic valueForKey:@"id"];
            [self addReference:nicId forObjectType:HYPOBJ_CS_NIC];
        }

        // link to the guest OS ID for the VM
        NSString* guestOsId = [properties valueForKey:@"guestosid"];
        [self addReference:guestOsId forObjectType:HYPOBJ_CS_OSTYPE];

    }
    return self;
}

-(NSString*)displayname{
    return [allProperties valueForKey:@"displayname"];
}

-(NSString*)instancename{
    return [allProperties valueForKey:@"instancename"];
}

-(NSString*) hypervisor{
    return [allProperties valueForKey:@"hypervisor"];}

-(NSString*) account{
    return [allProperties valueForKey:@"account"];
}

-(NSString*) guestosid{
    return [allProperties valueForKey:@"guestosid"];
}

-(NSString*) state{
    return [allProperties valueForKey:@"state"];
}

-(NSString*) domain{
    return [allProperties valueForKey:@"domain"];
}

-(NSString*) hostname{
    return [allProperties valueForKey:@"hostname"];
}

-(NSString*) serviceofferingname{
    return [allProperties valueForKey:@"serviceofferingname"];
}

-(NSString*) memory{
    return [allProperties valueForKey:@"memory"];
}

-(NSString*) cpunumber{
    return [allProperties valueForKey:@"cpunumber"];
}

-(NSString*) cpuused{
    return [allProperties valueForKey:@"cpuused"];
}

-(NSString*) cpuspeed{
    return [allProperties valueForKey:@"cpuspeed"];
}

-(NSString*) forvirtualnetwork{
    return [allProperties valueForKey:@"forvirtualnetwork"];
}

-(NSString*) networkkbsread{
    return [allProperties valueForKey:@"networkkbsread"];
}

-(NSString*) networkkbswrite{
    return [allProperties valueForKey:@"networkkbswrite"];
}


+ (NSPredicate *) vmWithReference:(NSString *)vmId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",vmId]];
}

+ (NSPredicate *) vms_OwnedByMe:(NSString *)myAccountName{
    return [NSPredicate predicateWithFormat:@"account like '%@'",myAccountName];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"displayname beginsWith [c]'%@' OR displayname contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"displayname beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"displayname == nil OR displayname ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByDisplayName:(CloudStackVMDetail *)otherObject
{
    return [[self displayname] compare:[otherObject displayname]];
}
@end
