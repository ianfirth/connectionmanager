//
//  CloudStackVMCore.h
//  hypOps
//
//  Created by Ian Firth on 31/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//
// Represents the core details for a VM.  This is a small subset of the VM data 
// the full details of a VM are obtained separatly on request and stored in the 
// VMDetail object

#import "CloudStackBase.h"

@interface CloudStackVMCore : CloudStackBase{
}

@property (readonly) NSString* displayname;
@property (readonly) NSString* instancename;
@property (readonly) NSString* hypervisor;
@property (readonly) NSString* account;
@property (readonly) NSString* state;
@property (readonly) NSString* domain;
@property (readonly) NSString* guestosid;
@property (readonly) NSString* hostname;

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

+ (NSPredicate *) vmWithReference:(NSString *)vmId;

+ (NSPredicate *) vms_OwnedByMe:(NSString *)myAccountName;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByDisplayName:(CloudStackVMCore *)otherObject;

@end
