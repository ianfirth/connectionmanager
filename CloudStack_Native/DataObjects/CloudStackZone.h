//
//  CloudStackZone.h
//  hypOps
//
//  Created by Ian Firth on 31/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackZone : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* name;
@property (readonly) NSString* description;
@property (readonly) NSString* allocationstate;
@property (readonly) NSString* dhcpprovider;
@property (readonly) NSString* dns1;
@property (readonly) NSString* dns2;
@property (readonly) NSString* domain;
@property (readonly) NSString* domainname;
@property (readonly) NSString* networktype;
@property (readonly) NSString* securitygroupenabled;
@property (readonly) NSString* vlan;

// metrics data
@property (readonly) double percentused_memory;
@property (readonly) double percentused_cpu;
@property (readonly) double percentused_storage;
@property (readonly) double percentused_storageAllocated;
@property (readonly) double percentused_vnetPublicIP;
@property (readonly) double percentused_privateIP;
@property (readonly) double percentused_secondaryStorage;
@property (readonly) double percentused_vlan;
@property (readonly) double percentused_directAttachedPublicIP;
@property (readonly) double percentused_localStorage;


+ (NSPredicate *) zoneWithReference:(NSString *)zoneId;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByName:(CloudStackZone *)otherObject;

@end
