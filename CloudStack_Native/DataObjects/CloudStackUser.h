//
//  CloudStackUser.h
//  hypOps
//
//  Created by Ian Firth on 10/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackUser : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@end
