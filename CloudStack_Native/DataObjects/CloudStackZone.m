//
//  CloudStackZone.m
//  hypOps
//
//  Created by Ian Firth on 31/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackZone.h"

// definitions for the various types of capacity that can be associated with a zone
#define CS_CAPACITY_TYPE_MEMORY 0
#define CS_CAPACITY_TYPE_CPU 1
#define CS_CAPACITY_TYPE_STORAGE 2
#define CS_CAPACITY_TYPE_STORAGE_ALLOCATED 3
#define CS_CAPACITY_TYPE_VIRTUAL_NETWORK_PUBLIC_IP 4
#define CS_CAPACITY_TYPE_PRIVATE_IP 5
#define CS_CAPACITY_TYPE_SECONDARY_STORAGE 6
#define CS_CAPACITY_TYPE_VLAN 7
#define CS_CAPACITY_TYPE_DIRECT_ATTACHED_PUBLIC_IP 8
#define CS_CAPACITY_TYPE_LOCAL_STORAGE 9
@interface CloudStackZone ()
-(double) capacityValueProperty:(NSString*)property forType:(int)type;
@end
    
@implementation CloudStackZone

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_ZONE   Dictionary:properties];
    if (self){
    }
    return self;
}

-(NSString *)name{
    return [allProperties valueForKey:@"name"];
}

-(NSString *)description{
    return [allProperties valueForKey:@"description"];
}

-(NSString *)vlan{
    return [allProperties valueForKey:@"vlan"];
}

-(NSString *)securitygroupenabled{
    return [allProperties valueForKey:@"securitygroupenabled"];
}

-(NSString *)networktype{
    return [allProperties valueForKey:@"networktype"];
}

-(NSString *)domainname{
    return [allProperties valueForKey:@"domainname"];
}

-(NSString *)domain{
    return [allProperties valueForKey:@"domain"];
}

-(NSString *)dns2{
    return [allProperties valueForKey:@"dns2"];
}

-(NSString *)dns1{
    return [allProperties valueForKey:@"dns1"];
}

-(NSString *)dhcpprovider{
    return [allProperties valueForKey:@"dhcpprovider"];
}

-(NSString *)allocationstate{
    return [allProperties valueForKey:@"allocationstate"];
}

-(double) percentused_memory{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_MEMORY];
}

-(double) percentused_cpu{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_CPU];
}

-(double) percentused_storage{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_STORAGE];
}

-(double) percentused_storageAllocated{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_STORAGE_ALLOCATED];
}

-(double) percentused_vnetPublicIP{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_VIRTUAL_NETWORK_PUBLIC_IP];
}

-(double) percentused_privateIP{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_PRIVATE_IP];
}

-(double) percentused_secondaryStorage{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_SECONDARY_STORAGE];
}

-(double) percentused_vlan{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_VLAN];
}

-(double) percentused_directAttachedPublicIP{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_DIRECT_ATTACHED_PUBLIC_IP];
}

-(double) percentused_localStorage{
    return [self capacityValueProperty:@"percentused" forType:CS_CAPACITY_TYPE_LOCAL_STORAGE];
}


-(double) capacityValueProperty:(NSString*)property forType:(int)type{
    NSArray* allCapacities = [allProperties valueForKey:@"capacity"];
    for (NSDictionary* capacity in allCapacities){
        if ([[capacity valueForKey:@"type"] intValue] == type){
            return [[capacity valueForKey:property] doubleValue];            
        }
    }
    return 0;
}

+ (NSPredicate *) zoneWithReference:(NSString *)zoneId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",zoneId]];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(CloudStackZone *)otherObject
{
    return [[self name] compare:[otherObject name]];
}


@end
