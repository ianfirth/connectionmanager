//
//  CloudStackNIC.m
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackNIC.h"

@implementation CloudStackNIC
- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_NIC Dictionary:properties];
    if (self){
        // link to the guest OS ID for the VM
        NSString* networkId = [properties valueForKey:@"networkid"];
        [self addReference:networkId forObjectType:HYPOBJ_CS_NETWORK];

    }
    return self;
}

-(NSString*) macaddress{
    return [allProperties valueForKey:@"macaddress"];
}

-(NSString*) ipaddress{
    return [allProperties valueForKey:@"ipaddress"];
}

-(NSString*) netmask{
    return [allProperties valueForKey:@"netmask"];
}

-(NSString*) traffictype{
    return [allProperties valueForKey:@"traffictype"];
}

-(NSString*) type{
    return [allProperties valueForKey:@"type"];
}

-(NSString*) gateway{
    return [allProperties valueForKey:@"gateway"];
}

-(NSString*) broadcastuir{
    return [allProperties valueForKey:@"broadcastuir"];
}

-(NSString*) isolationuri{
    return [allProperties valueForKey:@"isolationuri"];
}

+ (NSPredicate *) nicWithReference:(NSString *)nicId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",nicId]];
}

+ (NSPredicate *) macaddressBeginsOrContainsAWordBeginningWith:(NSString *)macaddress{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"macaddress beginsWith [c]'%@' OR macaddress contains [c]' %@'",macaddress,macaddress]];
}

+ (NSPredicate *) macaddressBeginWith:(NSString *)macaddress{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"macaddress beginsWith [c]'%@'",macaddress]];    
}

+ (NSPredicate *) macaddressNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"macaddress == nil OR macaddress ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByMacaddress:(CloudStackNIC *)otherObject
{
    return [[self macaddress] compare:[otherObject macaddress]];
}


@end
