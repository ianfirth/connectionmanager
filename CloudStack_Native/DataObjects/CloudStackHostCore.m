//
//  CloudStackHostCore.m
//  hypOps
//
//  Created by Ian Firth on 05/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackHostCore.h"

@implementation CloudStackHostCore

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_HOST_CORE   Dictionary:properties];
    if (self){
        // add a reference to itself for all properties in extending object
#warning need to add this when we actually have a Host detail object as well as the core one
        //[self addReference:[self uniqueReference] forObjectType:HYPOBJ_CS_HOST];
    }
    return self;
}

-(NSString*)hypervisor{
    return [allProperties valueForKey:@"hypervisor"];
}

-(NSString*)state{
    return [allProperties valueForKey:@"state"];
}

-(NSString*)version{
    return [allProperties valueForKey:@"version"];
}

-(NSString*)ipaddress{
    return [allProperties valueForKey:@"ipaddress"];
}

-(NSString*)type{
    return [allProperties valueForKey:@"type"];
}

-(NSString*)cpunumber{
    return [allProperties valueForKey:@"cpunumber"];
}

-(NSString*)cpuspeed{
    return [allProperties valueForKey:@"cpuspeed"];
}

-(NSArray*)capabilities{
    NSString* capabilities = [allProperties valueForKey:@"capabilities"];
    return [capabilities componentsSeparatedByString:@","];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

+ (NSPredicate *) hostWithReference:(NSString *)hostId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",hostId]];
}

+ (NSPredicate *) hostWithType:(NSString *)type{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"type like [c]'%@'",type]];
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(CloudStackHostCore *)otherObject
{
    return [[self name] compare:[otherObject name]];
}

@end
