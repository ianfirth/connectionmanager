//
//  CloudStackNetwork.h
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackNetwork : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* account;
@property (readonly) NSString* displaytext;
@property (readonly) NSString* domain;
@property (readonly) NSString* type;
@property (readonly) NSString* state;
@property (readonly) NSString* traffictype;
@property (readonly) NSString* project;
@property (readonly) NSString* networkofferingname;
@property (readonly) NSString* networkdomain;
@property (readonly) NSString* physicalnetworkid;
@property (readonly) NSString* dns1;
@property (readonly) NSString* dns2;
@property (readonly) NSString* netmask;
@property (readonly) NSString* vlan;
@property (readonly) NSString* gateway;
@property (readonly) NSString* broadcaseuri;
@property (readonly) NSString* broadcasedomaintype;
@property (readonly) NSString* acltype;
@property (readonly) NSString* subdomainaccess;
@property (readonly) NSString* restartrequired;

+ (NSPredicate *) networkWithReference:(NSString *)networkId;

+ (NSPredicate *) networks_OwnedByMe:(NSString *)myAccountName;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByName:(CloudStackNetwork *)otherObject;
@end
