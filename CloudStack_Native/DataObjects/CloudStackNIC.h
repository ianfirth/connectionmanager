//
//  CloudStackNIC.h
//  hypOps
//
//  Created by Ian Firth on 11/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackNIC : CloudStackBase
- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

@property (readonly) NSString* macaddress;
@property (readonly) NSString* ipaddress;
@property (readonly) NSString* netmask;
@property (readonly) NSString* traffictype;
@property (readonly) NSString* type;
@property (readonly) NSString* gateway;
@property (readonly) NSString* broadcastuir;
@property (readonly) NSString* isolationuri;

+ (NSPredicate *) nicWithReference:(NSString *)nicId;

+ (NSPredicate *) macaddressBeginsOrContainsAWordBeginningWith:(NSString *)macaddress;

+ (NSPredicate *) macaddressBeginWith:(NSString *)macaddress;

+ (NSPredicate *) macaddressNullOrEmpty;

- (NSComparisonResult)compareByMacaddress:(CloudStackNIC *)otherObject;

@end
