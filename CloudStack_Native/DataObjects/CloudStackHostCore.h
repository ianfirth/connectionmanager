//
//  CloudStackHostCore.h
//  hypOps
//
//  Created by Ian Firth on 05/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackBase.h"

@interface CloudStackHostCore : CloudStackBase

@property (readonly) NSString* hypervisor;
@property (readonly) NSString* state;
@property (readonly) NSString* version;
@property (readonly) NSString* ipaddress;
@property (readonly) NSString* type;
@property (readonly) NSString* cpunumber;
@property (readonly) NSString* cpuspeed;
@property (readonly) NSArray* capabilities;

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

+ (NSPredicate *) hostWithReference:(NSString *)hostId;

+ (NSPredicate *) hostWithType:(NSString *)type;

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;

+ (NSPredicate *) nameBeginWith:(NSString *)name;

+ (NSPredicate *) nameNullOrEmpty;

- (NSComparisonResult)compareByName:(CloudStackHostCore *)otherObject;

@end