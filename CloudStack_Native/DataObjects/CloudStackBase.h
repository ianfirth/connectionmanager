//
//  CloudStackBase.h
//  hypOps
//
//  Created by Ian Firth on 30/05/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

// definition of the types of objects that can be stored in the cache
// these are flags so that they can be used in combination
#define HYPOBJ_CS_VM_CORE 1          // this is the absolute basic VM data
#define HYPOBJ_CS_VM 2               // this is the full VM details 
#define HYPOBJ_CS_ZONE 4
#define HYPOBJ_CS_HOST_CORE 8        // this is the absolute basic HOST data
#define HYPOBJ_CS_HOST 16
#define HYPOBJ_CS_TEMPLATE 32
#define HYPOBJ_CS_ACCOUNT 64         // these are not in the cache (only current one in connection)
#define HYPOBJ_CS_USER 128           // these are not in the cache accessed from account
#define HYPOBJ_CS_SECURITYGROUP 256
#define HYPOBJ_CS_NETWORK 512
#define HYPOBJ_CS_NIC 1024           // these are not in cache, accessed via VM (full version)
#define HYPOBJ_CS_VOLUME 2048
#define HYPOBJ_CS_OSTYPE 4096

#import "CloudStackNativeHypervisorConnection.h"
#import "HypObjReferencesProtocol.h"

@class CloudStackNativeHypervisorConnection;

/**
 Class to represent a base CloudStack object.
 
 Objects that are representing Hypervisor objects should not hold onto references to other objects, they
 should store the unique identifier for the objects and access them back via the connection which has a cache of
 all these objects.  This allows for memory to be freed if required and the object to be refetched on demand.
 */
@interface CloudStackBase : NSObject<HypObjReferencesProtocol> 
{
    NSString* _name;
    NSMutableDictionary* allProperties;
}

/**
 Create a new instance of the class with the specified properties dictionary.
 @param vmProperties The dictionary of properties retreived via the XenServer API for the object.
 @returns a newly initialized NSPredicate
 */
- (id) initWithConnectionId: (NSString*)ConnectionId objectType:(int)hypObjectType Dictionary:(NSDictionary *)properties;

/**
 Store a referenece to another CloudStack object reference
 @param reference The reference for the object being referenced.
 @param objectType The type of hypervisor object the reference is for.
 */
- (void) addReference:(NSString *)reference forObjectType:(int)objectType;

/**
 Store refereneces to other CloudStack object refs
 @param references The array of references for the object being referenced.
 @param objectType The type of hypervisor object the references are for.
 */
- (void) addReferences:(NSArray *)references forObjectType:(int)objectType;

/**
 Retreive a referenec to another Xen object opaque_refs
 @param objectType The type of hypervisor object reference to be retreived.
 */
- (NSArray *) referencesForType:(int)objectType;

/**
 * provides a sort descriptor for each object type
 */
+ (NSSortDescriptor*) defaultSortForhypObjectType:(int)hypObjectType;

/**
 * provides a helper for converting the object type to a string
 * This is primarily for debugging aid and not for display to user.
 */
+ (NSString*) ObjectNameForType:(int)hypObjectType;

/**
 returns the hpervisor connection for the object.
 @returns CloudStackNativeHypervisorConnection
 */
-(CloudStackNativeHypervisorConnection *) hypervisorConnection;


/**
 The Dictionary of other objects that this object references and requires to be considered complete.
 This is keyed on the hypervisor object type and contains an NSMutable array of objects.  This should not store
 all references, otherwise when one object is loaded the tree will be very large and end up pulling in
 all objects into the graph.
 */
@property (strong) NSMutableDictionary *references; 

/**
 The unique reference of the object
 */
@property NSString* uniqueReference; 

/**
 The type of the object
 */
@property (readonly) int objectType; 

/**
 The identifier for the hypervisor Connection that this object was creted from.
 */
@property  NSString *connectionID;

@property (readonly) NSString* name;

@property (readonly) NSDictionary* properties;

@end

