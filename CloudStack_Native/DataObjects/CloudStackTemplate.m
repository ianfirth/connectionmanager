//
//  CloudStackTemplate.m
//  hypOps
//
//  Created by Ian Firth on 05/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackTemplate.h"

@implementation CloudStackTemplate

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_TEMPLATE   Dictionary:properties];
    if (self){
    }
    return self;
}

-(NSString*) displaytext{
    return [allProperties valueForKey:@"displaytext"];
}

-(NSString*) hypervisor{
    return [allProperties valueForKey:@"hypervisor"];    
}

-(NSString*) account{
    return [allProperties valueForKey:@"account"];    
}

-(NSString*) domain{
    return [allProperties valueForKey:@"domain"];    
}

-(NSString*) ostypename{
    return [allProperties valueForKey:@"ostypename"];    
}

-(NSString*) templatetype{
    return [allProperties valueForKey:@"templatetype"];    
}

+ (NSPredicate *) templateWithReference:(NSString *)templateId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"uniqueReference like [c]'%@'",templateId]];
}

+ (NSPredicate *) templates_OwnedByMe:(NSString *)myAccountName{
    return [NSPredicate predicateWithFormat:@"account like '%@'",myAccountName];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(CloudStackTemplate *)otherObject
{
    return [[self name] compare:[otherObject name]];
}

@end