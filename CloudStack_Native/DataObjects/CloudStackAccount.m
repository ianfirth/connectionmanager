//
//  CloudStackAccount.m
//  hypOps
//
//  Created by Ian Firth on 10/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//

#import "CloudStackAccount.h"
#import "CloudStackUser.h"

@implementation CloudStackAccount

@synthesize users;

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties{
    self = [super initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_ACCOUNT   Dictionary:properties];
    if (self){
        //_displayText = [properties valueForKey:@"displayText"];
        // this has a list of users associated with the account too.
        // perhaps I should split this into a separate object too
        // build the set of users and store within this class.  Dont use them as an entity cached
        // in their own right for now.  These are only used as part of the single account that is used to determine
        // who is logged in at present.  Later on if all the accounts are stored as normal then put them into the connection
        // cache then they will work as do all the other objects.
        NSMutableArray* allUsers = [[NSMutableArray alloc] init];
        NSArray* userList = [properties valueForKey:@"user"];
        for(NSDictionary* userProperties in userList){
            CloudStackUser* user = [[CloudStackUser alloc] initWithConnectionId:ConnectionId objectType:HYPOBJ_CS_USER Dictionary:userProperties];
            [allUsers addObject:user];
        }
        [self setUsers:allUsers];
    }
    return self;
}

@end
