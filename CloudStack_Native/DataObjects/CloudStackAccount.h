//
//  CloudStackAccount.h
//  hypOps
//
//  Created by Ian Firth on 10/06/2012.
//  Copyright (c) 2012 Ian Firth. All rights reserved.
//
// This class represents the cloud stack account object
// This is the result of a listAccounts command to cloudStack API
// http://download.cloud.com/releases/3.0.0/api_3.0.0/root_admin/listAccounts.html

#import "CloudStackBase.h"

// This is the definition of CloudStack account types
enum CSAccountType {
	ACCOUNTTYPE_USER = 0,
	ACCOUNTTYPE_ADMIN = 1,
    ACCOUNTTYPE_DOMAIN_ADMIN = 2,
};

@interface CloudStackAccount : CloudStackBase

- (id) initWithConnectionId: (NSString*)ConnectionId Dictionary:(NSDictionary *)properties;

/**
 * The collection of users that the account is associated with
 */
@property NSArray* users;
@end
