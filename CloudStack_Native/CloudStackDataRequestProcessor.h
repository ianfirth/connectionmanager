//
//  CloudStackDataRequestProcessor.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackBaseObjectBuilder.h"
@protocol CloudStackDataRequestProcessorDelegate <NSObject>

// this is called when the processor produces a set of objects from the request
-(void) processorObjectsGenerated:(NSArray*)theObjects ofType:(int)hypObjType andSetIsComplete:(BOOL)completeSet;

// This is called when the processor fails to get a good result from the request
-(void) processorObjectsGeneratedError:(NSError*)theError forType:(int)hypObjType andSetIsComplete:(BOOL)completeSet;

@end

@interface CloudStackDataRequestProcessor : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>{
    
    // the object type that the request will return
    int theHypObjectType;
    
    // the request to make
    NSURLRequest* theRequest;
    
    // the data received
    NSMutableData* theData;
    
    // object to convert the received data into cloud objects
    CloudStackBaseObjectBuilder* theBuilder;
    
    // this is the delegate to use to inform anyone that the process is complete
    id<CloudStackDataRequestProcessorDelegate> theDelegate;

    // indicates if the results set recieved is the complete set of the object type or a subset
    BOOL isCompleteResultSet;
}

-(id) initWithRequest:(NSURLRequest*)request 
               forCmd:(NSString*)cmd 
   producesHypObjType:(int)hypObjType 
     andObjectBuilder:(CloudStackBaseObjectBuilder*)builder 
 forConpleteResultSet:(BOOL)completeResultSet
          andDelegate:(id<CloudStackDataRequestProcessorDelegate>)theProcessorDelegate;

// send the request to the server
-(void)makeRequest;

@end
