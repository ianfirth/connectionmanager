//
//  CloudStackNativeHypervisorConnection.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypervisorConnectionFactory.h"
#import "CloudStackDataRequestProcessor.h"

// defines for the messages that can be sent to the SDK interface
#define CS_CMD_VMCORE_GET @"listVirtualMachines"
#define CS_CMD_VM_GET @"listVirtualMachines"
#define CS_CMD_HOSTCORE_GET @"listHosts"
#define CS_CMD_HOST_GET @"listHosts"
#define CS_CMD_ZONE_GET @"listZones"
#define CS_CMD_TEMPLATE_GET @"listTemplates"
#define CS_CMD_ACCOUNT_GET @"listAccounts"
#define CS_CMD_SECURITYGROUP_GET @"listSecurityGroups"
#define CS_CMD_NETWORK_GET @"listNetworks"
#define CS_CMD_VOLUME_GET @"listVolumes"
#define CS_CMD_OSTYPE_GET @"listOsTypes"
/**
 Class to represent a connection to a CloudStack Hypervisor.
 The XenServer connection is a caching connection
 
 The consumer of the connection can request data from the hypervisor, these are async requests.
 The connection is intended to support any version of CloudStack (what versions?) 
 properties left empty (nil) if the hypervisor being connected to does not support them.  It is up to the caller to determine the
 values and ensure that the code does not requre them.  
 The objects documentation should reflect the version of the CloudStack 
 required for the data to be populated, however if there is no comment it should be assumed that the data will be available in 
 all versions from vx onwards.
*/

@class CloudStackAccount;
@class CloudStackBase;
@class CloudStackVMCore;
@class CloudStackZone;

@interface CloudStackNativeHypervisorConnection : HypervisorConnectionFactory<CloudStackDataRequestProcessorDelegate>{
    
    /**
     The connection address for the current connection.
     */
    NSURL *connectionAddress;
    
    /**
     The connection username for the current connection.
     */
    NSString *connectionUsername;
    
    ConnectionState connectionState;
    
    /**
     indicates that the connection is suspending
     */
    BOOL suspending;
    
    /**
     indicates that the connection is suspended
     */
    BOOL suspended;
    
    /**
     indicates that the connection is resuming
     */
    BOOL resuming;
}

#pragma mark -
#pragma mark Connection Methods
- (void)reloadCoreData;
/*
 * Provides Access to the connection Address for the Cloud
 */
- (NSURL*) connectionAddress;

/*
 * Defines if the configuraton indicates that autoUpdate is enabled
 */
+ (BOOL) isAutoUpdateConfigured;

- (void) setConnectionAddress:(NSURL*) theConnectionAddress;

/**
 Request all data objects for a specific type from the cloud connection.  This is an async request.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObjectsForType:(int)hypObjectType;

/**
 Request specific data object for a specific type from the XenHypervisor connection.  This is an async request.
 @param reference The opaque reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType;

/**
 Close the connection.
 This will close the connection, by sending a request to stop listenting to events and sending a connection close to the hypervisor.
 The responses to these requests (and any other outstanding requests) will not be processed.  It is a assumed that the connection close
 will sucseed or if not time out at a later point.  There is no check that the close was compelted sucsesfully.
 */
- (void)closeConnection;

#pragma mark -
#pragma mark Cache reading

@property BOOL closing;

/**
 The connection password for the current connection.
 */
@property NSString* connectionPassword;

/**
 The connection address for the current connection.
 */
@property NSURL* connectionAddress;

/**
 The ZoneId for the current connection.
 */
@property NSString* zoneId;

/**
 The Account Object for the current connection.
 */
@property CloudStackAccount* cloudStackAccount;

@end

