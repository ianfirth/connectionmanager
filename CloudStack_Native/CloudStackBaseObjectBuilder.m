//
//  CloudStackBaseObjectBuilder.m
//  hypOps

// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackBaseObjectBuilder.h"

@implementation CloudStackBaseObjectBuilder

-(id) initWithClass:(Class) class 
       andObjectKey:(NSString*) objectKey
      forConnection:(NSString*) connectonId{
    self = [super init];
    if (self){
        theClass = class;
        theObjectKey = objectKey;
        theConnectionId = connectonId;
    }
    return self;
}
-(NSArray*) buildObjectsFromJSON:(NSDictionary*)json{
    // take the JSON data and build an array of cloudObjects
    //each object created will be of the type class passed in.
    // go though the dictionary and when find the key specified take the
    // value and pass it to the constructor for the object class provided
    // take value for first element as this will be the listresposne 
    NSMutableArray* finalResult = [[NSMutableArray alloc] init ];
    NSDictionary* actualResult = [[json allValues] objectAtIndex:0];
    
    for(NSString* key in [actualResult allKeys]){
        if ([key isEqualToString:theObjectKey]){
            NSArray* allObjects = [actualResult valueForKey:key];
            // each element in this array is a dictionary that is passed to the 
            // constructor for the object types passed in.
            for(NSDictionary* params in allObjects){
                NSObject* objectClass = [theClass alloc];
                if ([objectClass respondsToSelector:@selector(initWithConnectionId:Dictionary:)]){
                    
                    NSObject* theResult = [objectClass performSelector:@selector(initWithConnectionId:Dictionary:) withObject:theConnectionId withObject:params];
                    [finalResult addObject:theResult];
                     
                }
            }
        }
    }
    return finalResult;
}
@end
