//
//  CloudStackNativeHypervisorConnection.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackNativeHypervisorConnection.h"
#import "HypervisorConnectionFactory.h"
#import "CloudStackBase.h"
#import "NSData+Base64.h"
#import "CloudStackVMCore.h"
#import "CloudStackHostCore.h"
#import "CloudStackVMDetail.h"
#import "CloudStackZone.h"
#import "CloudStackAccount.h"
#import "CloudStackNetwork.h"
#import "CloudStackTemplate.h"
#import "CloudStackSecurityGroup.h"
#import "CloudStackVolume.h"
#import "CloudStackOsType.h"
#import <CommonCrypto/CommonHMAC.h>


// declare the private methods
@interface CloudStackNativeHypervisorConnection ()
-(NSString*) signRequestCommand:(NSString*)command withParameters:(NSDictionary*)parameters;
- (NSString *)URLEncodeString:(NSString *)b64Sig;
- (void)fireConnectedDelegatesWithResult:(BOOL)sucsess;
- (void)fireUpdatedDelegatesWithResult:(NSArray*)objects ofType:(int)hypObjType isCompleteSet:(BOOL)isCompleteSet wasSucsess:(BOOL)success;
- (void)sendRequestForType:(int)hypObjType 
                WithMethod:(NSString *)method 
              andParamters:(NSDictionary *)parameters 
             objectBuilder:(CloudStackBaseObjectBuilder*)objectBuilder 
             isCompleteSet:(BOOL)completeSet
                  runAsync:(BOOL)runAsync;
@end

@implementation CloudStackNativeHypervisorConnection

@synthesize closing;
@synthesize connectionPassword;
@synthesize connectionAddress;
@synthesize zoneId;
@synthesize cloudStackAccount;

/**
 * Method to get the objects that are considered CORE to load
 * 'CORE' means the objects that are needed for the entire app to work rather
 * than the other objects that just contain detailed data for specific items.
 * The VM objects have been split into 2, the core version contains the data that
 * is retrieved from the get all VM objects with the details = 'min'.
 */
- (void)reloadCoreData{
    int requestObjects = 0;
    if ([self isUpdateAvailableForType:HYPOBJ_CS_VM_CORE]){
        requestObjects |= HYPOBJ_CS_VM_CORE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_CS_HOST_CORE]){
        requestObjects |= HYPOBJ_CS_HOST_CORE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_CS_TEMPLATE]){
        requestObjects |= HYPOBJ_CS_TEMPLATE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_CS_NETWORK]){
        requestObjects |= HYPOBJ_CS_NETWORK;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_CS_SECURITYGROUP]){
        requestObjects |= HYPOBJ_CS_SECURITYGROUP;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_CS_VOLUME]){
        requestObjects |= HYPOBJ_CS_VOLUME;
    }
    
    if (requestObjects != 0){
        [self RequestHypObjectsForType:requestObjects]; 
    }
}

/**
 * Constructorfor the connection
 */
-(id) init{
    self = [super init];
    if (self) {
        hypervisorConnectionType = HYPERVISOR_CLOUDSTACK_NATIVE;
        connectionState = CONNECTION_UNCONNECTED;
        [self setClosing:NO]; 
    }
    return self;
}

/**
 * undertakes whatever is required to close the connection.
 */
- (void)closeConnection{
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_CLOUDSTACK_NATIVE];
    NSMutableDictionary *cloudstackNativeConnections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    if (cloudstackNativeConnections != nil){
        NSString *key = nil;
        for (NSString* connectionKey in [cloudstackNativeConnections allKeys]){
            CloudStackNativeHypervisorConnection *connection = [cloudstackNativeConnections objectForKey:connectionKey];
            if (connection == self){
                key = connectionKey;
            }
        }
        if (key){
            [self setClosing:YES];
            NSLog(@"Closing connection %@", key);
            // send an unregister and logout
            // if suspending dont remove the connection
            if (!suspending){
                [cloudstackNativeConnections removeObjectForKey:key];
            }
        }
    }
    
    hypervisorId = nil;
    if (suspending){
        suspending = NO;
        suspended = YES;
    }
    else
    {
        connectionUsername = nil;
        [self setConnectionPassword:nil];
    }
}

/**
 * Connect to the hypervisor using the supplied details.
 * connect in this connection type results in requesting the available zones.
 * Not expecting there to be many Zones in the model and they dont have
 * much data contained within them.
 * return Yes if connected or No if failed
 */
- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString*)password
{
    [super ConnectToAddress:address withUsername:username andPassword:password];
    NSLog(@"ConnectToAddress %@ username is %@",address,username);
    if ([self GetConnectionState] != CONNECTION_CONNECTED && [self GetConnectionState] != CONNECTION_CONNECTING){
        closing = NO;
        connectionState = CONNECTION_CONNECTING;

        NSString *finalAddress;
        if (![address hasPrefix:@"http://"] && ![address hasPrefix:@"https://"]){
            finalAddress = [@"http://" stringByAppendingString:address];
        }
        else {
            finalAddress = address;
        }
        if (![finalAddress hasSuffix:@"/client/api"]){
            finalAddress = [finalAddress stringByAppendingString:@"/client/api"];
        }

        [self setConnectionAddress: [NSURL URLWithString:finalAddress]];
        [self setConnectionPassword:password];
        
        // as part of the login process load the account and the list of available zones.
        
        CloudStackBaseObjectBuilder* zoneObjectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackZone class] andObjectKey:@"zone" forConnection:[self connectionID]];

        [self sendRequestForType:HYPOBJ_CS_ZONE  WithMethod:CS_CMD_ZONE_GET 
                       andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                        @"true",@"listAll",
                                        @"true",@"showcapacities",nil]
                        objectBuilder:zoneObjectBuilder
                       isCompleteSet:YES
                           runAsync:YES];

        CloudStackBaseObjectBuilder* accountObjectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackAccount class] andObjectKey:@"account" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_ACCOUNT  WithMethod:CS_CMD_ACCOUNT_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  @"false",@"listAll",nil] 
                   objectBuilder:accountObjectBuilder
                       isCompleteSet:YES
                        runAsync:YES];

        connectionUsername = username;
    }
    else {
        // dont connect again just imply the connect
        NSLog(@"Already connected");  
    }
}

// close the connection mark all objects as outofdate
- (void)suspendConnection{
    if (autoUpdateEnabled){
        suspending = YES;
        [self closeConnection];
        [self flushHypObjectCache];
    }
}

// clear the cache, reopen the connection mark all objects as outofdate
- (void)resumeConnection{
    if (autoUpdateEnabled){
        resuming = YES;
        [self ConnectToAddress:[[self connectionAddress] description] withUsername:connectionUsername andPassword:[self connectionPassword]];
    }
}

// return the state of the current connection
- (ConnectionState) GetConnectionState{
    return connectionState;
}

// clean up
- (void)dealloc{
    NSLog(@"dealloc CloudStack Native Hypervisor Connection");
    
    if (connectionAddress){
        connectionAddress = nil;
    }
}

/*
 * Send a request to the connected service with the specified parameters
 * Note URL was configured during initialization, when connection was established
 */
- (void)sendRequestForType:(int)hypObjType 
                WithMethod:(NSString *)method 
              andParamters:(NSDictionary *)parameters 
             objectBuilder:(CloudStackBaseObjectBuilder*)objectBuilder 
             isCompleteSet:(BOOL)completeSet
                  runAsync:(BOOL)runAsync{
    
//    NSMutableURLRequest *theRequest=[NSURLRequest requestWithURL:[self connectionAddress]
//                                                  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
//                                                  timeoutInterval:5];
    
    // making sure that the request is a JSON style as this is the expected format for the parser of
    // the response.
    NSMutableDictionary* fullParamSet = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [fullParamSet setValue:@"json" forKey:@"response"];
    
    // call a method to sign it
    NSString* hash = [self signRequestCommand:method withParameters:fullParamSet];
    
    // build command part
    NSMutableString* parameterString = [NSMutableString stringWithFormat:@"command=%@",method];
    // add the parmater section
    for (NSString* key in [fullParamSet allKeys]){
        NSString* value = [fullParamSet valueForKey:key];
        [parameterString appendFormat:@"&%@=%@",key,value];
    }
    
    [parameterString appendFormat:@"&%@=%@",@"apikey",[self connectionUserName]];    
    [parameterString appendFormat:@"&%@=%@",@"signature",hash];    

    
    NSString* urlString = [NSString stringWithFormat:@"%@?%@",[[self connectionAddress] description],parameterString];
    // add path components to the URL for the get here
    NSURL* urlwithQUery = [NSURL URLWithString:urlString];
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:urlwithQUery];
    [theRequest setHTTPMethod:@"GET"];
    
    CloudStackDataRequestProcessor* processor = [[CloudStackDataRequestProcessor alloc] initWithRequest:theRequest 
                                                                                                 forCmd:method 
                                                                                     producesHypObjType:hypObjType 
                                                                                       andObjectBuilder:objectBuilder 
                                                                                            forConpleteResultSet:completeSet
                                                                                            andDelegate:self];
    
    [processor makeRequest];
}

#pragma mark -
#pragma mark CloudStackDataProcessorDelegate
-(void) processorObjectsGenerated:(NSArray*)theObjects ofType:(int)hypObjType andSetIsComplete:(BOOL)completeSet{

    // special processing for account.  Currently the only way to get an account objcet is at login
    // there is no request for them as they are not needed for any other purpose. They are therefore
    // not stored in the cache, but instead the single one that represents this login is accsessed via the
    // connection.  If in future all accounts are needed, then this needs to store in cache and filter out the
    // account that represents the current account and keep this stored in the connection.
    
    if (hypObjType == HYPOBJ_CS_ACCOUNT){
        [self setCloudStackAccount:[theObjects objectAtIndex:0]];
    }
    else{
        if (completeSet){
            // clear any pending updates for the type specified
            [self clearObjectTypeRequestPending:hypObjType];
            
            if ([self GetConnectionState] == CONNECTION_CONNECTING){
                connectionState = CONNECTION_CONNECTED;
                [self fireConnectedDelegatesWithResult:YES];
            }
            // cache the results from the request and fire any events.
            [self fireUpdatedDelegatesWithResult:theObjects ofType:hypObjType isCompleteSet:YES wasSucsess:YES];
        }
        else{
            // fire a single object version here
            [self fireUpdatedDelegatesWithResult:theObjects ofType:hypObjType isCompleteSet:NO wasSucsess:YES];  
        }
        if (walkTreeMode){
            for (CloudStackBase* object in theObjects){
                // go though the object graph for this object and check that all required data is loaded
                [self PopulateHypObject:object];
            }
        }
    }
}

-(void) processorObjectsGeneratedError:(NSError*)theError forType:(int)hypObjType andSetIsComplete:(BOOL)completeSet{
// if connecting fire a connection failed message here
    if ([self GetConnectionState] == CONNECTION_CONNECTING){
        connectionState = CONNECTION_UNCONNECTED;
        [self fireConnectedDelegatesWithResult:NO];
    }
    if (completeSet){
        // clear any pending updates for the type specified
        [self clearObjectTypeRequestPending:hypObjType];
        
        if ([self GetConnectionState] == CONNECTION_CONNECTING){
            connectionState = CONNECTION_CONNECTED;
            [self fireConnectedDelegatesWithResult:YES];
        }
        // cache the results from the request and fire any events.
        [self fireUpdatedDelegatesWithResult:nil ofType:hypObjType isCompleteSet:YES wasSucsess:NO];
    }
    else{
        // fire a single onbject version here
        [self fireUpdatedDelegatesWithResult:nil ofType:hypObjType isCompleteSet:NO wasSucsess:NO];  
    }
}

#pragma mark -
#pragma mark CloudStackNativeHypervisorConnection

-(void) LogInformationForType:(int)hypObjectType andReference:(NSString*)reference withMessage:(NSString*) message{
#ifdef logCommsData
    NSString *objectType = [XenBase ObjectNameForType:hypObjectType];
    
    if (reference){
        NSLog(@"%@ <<type %@ and reference %@>>",message,objectType, reference);
    }
    else{
        NSLog(@"%@ <<type %@>>",message,objectType);
    }
#endif
}

-(void) LogInformationForType:(int)hypObjectType withMessage:(NSString*)message{
    [self LogInformationForType:hypObjectType andReference:nil withMessage:message];
}

/*
 * Request data for the required hypervisor object types from the XenServer
 * Can request multiple types in one go by using the flags as required
 */
- (void)RequestHypObjectsForType:(int)hypObjectType{
    [self LogInformationForType:hypObjectType withMessage:@"Request all"];

    if (hypObjectType & HYPOBJ_CS_OSTYPE) {  
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackOsType class] andObjectKey:@"ostype" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_OSTYPE];
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:nil];
        
        [self sendRequestForType:HYPOBJ_CS_OSTYPE 
                      WithMethod:CS_CMD_OSTYPE_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                   isCompleteSet:YES
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_ZONE) {  
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackZone class] andObjectKey:@"zone" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_ZONE];
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"true",@"listAll",
                                       @"true",@"showcapacities",nil];
        if (zoneId){
            [params setValue:zoneId forKey:@"zoneid"];
        }
        
        [self sendRequestForType:HYPOBJ_CS_ZONE 
                      WithMethod:CS_CMD_ZONE_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_VM_CORE) {  
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackVMCore class] andObjectKey:@"virtualmachine" forConnection:[self connectionID]];

        [self setObjectTypeRequestPending:HYPOBJ_CS_VM_CORE];
        [self sendRequestForType:HYPOBJ_CS_VM_CORE WithMethod:CS_CMD_VMCORE_GET andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                            @"true",@"listAll",
                            @"stats,volume,servoff,stats",@"details",nil] 
                        objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_VM) {  
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackVMDetail class] andObjectKey:@"virtualmachine" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_VM];
        [self sendRequestForType:HYPOBJ_CS_VM 
                      WithMethod:CS_CMD_VM_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  @"true",@"listAll",
                                  @"all",@"details",nil] 
                   objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_HOST_CORE) {  
        // request the core Host details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackHostCore class] andObjectKey:@"host" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_HOST_CORE];

        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"true",@"listAll",
                                       @"min",@"details",nil];
        if (zoneId){
            [params setValue:zoneId forKey:@"zoneid"];
        }

        [self sendRequestForType:HYPOBJ_CS_HOST_CORE 
                      WithMethod:CS_CMD_HOSTCORE_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }

    
    if (hypObjectType & HYPOBJ_CS_TEMPLATE) {  
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackTemplate class] andObjectKey:@"template" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_TEMPLATE];

        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"true",@"listAll",
                                       @"executable",@"templatefilter",nil];
        if (zoneId){
            [params setValue:zoneId forKey:@"zoneid"];
        }

        [self sendRequestForType:HYPOBJ_CS_TEMPLATE 
                      WithMethod:CS_CMD_TEMPLATE_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }


    if (hypObjectType & HYPOBJ_CS_NETWORK) {  
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackNetwork class] andObjectKey:@"network" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_NETWORK];
        
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"true",@"listAll",nil];
        if (zoneId){
            [params setValue:zoneId forKey:@"zoneid"];
        }
        
        [self sendRequestForType:HYPOBJ_CS_NETWORK 
                      WithMethod:CS_CMD_NETWORK_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_SECURITYGROUP) {  
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackSecurityGroup class] andObjectKey:@"securitygroup" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_SECURITYGROUP];
        
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"true",@"listAll",nil];
       
        [self sendRequestForType:HYPOBJ_CS_SECURITYGROUP 
                      WithMethod:CS_CMD_SECURITYGROUP_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                       isCompleteSet:YES
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_VOLUME) {  
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackVolume class] andObjectKey:@"volume" forConnection:[self connectionID]];
        
        [self setObjectTypeRequestPending:HYPOBJ_CS_VOLUME];
        
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"true",@"listAll",nil];
        
        [self sendRequestForType:HYPOBJ_CS_VOLUME 
                      WithMethod:CS_CMD_VOLUME_GET 
                    andParamters:params 
                   objectBuilder:objectBuilder
                   isCompleteSet:YES
                        runAsync:YES];    
    }
}

/*
 * Request a single hypervisor object
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType{
    if (!reference || [reference isEqualToString: @""]){
        // should not really ever get here, but if it does dont process the request
        NSLog(@"Dont queue an object with a opaque reference that is nil or empty");
        return;
    }

    
    if (hypObjectType & HYPOBJ_CS_OSTYPE) {  
        // request the core VM details
        [self setObjectTypeRequestPending:HYPOBJ_CS_OSTYPE];
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackOsType class] andObjectKey:@"ostype" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_OSTYPE 
                      WithMethod:CS_CMD_OSTYPE_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",nil] 
                   objectBuilder:objectBuilder
                   isCompleteSet:NO
                        runAsync:YES];    
    }

    
    if (hypObjectType & HYPOBJ_CS_VM) {  
        // request the core VM details
        [self setObjectTypeRequestPending:HYPOBJ_CS_VM];
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackVMDetail class] andObjectKey:@"virtualmachine" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_VM 
                      WithMethod:CS_CMD_VM_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",nil] 
                   objectBuilder:objectBuilder
                       isCompleteSet:NO
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_VM_CORE) {  
        [self setObjectTypeRequestPending:HYPOBJ_CS_VM_CORE];
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackVMCore class] andObjectKey:@"virtualmachine" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_VM_CORE 
                      WithMethod:CS_CMD_VMCORE_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",nil] 
                   objectBuilder:objectBuilder
                   isCompleteSet:NO
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_SECURITYGROUP) {  
        [self setObjectTypeRequestPending:HYPOBJ_CS_SECURITYGROUP];
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackSecurityGroup class] andObjectKey:@"securitygroup" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_SECURITYGROUP 
                      WithMethod:CS_CMD_SECURITYGROUP_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",nil] 
                   objectBuilder:objectBuilder
                   isCompleteSet:NO
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_VOLUME) {  
        [self setObjectTypeRequestPending:HYPOBJ_CS_VOLUME];
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackVolume class] andObjectKey:@"volume" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_VOLUME 
                      WithMethod:CS_CMD_VOLUME_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",nil] 
                   objectBuilder:objectBuilder
                   isCompleteSet:NO
                        runAsync:YES];    
    }
    
    if (hypObjectType & HYPOBJ_CS_ZONE) {  
        [self setObjectTypeRequestPending:HYPOBJ_CS_ZONE];
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackZone class] andObjectKey:@"zone" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_ZONE 
                      WithMethod:CS_CMD_ZONE_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",
                                  @"true",@"showcapacities",nil] 
                   objectBuilder:objectBuilder
                   isCompleteSet:NO
                        runAsync:YES];    
    }

    if (hypObjectType & HYPOBJ_CS_HOST_CORE) {  
        [self setObjectTypeRequestPending:HYPOBJ_CS_HOST_CORE];
        // request the core VM details
        CloudStackBaseObjectBuilder* objectBuilder = [[CloudStackBaseObjectBuilder alloc] initWithClass:[CloudStackHostCore class] andObjectKey:@"host" forConnection:[self connectionID]];
        
        [self sendRequestForType:HYPOBJ_CS_HOST_CORE 
                      WithMethod:CS_CMD_HOSTCORE_GET 
                    andParamters:[NSDictionary dictionaryWithObjectsAndKeys:
                                  reference,@"id",nil] 
                   objectBuilder:objectBuilder
                   isCompleteSet:NO
                        runAsync:YES];    
    }
    
}

// all objects in CloudStack connection have an uniqueIdentifer property that defines their uniqueness
// I have done this by extending the CloudStackBase there is no need for that
// to really be the case could do it this way if required.
-(NSString *) uniqueObjectReferencePropertyForHypObjectType:(int)objectType{
    return @"uniqueReference";
}

//
//  Determine if there are updates available for the specified object type.
//  This assumes that in 
//      AutoUpdate mode that once all objects of a specified type have been requested that
//         the automatic update mechanism will keep the list up to date.
//      Manual update mode that the lists are never up to date and can always be retreived
- (BOOL)isUpdateAvailableForType:(int)hypObjectType{
    // if in auto mode it is yes for each type until a response for all objects of that type has been recieved then after that
    // it is no (unless a lost events message is received).
    if ([self isAutoUpdateEnabled]){
        return (availableUpdates & hypObjectType);
    }
    // if in manual mode this is always yes
    else {
        return YES;
    }
}

#pragma mark - 
#pragma mark private methods

/**
 * Method to fire the connection events
 */
- (void)fireConnectedDelegatesWithResult:(BOOL)sucsess{
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    if (!sucsess){
        [self setLastError:[NSError errorWithDomain:@"CloudStack connection failed - possibly wrong username or password" code:0 userInfo:nil]];
        connectionState = CONNECTION_UNCONNECTED;
    }
    else{
        connectionState = CONNECTION_CONNECTED;
    }
    
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorConnectedToConnection:withResult:)])
        {
            [connectionDelegateToCall hypervisorConnectedToConnection:self withResult:sucsess];
        }
    }
    copyOfDelegates = nil;
}

/**
 * Method to fire the updated events
 */
- (void)fireUpdatedDelegatesWithResult:(NSArray*)objects ofType:(int)hypObjType isCompleteSet:(BOOL)isCompleteSet wasSucsess:(BOOL)success{
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.    
    if (success){
        [self updateCachedHypObjects:[NSArray arrayWithArray:objects] forObjectType:hypObjType isCompleteSet:isCompleteSet];
    }
    else{
        // if get an error (and this can happen if you are a cloudstack user and you dont have permissions to this object type
        // then clear the outstanding request
        [self clearObjectTypeRequestPending:hypObjType];           
    }

    // fire the object set changed
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)])
        {
            [connectionDelegateToCall hypervisorObjectsUpdated:self updatesObjectsOfType:hypObjType withResult:success];
        }
    }

    // fire the individual object changed
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        for (CloudStackBase* object in objects){
            if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)])
            {
                [connectionDelegateToCall hypervisorObjectsUpdated:self updatesObjectsOfType:hypObjType withReference:[object uniqueReference] withResult:success];
            }
        }
    }
    
    copyOfDelegates = nil;
}


+ (BOOL) isAutoUpdateConfigured{
    return NO;
}

- (NSSortDescriptor*) sortForhypObjectType:(int)hypObjectType{
    return [CloudStackBase defaultSortForhypObjectType:hypObjectType];
}

-(NSString*) signRequestCommand:(NSString*)command withParameters:(NSDictionary*)parameters{
    
    //1. For each field-value pair (as separated by a ‘&’) in the Command String, URL encode each value so that it can be safely sent via HTTP GET. NOTE: Make sure all spaces are encoded as “%20” rather than “+”.
    //2. Lower case the entire Command String and sort it alphabetically via the field for each field-value pair. The result of this step would look like the following.
    //3. Take the sorted Command String and run it through the HMAC SHA-1 hashing algorithm (most programming languages offer a utility method to do this) with the user’s Secret Key. Base64 encode the resulting byte array in UTF-8 so that it can be safely transmitted via HTTP. The final string produced after Base64 encoding should be “Lxx1DM40AjcXU%2FcaiK8RAP0O1hU%3D”.
    
    // get the full parameter set by adding the apikey
    NSMutableDictionary* fullParamSet = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [fullParamSet setValue:[self connectionUserName] forKey:@"apikey"];
    [fullParamSet setValue:command forKey:@"command"];    
    
    NSSortDescriptor* descriptor = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
    
    // sort the dictionary keys alphabetcially
    NSArray* allKeys = [fullParamSet allKeys];
    NSArray* allSortedKeys = [allKeys sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    NSMutableString* parameterString = [[NSMutableString alloc] init];
    // add the parmater section
    for (NSString* key in allSortedKeys){
        NSString* value = [fullParamSet valueForKey:key];
        
        NSString* escapedUrlString = [self URLEncodeString:value];
        
        if ([parameterString length] == 0){
            [parameterString appendFormat:@"%@=%@",key,escapedUrlString];            
        }
        else {
            [parameterString appendFormat:@"&%@=%@",key,escapedUrlString];
        }
    }
    
    // now have the parameter string lowercase it
    NSString* lowerData = [parameterString lowercaseString];
    // now sign it
    
    const char *cKey  = [[self connectionPassword] cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [lowerData cStringUsingEncoding:NSASCIIStringEncoding];
    
    // sign the string with HMACSHA-1
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC
                                          length:sizeof(cHMAC)];
    
    // convert the HMAC to a UTF8 Base64 string
    NSString* b64Sig = [HMAC base64EncodedString];
    
    return [self URLEncodeString:b64Sig];
}

- (NSString *)URLEncodeString:(NSString *)b64Sig {
   
    CFStringRef stringRef = CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)b64Sig, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
    
    NSString *encodedString = [NSString stringWithFormat:@"%@", (__bridge_transfer NSString *)stringRef];
    

    return encodedString;
}

@end
