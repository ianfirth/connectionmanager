//
//  CloudStackDataRequestProcessor.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This class is responsible for creating a HTTP/HTTPS request and
// retreiving the result.
// it then uses a plugin to convert the response to a dataObject that this application
// understands, and provides a delegate mechanism that the connection can use to 
// get that data into the cloud
#import "CloudStackDataRequestProcessor.h"

@implementation CloudStackDataRequestProcessor

-(id) initWithRequest:(NSURLRequest*)request 
               forCmd:(NSString*)cmd 
   producesHypObjType:(int)hypObjType 
     andObjectBuilder:(CloudStackBaseObjectBuilder*)builder 
 forConpleteResultSet:(BOOL)completeResultSet
          andDelegate:(id<CloudStackDataRequestProcessorDelegate>)theProcessorDelegate{
    self = [super init];
    if (self){
        theRequest = request;
        theDelegate = theProcessorDelegate;
        theBuilder = builder;
        theHypObjectType = hypObjType;
        isCompleteResultSet = completeResultSet;
    }
    return self;
}

-(void)makeRequest{
    id connection __unused = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
}

#pragma mark - 
#pragma mark NSURLConnectionDataDelegate

//- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response{
//    
//}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
// could check status codes here to indicate error
// assuming that there is only one of these at a time (which I think I can enforce)
// then can set status here and use it in the did receive data
// put the flag into a dictionary keyed of connection so that if more than one at a time it works ok.
// could allow 3 requests in parallel that way to speed it up if need be.
    if ([response respondsToSelector:@selector(statusCode)])
    {
        int statusCode = [((NSHTTPURLResponse *)response) statusCode];
        if (statusCode >= 400)
        {
            [connection cancel];  // stop connecting; no more delegate messages
            NSDictionary *errorInfo
            = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:
                                                  NSLocalizedString(@"Server returned status code %d",@""),
                                                  statusCode]
                                          forKey:NSLocalizedDescriptionKey];
            NSError *statusError
            = [NSError errorWithDomain:@"HTTPError"
                                  code:statusCode
                              userInfo:errorInfo];
            [self connection:connection didFailWithError:statusError];
            
            [theDelegate processorObjectsGeneratedError:statusError forType:theHypObjectType andSetIsComplete:isCompleteResultSet];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"Connection got data");
    
    // append more data to it if nothing there already
    // process in the connection finished method

    if (!theData){
        theData = [NSMutableData dataWithData:data];
    }else {
        [theData appendData:data];
    }
}

//- (NSInputStream *)connection:(NSURLConnection *)connection needNewBodyStream:(NSURLRequest *)request{
//    
//}

//- (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
// totalBytesWritten:(NSInteger)totalBytesWritten
//totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
//    
//}

//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
//    
//}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    // might need to use a flag here to indicate that the load has finished and that the data can be procedded.
    // perhaps the data recieved needs to be appended until complete?
    //use the data and kill t from the dictionary here
    NSLog(@"HTTP Connection finished loading now processing data");
    NSError* error;
    NSDictionary* json = [NSJSONSerialization 
                          JSONObjectWithData:theData
                          
                          options:kNilOptions 
                          error:&error];
       
    NSArray* theResultingObjects = [theBuilder buildObjectsFromJSON:json];
    [theDelegate processorObjectsGenerated:theResultingObjects ofType:theHypObjectType andSetIsComplete:isCompleteResultSet];
}

#pragma mark - 
#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
   // if get an error deal with it somehow
    NSLog(@"Got a connection error -  %@",error);
    [theDelegate processorObjectsGeneratedError:error forType:theHypObjectType andSetIsComplete:isCompleteResultSet];
}

// Deprecated authentication delegates.
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace{
    // this will allow self signed certificates accross https connecton
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    // to make this work like XenServer I should get the fingerpring here and store it if I have not had one before
    // if it is different ask if you want to accept it if not go ahead.  For now leave it like this, although it
    // is not very secure, but then self sighned certs are not anyway :)
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
    NSLog(@"didCancelAuthenticationChallenge");    
}

@end
