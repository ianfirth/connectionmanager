//
//  EC2InstanceWrapper.h
//  hypOps
//
//  Created by Ian Firth on 23/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <AWSiOSSDK/EC2/EC2Instance.h>
#import "HypObjReferencesProtocol.h"

/**
 Enum to represent the different VM Power States available 
 */
typedef enum {
    EC2POWERSTATE_PENDING = 0,    // the VM is halted
    EC2POWERSTATE_RUNNING = 16,    // the VM is paused
    EC2POWERSTATE_SHUTTINGDOWN =32,   // the VM is running
    EC2POWERSTATE_TERMINATED = 48,  // the VM is suspended
    EC2POWERSTATE_STOPPING = 64,
    EC2POWERSTATE_STOPPED = 80
} EC2PowerState;

/**
 Enum to represent the different VM Power States available
 */
typedef enum {
    EC2POWEROPERATION_START,          // VM can be started
    EC2POWEROPERATION_STOP,           // VM can be stopped
    EC2POWEROPERATION_TERMINATE,     // VM can be terminated (stopped and removed)
} EC2PowerOperation;


@interface EC2InstanceWrapper : NSObject<HypObjReferencesProtocol>  {
    EC2Instance *wrappedInstance; 
}

// default constructor
-(id) initWithEC2Instance:(EC2Instance *)instance;

// constructor for failed load placeholder object
-(id) initWithEC2InstanceIdentity:(NSString *)instanceId;

+ (NSPredicate *) instanceIDBeginsOrContainsAWordBeginningWith:(NSString *)name;
+ (NSPredicate *) instanceWithReference:(NSString *)instanceId;
+ (NSPredicate *) instance_PowerStateFor:(EC2PowerState)powerState;
+ (NSPredicate *) instance_Tagged;
+ (NSPredicate *) Instances_WithTagKey:(NSString *)tagKey;
+ (NSPredicate *) Instances_WithTagKey:(NSString *)tagKey andValue:(NSString *)value;

/**
 Returns a set of EC2PowerOperation values indicating the available power transitions
 @returns set of EC2PowerOperation
 */
- (NSSet *) availablePowerOperations;

// these are thr properties that the wrapped object exposes

//Unique ID of the instance launched. 
@property (readonly) NSString *instanceId;
//Image ID of the AMI used to launch the instance. 
@property (readonly) NSString *imageId;
//The current state of the instance. 
@property ( readonly) EC2InstanceState *state;
// The private DNS name assigned to the instance. 
@property ( readonly) NSString *privateDnsName;
//The public DNS name assigned to the instance. 
@property ( readonly) NSString *publicDnsName;
//Reason for the most recent state transition. 
@property ( readonly) NSString * 	stateTransitionReason;
//If this instance was launched with an associated key pair, this displays the key pair name. 
@property ( readonly) NSString * 	keyName;
// The AMI launch index, which can be used to find this instance within the launch group. 
@property ( readonly) NSNumber * 	amiLaunchIndex;
// Product codes attached to this instance. 
@property ( readonly) NSMutableArray * 	productCodes;
// The instance type. 
@property ( readonly) NSString * 	instanceType;
//The time this instance launched. 
@property ( readonly) NSDate * 	launchTime;
// The location where this instance launched. 
@property ( readonly) EC2Placement * 	placement;
//Platform of the instance (e.g., Windows). 
@property ( readonly) NSString * 	platform;
// Monitoring status for this instance. 
@property ( readonly) EC2Monitoring * 	monitoring;
//Specifies the Amazon VPC subnet ID in which the instance is running. 
@property ( readonly) NSString * 	subnetId;
//Specifies the Amazon VPC in which the instance is running. 
@property ( readonly) NSString * 	vpcId;
//Specifies the private IP address that is assigned to the instance (Amazon VPC). 
@property ( readonly) NSString * 	privateIpAddress;
//Specifies the IP address of the instance. 
@property ( readonly) NSString * 	publicIpAddress;
// The reason for the state change. 
@property ( readonly) EC2StateReason * 	stateReason;
//The architecture of this instance. 
@property ( readonly) NSString * 	architecture;
//The root device type used by the AMI. 
@property ( readonly) NSString * 	rootDeviceType;
//The root device name (e.g., /dev/sda1). 
@property ( readonly) NSString * 	rootDeviceName;
//Block device mapping set. 
//Represents an active license in use and attached to an Amazon EC2 instance. 
@property (readonly) EC2InstanceLicense * 	license;
@property ( readonly) NSMutableArray * 	blockDeviceMappings;
@property ( readonly) NSString * 	virtualizationType;
@property ( readonly) NSString * 	instanceLifecycle;
@property ( readonly) NSString * 	spotInstanceRequestId;
@property ( readonly) NSString * 	clientToken;;
@property ( readonly) NSMutableArray * 	securityGroups;
@property ( readonly) NSString *description;
@property (readonly) EC2PowerState powerState;
@property ( readonly) NSArray *tags;
@property (readonly) NSUInteger tags_count;


// references
@property ( readonly) NSString *ramDiskId;
@property ( readonly) NSString *kernelId;

// not sure what these are
//bool 	sourceDestCheck
//
//bool 	sourceDestCheckIsSet
//


@end
