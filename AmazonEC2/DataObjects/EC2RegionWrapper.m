//
//  EC2RegionWrapper.m
//  hypOps
//
//  Created by Ian Firth on 24/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "EC2RegionWrapper.h"
#import "EC2HypervisorConnection.h"

@interface EC2RegionWrapper (){
    NSString* localRegionName;
}
@end


@implementation EC2RegionWrapper

-(id) initWithEC2RegionIdentity:(NSString *)regionName{
    self = [super init];
    if (self){
        wrappedRegion = nil;
        localRegionName = regionName;
    }
    return self;
}

-(id) initWithEC2Region:(EC2Region *)region{
    self = [super init];
    if (self){
        wrappedRegion = region;
    }
    return self;
}
- (void) dealloc{
    wrappedRegion = nil;
}

- (NSString *) regionName{
    if (wrappedRegion){
       return [wrappedRegion regionName];
    }
    else{
        return localRegionName;
    }
}

- (NSString *) endpoint{
    return [wrappedRegion endpoint];
}


#pragma mark HypObjReferencesProtocol
// might possibly need to add ownerID in here at some point too.
- (NSDictionary *)references{
    return nil;
}

- (NSString *)uniqueReference{
    return [self regionName];
}

- (int) objectType{
    return HYPOBJ_EC2REGIONS;
}

@end
