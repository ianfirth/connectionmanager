//
//  EC2ImageWrapper.m
//  hypOps
//
//  Created by Ian Firth on 17/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "EC2ImageWrapper.h"
#import "EC2HypervisorConnection.h"

@interface EC2ImageWrapper (){
    NSString* localImageId;
}
@end

@implementation EC2ImageWrapper
-(id) initWithEC2ImageIdentity:(NSString *)imageId{
    self = [super init];
    if (self){
        wrappedImage = nil;
        localImageId = imageId;
    }
    return self;
}

-(id) initWithEC2Image:(EC2Image *)image{
    self = [super init];
    if (self){
        wrappedImage = image;
    }
    return self;
}
- (void) dealloc{
    wrappedImage = nil;
}

- (NSString *) imageId{
if (wrappedImage){    
return [wrappedImage imageId];
}
else{
    return localImageId;
}
}

- (NSString *) name{
    if (wrappedImage){
       return [wrappedImage name];
    }
    return [NSString stringWithFormat:@"Image id:%@",[self imageId] ];
}

- (NSString *) description{
    if (wrappedImage){
        return [wrappedImage description];
    }
    return @"Image Not available";
}

-(NSArray *) tags{
    return [wrappedImage tags];
}

-(NSString *) platform{
    return [wrappedImage platform];
}

-(NSString *) imageOwnerAlias{
    return [wrappedImage imageOwnerAlias];
}

-(BOOL) publicValueIsSet{
    return [wrappedImage publicValueIsSet];
}

- (BOOL) publicValue{
    return [wrappedImage publicValue];
}

-(NSString *) architecture{
    return [wrappedImage architecture];
}

-(NSString *) rootDeviceType{
    return [wrappedImage rootDeviceType];
}

-(NSString *) imageLocation{
    return [wrappedImage imageLocation];
}

-(EC2_ImageType) imageType{
    if ([[wrappedImage imageType] isEqualToString:@"machine"]) {
        return EC2_IMAGETYPE_MACHINE;   
    }
    else if ([[wrappedImage imageType] isEqualToString:@"ramdisk"]) {
        return EC2_IMAGETYPE_RAMDISK;   
    }
    else if ([[wrappedImage imageType]  isEqualToString:@"kernel"]) {
        return EC2_IMAGETYPE_KERNEL;
    }
    else  {
        NSLog(@"Found UNKNOWN image type");
        return EC2_IMAGETYPE_UNKNOWN;
    }
}
#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(EC2ImageWrapper *)otherObject
{
    return [[self name] compare:[otherObject name]];
}

#pragma mark Helper predicates
+ (NSPredicate *) imageWithReference:(NSString *)imageId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"imageId like [c]'%@'",imageId]];
}

+ (NSPredicate *) Images_Tagged{
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        EC2ImageWrapper *image = (EC2ImageWrapper *)evaluatedObject;
        return ([[image tags] count] >0);
    }];
}

+ (NSPredicate *) Images_WithTagKey:(NSString *)tagKey{
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        EC2ImageWrapper *image = (EC2ImageWrapper *)evaluatedObject;
        int count = 0;
        while (count < [[image tags] count]){
            EC2Tag *tag = [[image tags] objectAtIndex:count];
            if ([[tag key] isEqualToString:tagKey])
            {
                return YES;
            }
            count ++;
        }
        return NO;
    }];
}
- (NSString*) instanceId{
    return @"oops";
}

+ (NSPredicate *) Images_WithTagKey:(NSString *)tagKey andValue:(NSString *)value{
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        EC2ImageWrapper *instance = (EC2ImageWrapper *)evaluatedObject;
        int count = 0;
        while (count < [[instance tags] count]){
            EC2Tag *tag = [[instance tags] objectAtIndex:count];
            if ([[tag key] isEqualToString:tagKey] && [[tag value] isEqualToString:value])
            {
                return YES;
            }
            count ++;
        }
        return NO;
    }];    
}

+ (NSPredicate *) Images_OwnedByMe:(NSString *)myAccountId{
    return [NSPredicate predicateWithFormat:@"name like '%@'",myAccountId];
}

+ (NSPredicate *) Images_64Bit{
    return [NSPredicate predicateWithFormat:@"architecture like [c]'i386'"];
}

+ (NSPredicate *) Images_32Bit{
    return [NSPredicate predicateWithFormat:@"architecture like [c]'x86_64'"];
}

+ (NSPredicate *) Images_Public{
    return [NSPredicate predicateWithFormat:@"publicValueIsSet == 0 || publicValue == 1"];
}

+ (NSPredicate *) Images_Private{
    return [NSPredicate predicateWithFormat:@"publicValueIsSet == 1 && publicValue == 0"];
}

+ (NSPredicate *) Images_EBS{
    return [NSPredicate predicateWithFormat:@"rootDeviceType == 'ebs'"];
}

+ (NSPredicate *) Images_Amazon{
    return [NSPredicate predicateWithFormat:@"imageOwnerAlias like [c]'Amazon'"];
}

+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) nameBeginWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@'",name]];    
}

+ (NSPredicate *) nameNullOrEmpty{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name == nil OR name ==''"]];    
}

#pragma mark HypObjReferencesProtocol
// might possibly need to add ownerID in here at some point too.
- (NSDictionary *)references{
    // if there are objects that this object is dependne on and should be loaded before this object is used/considered complete
    // return a dictionary of the type->NSArray(NSString *)references
    // this needs to be an array containing the kernel and ramdisk ids
    NSMutableDictionary *refs = [[NSMutableDictionary alloc] initWithCapacity:2];
    if ([wrappedImage kernelId]){
        [refs setObject:[NSArray arrayWithObject:[wrappedImage kernelId]] forKey:[NSNumber numberWithInt:HYPOBJ_EC2IMAGES_KERNEL]];
    }
    if ([wrappedImage ramdiskId]){
        [refs setObject:[NSArray arrayWithObject:[wrappedImage ramdiskId]] forKey:[NSNumber numberWithInt:HYPOBJ_EC2IMAGES_RAMDISK]];
    }
    return refs;
}

- (NSString *)uniqueReference{
    return [self imageId];
}

- (int) objectType{
    return HYPOBJ_EC2IMAGES_MACHINE;
}

@end
