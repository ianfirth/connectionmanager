//
//  EC2RegionWrapper.h
//  hypOps
//
//  Created by Ian Firth on 24/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <AWSiOSSDK/EC2/EC2Region.h>
#import "HypObjReferencesProtocol.h"

@interface EC2RegionWrapper : NSObject<HypObjReferencesProtocol>  {
    EC2Region *wrappedRegion; 
}

// default constructor
-(id) initWithEC2Region:(EC2Region *)region;
//constructor for failed load placeholder
-(id) initWithEC2RegionIdentity:(NSString *)regionName;

// these are the properties that the wrapped object exposes
@property (readonly) NSString *regionName;
@property ( readonly) NSString *endpoint;


@end
