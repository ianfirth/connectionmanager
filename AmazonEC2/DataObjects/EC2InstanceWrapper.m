//
//  EC2InstanceWrapper.m
//  hypOps
//
//  Created by Ian Firth on 23/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "EC2InstanceWrapper.h"
#import "EC2HypervisorConnection.h"

@interface EC2InstanceWrapper (){
    NSString* localInstanceId;
}
@end

@implementation EC2InstanceWrapper


-(id) initWithEC2InstanceIdentity:(NSString *)instanceId{
    self = [super init];
    if (self){
        localInstanceId = nil;
        localInstanceId = instanceId;
    }
    return self;
}

-(id) initWithEC2Instance:(EC2Instance *)instance{
    self = [super init];
    if (self){
        wrappedInstance = instance;
    }
    return self;
}
- (void) dealloc{
    wrappedInstance = nil;
}

- (NSString *) instanceId{
    if (wrappedInstance){
    return [wrappedInstance instanceId];
    }
    else {
        return localInstanceId;
    }
}


- (NSSet *) availablePowerOperations{
    NSMutableSet *availableOps = [[NSMutableSet alloc] initWithCapacity:1];
    
    switch ([self powerState]) {
        case EC2POWERSTATE_PENDING:
            break;
        case EC2POWERSTATE_RUNNING:
            [availableOps addObject:[NSNumber numberWithInt:EC2POWEROPERATION_STOP]];
            [availableOps addObject:[NSNumber numberWithInt:EC2POWEROPERATION_TERMINATE]];
            break;
        case EC2POWERSTATE_SHUTTINGDOWN:
            break;
        case EC2POWERSTATE_STOPPED:
        case EC2POWERSTATE_STOPPING:
            [availableOps addObject:[NSNumber numberWithInt:EC2POWEROPERATION_START]];
            [availableOps addObject:[NSNumber numberWithInt:EC2POWEROPERATION_TERMINATE]];
            break;
        case EC2POWERSTATE_TERMINATED:
            break;
        default:
            break;
    }    
    return availableOps;
}

- (NSString *) imageId{
    return [wrappedInstance imageId];
}

- (EC2InstanceState *) state{
    return [wrappedInstance state];
}

- (NSString *) privateDnsName{
    return [wrappedInstance privateDnsName];
}

- (NSString *) publicDnsName{
    return [wrappedInstance publicDnsName];
}

- (NSString *) stateTransitionReason{
    return [wrappedInstance stateTransitionReason];
}

- (NSString *) keyName{
    return [wrappedInstance keyName];
}

- (NSNumber *) amiLaunchIndex{
    return [wrappedInstance amiLaunchIndex];
}

- (NSMutableArray *) productCodes{
    return [wrappedInstance productCodes];
}

- (NSString *) instanceType{
    return [wrappedInstance instanceType];
}

- (NSDate *) launchTime{
    return [wrappedInstance launchTime];
}

- (EC2Placement *) placement{
    return [wrappedInstance placement];
}

- (NSString *) platform{
    return [wrappedInstance platform];
}

- (EC2Monitoring *) monitoring{
    return [wrappedInstance monitoring];
}

- (NSString *) subnetId{
    return [wrappedInstance subnetId];
}

- (NSString *) vpcId{
    return [wrappedInstance vpcId];
}

- (NSString *) privateIpAddress{
    return [wrappedInstance privateIpAddress];
}

- (NSString *) publicIpAddress{
    return [wrappedInstance publicIpAddress];
}

- (EC2StateReason *) stateReason{
    return [wrappedInstance stateReason];
}

- (NSString *) architecture{
    return [wrappedInstance architecture];
}

- (NSString *) rootDeviceType{
    return [wrappedInstance rootDeviceType];
}

- (NSString *) rootDeviceName{
    return [wrappedInstance rootDeviceName];
}

- (EC2InstanceLicense *) license{
    return [wrappedInstance license];
}

- (NSMutableArray *) blockDeviceMappings{
    return [wrappedInstance blockDeviceMappings];
}

- (NSString *) virtualizationType{
    return [wrappedInstance virtualizationType];
}

- (NSString *) instanceLifecycle{
    return [wrappedInstance instanceLifecycle];
}

- (NSString *) spotInstanceRequestId{
    return [wrappedInstance spotInstanceRequestId];
}

- (NSString *) clientToken{
    return [wrappedInstance clientToken];
}

- (NSMutableArray *) securityGroups{
    return [wrappedInstance securityGroups];
}

- (NSString *) description{
    return [wrappedInstance description];
}

- (EC2PowerState) powerState{
    
    EC2InstanceState *s = [wrappedInstance state];
    int stateCodeInt = [[s code] intValue];

    switch (stateCodeInt) {
        case 0:
            return EC2POWERSTATE_PENDING;
            break;
        case 16:
            return EC2POWERSTATE_RUNNING;
            break;
        case 32:
            return EC2POWERSTATE_SHUTTINGDOWN;
            break;
        case 48:
            return EC2POWERSTATE_TERMINATED;
            break;
        case 64:
            return EC2POWERSTATE_STOPPING;
            break;
        case 80:
            return EC2POWERSTATE_STOPPED;
            break;
            
        default:
            return EC2POWERSTATE_PENDING;
            break;
    }
}

-(NSArray *) tags{
    return [wrappedInstance tags];
}

-(NSUInteger) tags_count{
    return [[self tags] count];
}

// references
- (NSString *) ramDiskId{
    return [wrappedInstance ramdiskId];
}

- (NSString *) kernelId{
    return [wrappedInstance kernelId];
}


#pragma mark HypObjReferencesProtocol
// might possibly need to add ownerID in here at some point too.
- (NSDictionary *)references{
    // if there are objects that this object is dependne on and should be loaded before this object is used/considered complete
    // return a dictionary of the type->NSArray(NSString *)references
    // this needs to be an array containing the kernel and ramdisk ids
    NSMutableDictionary *refs = [[NSMutableDictionary alloc] initWithCapacity:2];
    if ([wrappedInstance kernelId]){
        [refs setObject:[NSArray arrayWithObject:[wrappedInstance kernelId]] forKey:[NSNumber numberWithInt:HYPOBJ_EC2IMAGES_KERNEL]];
    }
    if ([wrappedInstance ramdiskId]){
        [refs setObject:[NSArray arrayWithObject:[wrappedInstance ramdiskId]] forKey:[NSNumber numberWithInt:HYPOBJ_EC2IMAGES_RAMDISK]];
    }

    if ([wrappedInstance imageId]){
        [refs setObject:[NSArray arrayWithObject:[wrappedInstance imageId]] forKey:[NSNumber numberWithInt:HYPOBJ_EC2IMAGES_MACHINE]];
    }
    return refs;
}

- (NSString *)uniqueReference{
    return [self instanceId];
}

- (int) objectType{
    return HYPOBJ_EC2INSTANCES;
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByInstanceId:(EC2InstanceWrapper *)otherObject
{
    return [[self instanceId] compare:[otherObject instanceId]];
}

#pragma mark -
#pragma mark Predicates
+ (NSPredicate *) instanceIDBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name beginsWith [c]'%@' OR name contains [c]' %@'",name,name]];
}

+ (NSPredicate *) instanceWithReference:(NSString *)instanceId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"instanceId like [c]'%@'",instanceId]];
}

// a predicate to define the VMs in the specified state
+ (NSPredicate *) instance_PowerStateFor:(EC2PowerState)powerState{
    NSNumber *stateValue =  [NSNumber numberWithInt:powerState];
    return [NSPredicate predicateWithFormat:@"powerState == %@",stateValue];
}

+ (NSPredicate *) instance_Tagged{
    return [NSPredicate predicateWithFormat:@"tags_count > 0"];
}

+ (NSPredicate *) Instances_WithTagKey:(NSString *)tagKey{
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        EC2InstanceWrapper *instance = (EC2InstanceWrapper *)evaluatedObject;
        int count = 0;
        while (count < [[instance tags] count]){
            EC2Tag *tag = [[instance tags] objectAtIndex:count];
            if ([[tag key] isEqualToString:tagKey])
            {
                return YES;
            }
            count ++;
        }
        return NO;
    }];
}

+ (NSPredicate *) Instances_WithTagKey:(NSString *)tagKey andValue:(NSString *)value{
    return [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        EC2InstanceWrapper *instance = (EC2InstanceWrapper *)evaluatedObject;
        int count = 0;
        while (count < [[instance tags] count]){
            EC2Tag *tag = [[instance tags] objectAtIndex:count];
            if ([[tag key] isEqualToString:tagKey] && [[tag value] isEqualToString:value])
            {
                return YES;
            }
            count ++;
        }
        return NO;
    }];    
}
@end
