//
//  EC2SecurityGroupWrapper.m
//  hypOps
//
//  Created by Ian Firth on 24/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import "EC2SecurityGroupWrapper.h"
#import "EC2HypervisorConnection.h"

@interface EC2SecurityGroupWrapper (){
    NSString* localGroupId;
}
@end

@implementation EC2SecurityGroupWrapper


-(id) initWithEC2SecurityGroupIdentity:(NSString *)groupId{
    self = [super init];
    if (self){
        wrappedSecurityGroup = nil;
        localGroupId = groupId;
    }
    return self;
}

-(id) initWithEC2SecurityGroup:(EC2SecurityGroup *)securityGroup{
    self = [super init];
    if (self){
        wrappedSecurityGroup = securityGroup;
    }
    return self;
}

- (void) dealloc{
    wrappedSecurityGroup = nil;
}

//The name of this security group.
- (NSString *) groupName{
    return [wrappedSecurityGroup groupName];
}

- (NSString *) groupId{
    if (wrappedSecurityGroup){
        return [wrappedSecurityGroup groupId];
    }
    else {
        return localGroupId;
    }
}

//The AWS Access Key ID of the owner of the security group.
- (NSString *) ownerId{
    return [wrappedSecurityGroup ownerId];
}

//The description of this security group. 
- (NSString *) descriptionValue{
    return [wrappedSecurityGroup descriptionValue];
}

//The permissions enabled for this security group.  
- (NSMutableArray *) ipPermissions{
    return [wrappedSecurityGroup ipPermissions];
}

- (NSMutableArray *) tags{
    return [wrappedSecurityGroup tags];
}

-(NSUInteger) tags_count{
    return [[self tags] count];
}

//These 2 are not exposed yet
//NSMutableArray * 	ipPermissionsEgress
//NSString * 	vpcId

#pragma mark -
#pragma mark Predicates
+ (NSPredicate *) groupNameBeginsOrContainsAWordBeginningWith:(NSString *)name{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"groupName beginsWith [c]'%@' OR groupName contains [c]' %@'",name,name]];
}

+ (NSPredicate *) groupWithReference:(NSString *)groupId{
    return [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"groupId like [c]'%@'",groupId]];
}

#pragma mark sort comparitors
- (NSComparisonResult)compareByName:(EC2SecurityGroupWrapper *)otherObject
{
    return [[self groupName] compare:[otherObject groupName]];
}

#pragma mark HypObjReferencesProtocol
// might possibly need to add ownerID in here at some point too.
- (NSDictionary *)references{
    return nil;
}

- (NSString *)uniqueReference{
    return [self groupId];
}

- (int) objectType{
    return HYPOBJ_EC2SECURITYGROUPS;
}

@end
