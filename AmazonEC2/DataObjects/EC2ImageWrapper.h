//
//  EC2ImageWrapper.h
//  hypOps
//
//  Created by Ian Firth on 17/07/2011.
//  Copyright 2011 Ian Firth. All rights reserved.
//

#import <AWSiOSSDK/EC2/EC2Image.h>
#import "HypObjReferencesProtocol.h"

/**
 Enum to represent the different image types available 
 */
typedef enum {
    EC2_IMAGETYPE_UNKNOWN,   
    EC2_IMAGETYPE_MACHINE,   
    EC2_IMAGETYPE_KERNEL,   
    EC2_IMAGETYPE_RAMDISK,  
} EC2_ImageType;

@interface EC2ImageWrapper : NSObject<HypObjReferencesProtocol>  {
    EC2Image *wrappedImage; 
}

// default constructor
-(id) initWithEC2Image:(EC2Image *)image;

// constructor for null object that failed load
-(id) initWithEC2ImageIdentity:(NSString *)imageId;

// these are thr properties that the wrapped object exposes
@property (readonly) NSString *imageId;
@property (readonly) NSString *name;
@property (readonly) NSString *description;
@property (readonly) NSArray *tags;
@property (readonly) NSString *platform;
@property (readonly) NSString *imageOwnerAlias;
@property (readonly) BOOL publicValueIsSet;
@property (readonly) BOOL publicValue;
@property (readonly) NSString *architecture;
@property (readonly) NSString *rootDeviceType;
@property (readonly) NSString *imageLocation;
@property (readonly) EC2_ImageType imageType;

// comparators for seaching
- (NSComparisonResult)compareByName:(EC2ImageWrapper *)otherObject;


+ (NSPredicate *) imageWithReference:(NSString *)imageId;
+ (NSPredicate *) Images_Tagged;
+ (NSPredicate *) Images_WithTagKey:(NSString *)tagKey;
+ (NSPredicate *) Images_WithTagKey:(NSString *)tagKey andValue:(NSString *)value;
+ (NSPredicate *) Images_64Bit;
+ (NSPredicate *) Images_32Bit;
+ (NSPredicate *) Images_Public;
+ (NSPredicate *) Images_Private;
+ (NSPredicate *) Images_EBS;
+ (NSPredicate *) Images_OwnedByMe:(NSString *)myAccountId;
+ (NSPredicate *) Images_Amazon;
+ (NSPredicate *) nameBeginsOrContainsAWordBeginningWith:(NSString *)name;
+ (NSPredicate *) nameBeginWith:(NSString *)name;
+ (NSPredicate *) nameNullOrEmpty;
@end
