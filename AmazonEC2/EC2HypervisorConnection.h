//
//  EC2HypervisorConnection.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "HypervisorConnectionFactory.h"
#import <AWSiOSSDK/EC2/AmazonEC2Client.h>

#define HYPOBJ_EC2INSTANCES 1
#define HYPOBJ_EC2IMAGES_MACHINE 2
#define HYPOBJ_EC2IMAGES_KERNEL 4
#define HYPOBJ_EC2IMAGES_RAMDISK 8
#define HYPOBJ_EC2VOLUMES 16
#define HYPOBJ_EC2SECURITYGROUPS 32
#define HYPOBJ_EC2REGIONS 64
#define HYPOBJ_EC2_LASTOBJECTTYPE 64

@interface InstanceCounter:NSObject
    @property int totalInstanceCount;
    @property int runningInstanceCount;
@end


@interface EC2HypervisorConnection : HypervisorConnectionFactory {
    AmazonEC2Client *ec2Client;
    
    /**
     The display controller object for the connection
     */
    id ec2ConnectionViewController;
    
    ConnectionState connectionState;
}

// for the HypervisorConnectionFactory abstract methods
- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password;
- (ConnectionState)GetConnectionState;
- (void)releaseMemory;
- (void)closeConnection;
- (void)reloadCoreData;
/**
 Request all data objects for a specific type from the Hypervisor connection.  This is an async request.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObjectsForType:(int)hypObjectType;

/**
 Request specific data object for a specific type from the Hypervisor connection.  This is an async request.
 @param reference The reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType;

/**
 Request specific data objects for a specific type from the Hypervisor connection.  This is an async request.
 @param Ec2Objects The hypervisor objects to requrest.
 @param hypObjectType The type of objects to request (all objets must be of the same type)
 */
-(void) RequestHypObjectsForType:(int)hypObjectType withEC2Objects:(NSArray*)Ec2Objects;

/**
 Request specific data objects for a specific type from the Hypervisor connection.  This is an async request.
 @param idStrings The unique identifiers of the hypervisor objects to request.
 @param hypObjectType The type of objects to request (all objets must be of the same type)
 */
-(void) RequestHypObjectsForType:(int)hypObjectType withIds:(NSArray*)idStrings;

// methods for just this connection type

/**
 Sets the client region endpoint.  This will essestially resrict the connection
 to data within this specifiec region.  Setting nil will open it up to all regions (I think)
 */
-(void) setEC2ClientEndpoint:(NSString *)endpoint;


/**
 Starts the specified instances.
 The array of instances is the list of iEC2instanceWrappers that are to be started.
 */
- (NSError*) startInstances:(NSMutableArray*)instances ignoreTerminated:(BOOL)ignoreTerminsted;

/**
 Stop the specified instances.
 The array of instances is the list of EC2instanceWrappers that are to be stopped.
 */
- (NSError*) stopInstances:(NSMutableArray*)instances ignoreTerminated:(BOOL)ignoreTerminsted;

/**
 Stop the specified instances.
 The array of instances is the list of EC2instanceWrappers that are to be stopped.
 */
- (NSError*) terminateInstances:(NSArray*)instances;

/**
 return the number of instances in the region.
 */
-(InstanceCounter*) numberOfInstances;

@property (readonly) AmazonEC2Client *ec2Client;

@end

@interface RequestTypes: NSObject{
    EC2HypervisorConnection *__weak hypervisorConnection;
    int objectTypes;
}

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)objectTypes;

@property (weak, readonly) EC2HypervisorConnection *hypervisorConnection;
@property (readonly)int objectTypes;

@end

@interface RequestObject: NSObject{
    EC2HypervisorConnection *__weak hypervisorConnection;
    int objectType;
    NSString *reference;
}

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection type:(int)theObjectType andReference:(NSString *)theReference;

@property (weak, readonly) EC2HypervisorConnection *hypervisorConnection;
@property (readonly) int objectType;
@property (readonly) NSString* reference;

@end

@interface ResponseTypes: NSObject{
    EC2HypervisorConnection *__weak hypervisorConnection;
    int objectTypes;
    BOOL sucsess;
    NSArray* objects;
    BOOL isCompleteSet;
}


- (id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)types result:(BOOL)result resultObjectsOrNil:(NSArray *)results  isCompleteSet:(BOOL)completeSet;

@property (weak, readonly) EC2HypervisorConnection *hypervisorConnection;
@property (readonly)int objectTypes;
@property (readonly)BOOL sucsess;
@property (readonly)NSArray *objects;
@property (readonly)BOOL isCompleteSet;

@end
