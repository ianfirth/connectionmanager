//
//  EC2HypervisorConnection.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "EC2HypervisorConnection.h"
#import "HypervisorConnectionFactory.h"
#import <AWSiOSSDK/EC2/EC2DescribeImagesRequest.h>
#import "HypObjReferencesProtocol.h"
#import "EC2ImageWrapper.h"
#import "EC2InstanceWrapper.h"
#import "EC2RegionWrapper.h"
#import "EC2SecurityGroupWrapper.h"

@implementation RequestTypes

@synthesize hypervisorConnection,objectTypes;

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)types{
    self = [super init];
    if (self){
    hypervisorConnection = connection;
    objectTypes = types;
    }
    return self;
}

@end

@implementation RequestObject

@synthesize hypervisorConnection,objectType,reference;

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection type:(int)theObjectType andReference:(NSString *)theReference{
    self = [super init];
    if (self){
        reference = [theReference copy];
        hypervisorConnection  = connection;
        objectType = theObjectType;
    }
    return self;
}

@end

@implementation ResponseTypes

@synthesize hypervisorConnection,objectTypes,sucsess, objects, isCompleteSet;

-(id) initWithHypervisorConnection:(EC2HypervisorConnection *)connection types:(int)types result:(BOOL)result resultObjectsOrNil:(NSArray *)results  isCompleteSet:(BOOL)completeSet{
    self = [super init];
    if (self){
        hypervisorConnection = connection;
        objectTypes = types;
        sucsess = result;
        objects = results;
        isCompleteSet = completeSet;
    }
    return self;
}

@end

@implementation InstanceCounter
@synthesize totalInstanceCount,runningInstanceCount;
@end

@interface EC2HypervisorConnection ()
    -(void)RequestHypObjects:(RequestTypes *)request;
    -(void)RequestHypSingleObject:(RequestObject *)request;
@end

@implementation EC2HypervisorConnection

@synthesize ec2Client;


// constructor for the class
-(id) init{
    self = [super init];
    if (self){
        hypervisorConnectionType = HYPERVISOR_EC2;
        connectionState = CONNECTION_UNCONNECTED;
    }
    return self;
}

- (void)dealloc{
    NSLog(@"dealloc Amazon EC2 Connection");
}

-(void) setEC2ClientEndpoint:(NSString *)endpoint{
    // this is what needs to be set to control the regions
    // might be better to use NSURL here, but could not work out how to create one without a path
    NSString *finalUrl = [NSString stringWithFormat:@"https://%@",endpoint];
    [ec2Client setEndpoint:finalUrl];              
}


#pragma mark HypervisorConnectionFactory implementation
// EC2 has currently no autoupdate mode.  This could be implemented as a continuous
// background timer if required but no need for now at all.
+ (BOOL) isAutoUpdateConfigured{
    return NO;
}

- (void)ConnectToAddress:(NSString *)address withUsername:(NSString *)username andPassword:(NSString *)password{
    [super ConnectToAddress:address withUsername:username andPassword:password];
    NSLog(@"ConnectToAddress %@ ACCESS Key is %@",address,username);
    if ([self GetConnectionState] != CONNECTION_CONNECTED && [self GetConnectionState] != CONNECTION_CONNECTING){

        // clean up any previous client
        if (!ec2Client){
            ec2Client = nil;
        }
        ec2Client = [[AmazonEC2Client alloc] initWithAccessKey:username withSecretKey:password];
        if (address){
            [ec2Client setEndpoint:address];
        }

        connectionState = CONNECTION_CONNECTING;
        [NSThread detachNewThreadSelector:@selector(connectTo:) 
                                 toTarget:self 
                               withObject:self];

    }
    else {
        // dont connect again just imply the connect
        NSLog(@"Already connected");  
    }
}


- (ConnectionState)GetConnectionState{
    return connectionState;
}

- (void)suspendConnection{
   // nothing to do for Amazon EC2.  Each connection can just be left as there
   // is no automatic refresh mode at present.
}


- (void)releaseMemory{
    
}

// close the connection
- (void)closeConnection{
    NSNumber *hypervisorId = [[NSNumber alloc] initWithInt:HYPERVISOR_EC2];
    NSMutableDictionary *ec2Connections = [[HypervisorConnectionFactory connectionDictionary] objectForKey:hypervisorId];
    if (ec2Connections != nil){
        NSString *key = nil;
        for (NSString* connectionKey in [ec2Connections allKeys]){
            EC2HypervisorConnection *connection = [ec2Connections objectForKey:connectionKey];
            if (connection == self){
                key = connectionKey;
            }
        }
        if (key){
            NSLog(@"Closing connection %@", key);
            // send an unregister and logout
            [ec2Connections removeObjectForKey:key];
        }
    }
    
    hypervisorId = nil;
}


- (void)reloadCoreData;{
    int requestObjects = 0;
    if ([self isUpdateAvailableForType:HYPOBJ_EC2INSTANCES]){
        requestObjects = HYPOBJ_EC2INSTANCES;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_EC2IMAGES_MACHINE]){
        requestObjects = requestObjects | HYPOBJ_EC2IMAGES_MACHINE;
    }
    if ([self isUpdateAvailableForType:HYPOBJ_EC2SECURITYGROUPS]){
        requestObjects = requestObjects | HYPOBJ_EC2SECURITYGROUPS;
    }
    if (requestObjects != 0){
        RequestTypes *request = [[RequestTypes alloc] initWithHypervisorConnection:self types:requestObjects];
        [self RequestHypObjects:request];
    }
}

/**
 Request specific data object for a specific type from the Hypervisor connection.  This is an async request.
 @param reference The reference for the object to requrest.
 @param hypObjectType The type of objects to requrest.
 */
- (void)RequestHypObject:(NSString *)reference ForType:(int)hypObjectType{
   // create a request using a filter for the specific object
    RequestObject *request = [[RequestObject alloc] initWithHypervisorConnection:self type:hypObjectType andReference:reference];
    [self RequestHypSingleObject:request];
}

// provide the appropriate property that defines uniqueness for the specified type
-(NSString *) uniqueObjectReferencePropertyForHypObjectType:(int)objectType{
    switch (objectType){
        case HYPOBJ_EC2IMAGES_MACHINE:
        case HYPOBJ_EC2IMAGES_KERNEL:
        case HYPOBJ_EC2IMAGES_RAMDISK:
           return @"imageId";
        case HYPOBJ_EC2INSTANCES:
            return @"instanceId";
        case HYPOBJ_EC2VOLUMES:
            return @"volumeId";
        case HYPOBJ_EC2SECURITYGROUPS:
            return @"groupId";
        case HYPOBJ_EC2REGIONS:
            return @"regionName";
    }

    NSLog(@"No unique property defined for object Amazon object type");
    return @"";
}

#pragma mark -
#pragma mark helperMethods

/**
 * Method to fire the connection events
 */
- (void)fireConnectedDelegatesWithResult:(NSNumber *)sucsess{
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.
    BOOL successBool = [sucsess boolValue];
    if (!successBool){
        [self setLastError:[NSError errorWithDomain:@"Amazon EC2 connection failed" code:0 userInfo:nil]];
        connectionState = CONNECTION_UNCONNECTED;
    }
    else{
        connectionState = CONNECTION_CONNECTED;
    }
    
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorConnectedToConnection:withResult:)])
        {
           [connectionDelegateToCall hypervisorConnectedToConnection:self withResult:successBool];
        }
    }
    copyOfDelegates = nil;
}

/**
* Method to fire the updated events
*/
- (void)fireUpdatedDelegatesWithResult:(ResponseTypes *)response{
    // work on a copy of the delegates so that if anyone removes themselves whilst being used
    // the array is not mutated.    
    if (![response sucsess]){
       // fire an object load failure here....
        
        NSString* reference = nil;
        if ([[response objects] count] >0){
            // ned to find the inentity column name for an object of this type and use this to get the actual reference forn the respose object
            
            NSString* propertyName = [self uniqueObjectReferencePropertyForHypObjectType:[response objectTypes]];
            ResponseTypes* type = [[response objects] objectAtIndex:0];
            reference = [type valueForKey:propertyName];
        }
        
        NSLog(@"load failure");
        
        for (NSObject<HypervisorConnectionDelegate> *delegate in hypervisorConnectionDelegates) {
            if ([delegate respondsToSelector:@selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)]){
                [delegate hypervisorObjectsUpdated:[response hypervisorConnection] updatesObjectsOfType:[response objectTypes] withReference:reference withResult:NO];
            }
        }
        
    }
    
    
    if ([response sucsess]){
        [self updateCachedHypObjects:[NSArray arrayWithArray:[response objects]] forObjectType:[response objectTypes] isCompleteSet:[response isCompleteSet]];
    }
    NSArray *copyOfDelegates = [hypervisorConnectionDelegates copy];
    for (NSObject<HypervisorConnectionDelegate> *connectionDelegateToCall in copyOfDelegates){
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorObjectsUpdated:updatesObjectsOfType:withResult:)])
        {
            [connectionDelegateToCall hypervisorObjectsUpdated:[response hypervisorConnection] updatesObjectsOfType:[response objectTypes] withResult:[response sucsess]];
        }
        if ([connectionDelegateToCall respondsToSelector:@selector(hypervisorObjectsUpdated:updatesObjectsOfType:withReference:withResult:)])
            {
                for (id<HypObjReferencesProtocol> ref in [response objects])
                {
                    [connectionDelegateToCall hypervisorObjectsUpdated:[response hypervisorConnection] updatesObjectsOfType:[response objectTypes]
                                                         withReference:[ref uniqueReference]
                                                            withResult:[response sucsess]];
                }
            }

    }
    copyOfDelegates = nil;
}

- (void)RequestHypObjectsForType:(int)hypObjectType{
    RequestTypes *request = [[RequestTypes alloc] initWithHypervisorConnection:self types:hypObjectType];
    [self RequestHypObjects:request];
}

-(void)RequestHypSingleObject:(RequestObject *)request{
    [NSThread detachNewThreadSelector:@selector(requestDataSingleObject:) 
                             toTarget:self 
                           withObject:request];
}

-(void)RequestHypObjects:(RequestTypes *)request{
    // go though the types and make separate requests here
    int i = 1;
    while (i < (HYPOBJ_EC2_LASTOBJECTTYPE << 1)){
        if ((request.objectTypes & i) > 0){
            RequestTypes *localRequest = [[RequestTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:i];
            [NSThread detachNewThreadSelector:@selector(requestData:) 
                                     toTarget:self 
                                   withObject:localRequest];
        }
        i = i << 1;
    }
}

#pragma mark methods that can be called on a background thread to perform syncronous requrests to the cloud
- (void)connectTo:(EC2HypervisorConnection *)connection{
    @try {
        
        // do something to check that can connect here
        // get regions?
        EC2DescribeRegionsRequest *request = [[EC2DescribeRegionsRequest alloc] init];
        EC2DescribeRegionsResponse *response = [[connection ec2Client] describeRegions:request];

        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (EC2Region *region in [response regions]) {
            [resultArray addObject:[[EC2RegionWrapper alloc] initWithEC2Region:region]];
        }

        [self updateCachedHypObjects:resultArray forObjectType:HYPOBJ_EC2REGIONS isCompleteSet:YES];

        // when get result call the connected delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireConnectedDelegatesWithResult:) 
                                   withObject:[NSNumber numberWithBool:YES] 
                                waitUntilDone:false];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception = %@", exception);
        [self performSelectorOnMainThread:@selector(fireConnectedDelegatesWithResult:) 
                               withObject:[NSNumber numberWithBool:NO] 
                            waitUntilDone:false];

    }
}

- (void)requestDataSingleObject:(RequestObject *)request{
    [self RequestHypObjectsForType:[request objectType] withIds:[NSArray arrayWithObject:[request reference]]];
}

- (void)requestData:(RequestTypes *)request{
    // make sure that there are no outstanding requests for these types already
    while ([self isUpdatePendingForType:[request objectTypes]]){   
        [NSThread sleepForTimeInterval:0.1];
    }
    
    [self setObjectTypeRequestPending:[request objectTypes]];  // indicate that updates are pending 
    
    @try {
        NSMutableArray *resultArray = nil;
        if (([request objectTypes] & HYPOBJ_EC2INSTANCES) >0){
            EC2DescribeInstancesRequest *instancesRequest = [[EC2DescribeInstancesRequest alloc] init];
            
            EC2DescribeInstancesResponse *instancesResponse = [[[request hypervisorConnection] ec2Client] describeInstances:instancesRequest];
             
            resultArray = [[NSMutableArray alloc] init];
            for(EC2Reservation *reservation in [instancesResponse reservations]){
                for (EC2Instance *instance in [reservation instances]){
                    [resultArray addObject:[[EC2InstanceWrapper alloc] initWithEC2Instance:instance]];
                }
            }
        }else if (([request objectTypes] & HYPOBJ_EC2VOLUMES) >0){
            EC2DescribeVolumesRequest *volumesRequest = [[EC2DescribeVolumesRequest alloc] init];
            EC2DescribeVolumesResponse *volumesResponse = [[[request hypervisorConnection] ec2Client] describeVolumes:volumesRequest];
            resultArray = [volumesResponse volumes];
        }else if (([request objectTypes] & HYPOBJ_EC2IMAGES_MACHINE) >0){
            EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
            EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"machine"]];
            [[imagesRequest filters] addObject:filter];
            EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2Image *image in [imageResponse images]) {
                [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
            }
        }else if (([request objectTypes] & HYPOBJ_EC2IMAGES_KERNEL) >0){
            EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
            EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"kernel"]];
            [[imagesRequest filters] addObject:filter];
            EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2Image *image in [imageResponse images]) {
                [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
            }
        }else if (([request objectTypes] & HYPOBJ_EC2IMAGES_RAMDISK) >0){
            EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
            EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"ramdisk"]];
            [[imagesRequest filters] addObject:filter];
            EC2DescribeImagesResponse *imageResponse = [[[request hypervisorConnection] ec2Client] describeImages:imagesRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2Image *image in [imageResponse images]) {
                [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
            }
        }else if (([request objectTypes] & HYPOBJ_EC2SECURITYGROUPS) >0){
            EC2DescribeSecurityGroupsRequest *securityGroupRequest = [[EC2DescribeSecurityGroupsRequest alloc] init];
            EC2DescribeSecurityGroupsResponse *securtyGroupResponse = [[[request hypervisorConnection] ec2Client] describeSecurityGroups:securityGroupRequest];
            resultArray = [[NSMutableArray alloc] init];
            for (EC2SecurityGroup *group in [securtyGroupResponse securityGroups]) {
                [resultArray addObject:[[EC2SecurityGroupWrapper alloc] initWithEC2SecurityGroup:group]];
            }

        }
       
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:[request objectTypes] result:YES resultObjectsOrNil:resultArray  isCompleteSet:YES];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:) 
                                   withObject:response 
                                waitUntilDone:false];
    }
    @catch (AmazonServiceException *exception) {
        NSLog(@"Exception = %@", exception);
        // when get result call the updated objects delegate on the main thread again with error
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:[request hypervisorConnection] types:[request objectTypes] result:NO resultObjectsOrNil:nil isCompleteSet:YES];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:) 
                               withObject:response 
                            waitUntilDone:false];
        
    }
    
    // clear any pending updates for the type specified
    [self clearObjectTypeRequestPending:[request objectTypes]];
}

- (NSSortDescriptor*) sortForhypObjectType:(int)hypObjectType{
    return nil;
}

-(InstanceCounter*) numberOfInstances{
    InstanceCounter* result = nil;
    @try {
        EC2DescribeInstancesRequest *instancesRequest = [[EC2DescribeInstancesRequest alloc] init];
        EC2DescribeInstancesResponse *instancesResponse = [[self ec2Client] describeInstances:instancesRequest];
        int instanceCount =0;
        int runningCount = 0;
        for(EC2Reservation *reservation in [instancesResponse reservations]){
            instanceCount += [[reservation instances] count];
            for (EC2Instance *instance in [reservation instances]){
                EC2InstanceWrapper* wrapper = [[EC2InstanceWrapper alloc] initWithEC2Instance:instance];
                if([wrapper powerState] == EC2POWERSTATE_RUNNING){
                    runningCount ++;
                }
            }
        }
        result = [[InstanceCounter alloc] init];
        [result setTotalInstanceCount:instanceCount];
        [result setRunningInstanceCount:runningCount];
        return result;
    }
    @catch (NSException *exception) {
        return nil;
    }
}

/**
 * takes list of EC2InstanceWrappers and attempts to start them
 */
- (NSError*) startInstances:(NSArray*)instances ignoreTerminated:(BOOL)ignoreTerminsted{ 
    NSError* error = nil;
    NSMutableArray* instanceIds = [NSMutableArray arrayWithCapacity:[instances count]];
    for (EC2InstanceWrapper* instance in instances) {
        if (!(ignoreTerminsted && [instance powerState] == EC2POWERSTATE_TERMINATED)){
            [instanceIds addObject:[instance instanceId]];
        }
    }
    EC2StartInstancesRequest* startRequest = [[EC2StartInstancesRequest alloc] initWithInstanceIds:instanceIds];
    @try{
        [[self ec2Client] startInstances:startRequest];
    }
    @catch (AmazonServiceException *exception) {
        NSDictionary *errorInfo = [NSDictionary dictionaryWithObject:[exception message] forKey:NSLocalizedDescriptionKey];
        error = [[NSError alloc] initWithDomain:[exception errorCode] code:1 userInfo:errorInfo];
    }

    // request updates for the individual objects here
    [self RequestHypObjectsForType:HYPOBJ_EC2INSTANCES withIds:instanceIds];
    return error;
}

/**
 * takes list of EC2InstanceWrappers and attempts to stop them
 */
- (NSError*) stopInstances:(NSArray*)instances ignoreTerminated:(BOOL)ignoreTerminsted{
    NSError* error = nil;
    NSMutableArray* instanceIds = [NSMutableArray arrayWithCapacity:[instances count]];
    for (EC2InstanceWrapper* instance in instances) {
        if (!(ignoreTerminsted && [instance powerState] == EC2POWERSTATE_TERMINATED)){
            [instanceIds addObject:[instance instanceId]];
        }
    }
    EC2StopInstancesRequest* stopRequest = [[EC2StopInstancesRequest alloc] initWithInstanceIds:instanceIds];
    @try{
        [[self ec2Client] stopInstances:stopRequest];
    }
    @catch (AmazonServiceException *exception) {
        NSDictionary *errorInfo = [NSDictionary dictionaryWithObject:[exception message] forKey:NSLocalizedDescriptionKey];
        error = [[NSError alloc] initWithDomain:[exception errorCode] code:1 userInfo:errorInfo];
    }

        // request updates for the individual objects here
    [self RequestHypObjectsForType:HYPOBJ_EC2INSTANCES withIds:instanceIds];
    return error;
}

/**
 * takes list of EC2InstanceWrappers and attempts to terminate them
 */
- (NSError*) terminateInstances:(NSArray*)instances{
    NSError* error = nil;
    NSMutableArray* instanceIds = [NSMutableArray arrayWithCapacity:[instances count]];
    for (EC2InstanceWrapper* instance in instances) {
        [instanceIds addObject:[instance instanceId]];
    }
    EC2TerminateInstancesRequest* terminateRequest = [[EC2TerminateInstancesRequest alloc] initWithInstanceIds:instanceIds];
    @try{
        [[self ec2Client] terminateInstances:terminateRequest];
    }
    @catch (AmazonServiceException *exception) {
        NSDictionary *errorInfo = [NSDictionary dictionaryWithObject:[exception message] forKey:NSLocalizedDescriptionKey];
        error = [[NSError alloc] initWithDomain:[exception errorCode] code:1 userInfo:errorInfo];
    }
    // request updates for the individual objects here
    [self RequestHypObjectsForType:HYPOBJ_EC2INSTANCES withIds:instanceIds];

    return error;
}


-(void) RequestHypObjectsForType:(int)hypObjectType withEC2Objects:(NSArray*)Ec2Objects{
    NSMutableArray* mutableIds = [NSMutableArray arrayWithCapacity:[Ec2Objects count]];
    // convert the input objects to a string list of IDs
    for (id<HypObjReferencesProtocol> hypObj in Ec2Objects) {
        [mutableIds addObject:[hypObj uniqueReference]];
    }
    [self RequestHypObjectsForType:hypObjectType withIds:mutableIds];
}

-(void) RequestHypObjectsForType:(int)hypObjectType withIds:(NSArray*)idStrings{
    // expect the array to contain a bunch of these id<HypObjReferencesProtocol>
    @try {
        NSMutableArray *resultArray = nil;
        NSMutableArray* mutableIds = [NSMutableArray arrayWithArray:idStrings];
        switch (hypObjectType) {
            case HYPOBJ_EC2INSTANCES:
            {
                EC2DescribeInstancesRequest *instancesRequest = [[EC2DescribeInstancesRequest alloc] init];
                [instancesRequest setInstanceIds:mutableIds];
                
                EC2DescribeInstancesResponse *instancesResponse = [[self ec2Client] describeInstances:instancesRequest];
                
                resultArray = [[NSMutableArray alloc] init];
                for(EC2Reservation *reservation in [instancesResponse reservations]){
                    for (EC2Instance *instance in [reservation instances]){
                        [resultArray addObject:[[EC2InstanceWrapper alloc] initWithEC2Instance:instance]];
                    }
                }
                break;
            }
            case HYPOBJ_EC2VOLUMES:
            {
                EC2DescribeVolumesRequest *volumesRequest = [[EC2DescribeVolumesRequest alloc] init];
                [volumesRequest setVolumeIds:mutableIds];
                EC2DescribeVolumesResponse *volumesResponse = [[self ec2Client] describeVolumes:volumesRequest];
                resultArray = [volumesResponse volumes];
                break;
            }
            case HYPOBJ_EC2IMAGES_MACHINE:
            {
                EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
                [imagesRequest setImageIds:mutableIds];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"machine"]];
                [[imagesRequest filters] addObject:filter];
                EC2DescribeImagesResponse *imageResponse = [[self ec2Client] describeImages:imagesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2Image *image in [imageResponse images]) {
                    [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
                }
                if ([resultArray count] < [mutableIds count]){
                    // not all the images were found, create a place holder one so that the
                    // loader works
                    NSMutableArray* additionalResults = [NSMutableArray arrayWithArray:mutableIds];
                    for (EC2ImageWrapper* imageWrapper in resultArray) {
                        [additionalResults removeObject:[imageWrapper imageId]];
                    }
                    for (NSString* addID in additionalResults) {
                        EC2ImageWrapper* fakeWrapper = [[EC2ImageWrapper alloc] initWithEC2ImageIdentity:addID];
                        [resultArray addObject:fakeWrapper];
                    }
                }
                break;
            }
            case HYPOBJ_EC2IMAGES_KERNEL:
            {
                EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
                [imagesRequest setImageIds:mutableIds];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"kernel"]];
                [[imagesRequest filters] addObject:filter];
                EC2DescribeImagesResponse *imageResponse = [[self ec2Client] describeImages:imagesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2Image *image in [imageResponse images]) {
                    [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
                }
                break;
            }
            case HYPOBJ_EC2IMAGES_RAMDISK:
            {
                EC2DescribeImagesRequest *imagesRequest = [[EC2DescribeImagesRequest alloc] init];
                [imagesRequest setImageIds:mutableIds];
                EC2Filter *filter = [[EC2Filter alloc] initWithName:@"image-type" andValues:[NSMutableArray arrayWithObject:@"ramdisk"]];
                [[imagesRequest filters] addObject:filter];
                EC2DescribeImagesResponse *imageResponse = [[self ec2Client] describeImages:imagesRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2Image *image in [imageResponse images]) {
                    [resultArray addObject:[[EC2ImageWrapper alloc] initWithEC2Image:image]];
                }
                break;
            }
            case HYPOBJ_EC2SECURITYGROUPS:
            {
                EC2DescribeSecurityGroupsRequest *securityGroupRequest = [[EC2DescribeSecurityGroupsRequest alloc] init];
                [securityGroupRequest setGroupIds:mutableIds];
                EC2DescribeSecurityGroupsResponse *securtyGroupResponse = [[self ec2Client] describeSecurityGroups:securityGroupRequest];
                resultArray = [[NSMutableArray alloc] init];
                for (EC2SecurityGroup *group in [securtyGroupResponse securityGroups]) {
                    [resultArray addObject:[[EC2SecurityGroupWrapper alloc] initWithEC2SecurityGroup:group]];
                }
                break;
            }
            default:
                break;
        }
        
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:self types:hypObjectType result:YES resultObjectsOrNil:resultArray  isCompleteSet:NO];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:)
                               withObject:response
                            waitUntilDone:false];
    }
    @catch (AmazonServiceException *exception) {
        NSLog(@"Exception = %@", exception);
        // when get result call the updated objects delegate on the main thread again with error
        ResponseTypes *response = [[ResponseTypes alloc] initWithHypervisorConnection:self types:hypObjectType result:NO resultObjectsOrNil:nil isCompleteSet:NO];
        // when get result call the updated objects delegate on the main thread again
        [self performSelectorOnMainThread:@selector(fireUpdatedDelegatesWithResult:)
                               withObject:response
                            waitUntilDone:false];
        
    }
    
    // clear any pending updates for the type specified
    [self clearObjectTypeRequestPending:hypObjectType];

}
@end
