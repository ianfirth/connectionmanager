//
//  HypObjReferencesProtocol.h
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  This protocol should be applied to all objects that a hyperivsor can load.
//  It is assumed that this is the case for most of the operations that the hypervisorConnections can provide that
//  are produced from the hypervisor connections factory.


@protocol HypObjReferencesProtocol <NSObject>
/**
 The Dictionary of other objects that this object references and requires to be considered complete.
 This is keyed on the hypervisor object type and contains an NSMutable array of objects.  This should not store
 all references, otherwise when one object is loaded the tree will be very large and end up pulling in
 all objects into the graph.
 */
- (NSMutableDictionary *)references;

/** 
 * The primary unique refernce value for the object
 */
- (NSString *)uniqueReference;

/**
 * the type of the object
 */
- (int) objectType;

@end
