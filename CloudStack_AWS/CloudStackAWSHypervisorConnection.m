//
//  CloudStackAWSHypervisorConnection.m
//  hypOps
//
// Copyright (C) 2017  Ian Firth (ian@thefirths.me.uk)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import "CloudStackAWSHypervisorConnection.h"

@implementation CloudStackAWSHypervisorConnection
// constructor for the class
-(id) init{
    self = [super init];
    if (self){
        // override the connection type.  Even though it works like amazon I want it to appear as
        // a cloudstack connection so that it can be extended later on
        hypervisorConnectionType = HYPERVISOR_CLOUDSTACK_AWS;
        connectionState = CONNECTION_UNCONNECTED;
    }
    return self;
}

@end
